gpss-server
===========

GPSS aplikacija koja je preradjena da radi kao server za izvršavanje simulacija. Napisana u programskom jeziku Delphi.

Korišćenje
==========

`GPSSW.exe` je izvršna aplikacija. Projekat je `GPSS.dpr`. Dokumentacija i primeri za testiranje se nalaze u odgovarajućim direktorijumima.