�
 TAFORM 0|  TPF0TAFormAFormLeft� TopsWidth1Height>CaptionGPSS for WindowsColor	clBtnFaceFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.Name	Ottawa YU
Font.Style Menu	MainMenu1OldCreateOrder	WindowStatewsMaximizedOnCreate
FormCreatePixelsPerInch`
TextHeight TPageControlPageControlLeft Top Width)Height
ActivePageTabGraphAlignalClientTabOrder OnChangePageControlChange 	TTabSheetTabKodCaptionSource TMemokodLeft Top Width!Height� AlignalClientFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFont
ScrollBars
ssVerticalTabOrder WantTabs	WordWrapOnKeyUpkodKeyUp   	TTabSheet	TabGreskeCaptionListing TMemogreskeLeft Top Width!Height� AlignalClientFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFontReadOnly	
ScrollBars
ssVerticalTabOrder WordWrap   	TTabSheetTabRezultatiCaptionResults 	TRichEdit	RezultatiLeft Top Width!Height� AlignalClientFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameCourier New
Font.Style 
ParentFont
ScrollBars
ssVerticalTabOrder    	TTabSheetTabTabCaptionTables  	TTabSheetTabGraphCaptionCharts TChartchartLeft TopKWidth!Height� BackWall.Brush.ColorclWhiteBackWall.ColorclWhiteFoot.Font.CharsetDEFAULT_CHARSETFoot.Font.ColorclRedFoot.Font.Height�Foot.Font.NameArialFoot.Font.StylefsItalic LeftWall.Brush.StylebsFDiagonalLeftWall.ColorclGreenTitle.Text.Strings  	BackColorclWhiteLegend.TextStyle	ltsXValueLegend.VisibleMaxPointsPerPageAlignalClientColorclWhiteTabOrder  
TBarSeriesSeries1Marks.ArrowLengthMarks.VisibleSeriesColorclRedXValues.DateTimeXValues.NameXXValues.MultiplierXValues.OrderloAscendingYValues.DateTimeYValues.NameBarYValues.MultiplierYValues.OrderloNone   TPanelPanel1Left Top Width!HeightKAlignalTopTabOrder 	TGroupBox	GroupBox4LeftTopWidth`HeightIAlignalLeftCaptionChart selectionTabOrder  TLabelLabel1LeftTopWidth(HeightCaptionTable:  TLabelLabel2Left� TopWidthWHeightCaptionDisplay:  	TComboBoxTabelaCBLeftTop)Width� HeightStylecsDropDownList
ItemHeightTabOrder OnChangeTabelaCBChange  	TComboBoxVrstaCBLeft� Top)Width� HeightStylecsDropDownList
ItemHeightTabOrderOnChangeVrstaCBChange   	TGroupBox	GroupBox5LeftaTopWidth� HeightIAlignalClientCaptionChart optionsTabOrder 	TCheckBoxmonoLeftTop,Width� HeightCaptionBlack and whiteTabOrder OnClick	monoClick  TBitBtnKopirajLeft� TopWidth� HeightCaptionCopy to clipboardTabOrderOnClickKopirajClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 333���333w33w333���333w33w333���333w33w333���?��w��w     w pwwwwwwww���� w�3337��w���� {�?����w � � �w7w7ww����9�3?����w�  �𙙓ww77ww�������?���www �  9�3w7ww7w����9�3?��s7w���3y�3w7�?ww3�����3��swws3   33333www33333	NumGlyphs  TBitBtnStampajLeft� Top-WidthjHeightCaptionPrintTabOrderOnClickStampajClick
Glyph.Data
z  v  BMv      v   (                                    �  �   �� �   � � ��   ���   �  �   �� �   � � ��  ��� 0      ?��������������wwwwwww�������wwwwwww        ���������������wwwwwww�������wwwwwww�������wwwwww        wwwwwww30����337���?330� 337�wss330����337��?�330�  337�swws330���3337��73330��3337�ss3330�� 33337��w33330  33337wws333	NumGlyphs  	TCheckBoxCB3dLeftTopWidthCHeightCaption3DChecked	State	cbCheckedTabOrderOnClick	CB3dClick     	TTabSheetTabPomocCaptionInstructions TMemoPomocLeft Top Width!Height� AlignalClientReadOnly	
ScrollBars
ssVerticalTabOrder WantTabs	    	TMainMenu	MainMenu1Left Top�  	TMenuItemProgram1Caption&File  	TMenuItemSimulacija1Caption&Simulation 	TMenuItem
Simuliraj1Caption&Start simulationOnClickSimuliraj1Click  	TMenuItemN1Caption-  	TMenuItemStampajrezlutate1CaptionPrint resultsOnClickStampajrezlutate1Click  	TMenuItemIskopirajrezultatenaClipboard1CaptionCopy results to clipboardOnClick#IskopirajrezultatenaClipboard1Click    	TAutoMenuAMUkljuci_precicu	MeniProgram1FajlPrecice	GPSSW.iniFilterGPSS models(*.gps)|*.GPSNaziv_sa_ekstenzijomAuto_promena_fonta	Max_velicina_fontaOnUcitajAMUcitajOnSnimiAMSnimiOnNoviAMNoviOnKrajAMKrajOnOProgramuAMOProgramuLeft� Topx   