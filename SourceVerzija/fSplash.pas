unit fSplash;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, jpeg;

type
  TSplash = class(TForm)
    Image1: TImage;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Splash: TSplash;

implementation

{$R *.DFM}

procedure TSplash.Timer1Timer(Sender: TObject);
begin
    Timer1.Interval := 600000;
    close;
end;

procedure TSplash.FormCreate(Sender: TObject);
var hr : THandle;
begin
  hr:=CreateRoundRectRgn(0,0,Width,Height, 30, 30);
  SetWindowRgn(Handle,Hr, true);
end;

end.
