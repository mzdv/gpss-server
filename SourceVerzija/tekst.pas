unit tekst;
{Ovde su svi tekstovi poruka u srpskoj i engleskoj varijanti.}

interface

const

{$DEFINE SERBIAN} // Ovde obrisati "$" kad se zeli engleska verzija.

{$IFDEF SERBIAN}

   mHelpFile = 'procitaj.txt';
//Poruke:
   mErrorsInModel    =  'U modelu ima gresaka!!!';
   mNothingToPrint   =  'Nema rezultata koji bi mogli da se stampaju!';
   mSimResults       =  'Rezultati simulacije : (%s)';
   mFrequencies      =  'Frekvencije';
   mPercentages      =  'Procenti';
   mCumulativePercentages = 'Kumulativni procenti';
   mAverageContents  =  'Prosecni sadrzaj';
   mAverageUtilization= 'Prosecna iskoriscenost';
   mNoEntries =         'Broj ulazaka';
   mAverageTimeTran=    'Prosecno zadrzavanje';
   mFacilities        = 'Uredjaji';
   mFacility =           'Uredjaj';
   mStorages          = 'Skladista';
   mTable    =           'Tabela';
   mQueues   =           'Redovi';
   mQueue    =           'Red';
   mZeroEntries =        'Broj ulazaka bez zadrzavanja';
   mUserChains =         'Korisnicki redovi';
   mUserChain =         'Korisnicki red';
   mMaxContents =        'Maksimalni sadrzaj';
   mStorage =            'Skladiste';
   mCapacity =           'Kapacitet';
   mCurrentContents =    'Trenutni sadrzaj';
   mUpperLimit =         'Gornja granica';
   mObservedFrequency =  'Frekvencija';
   mPercentOfTotal =     'Procenat od ukupnog broja';
   mCumulativePercentage='Kumulativni procenat';
   mCumulativeRemainder= 'Kumulativni ostatak';
   mMultipleOfMean=      'Multiple of mean';
   mDeviationFromMean=   'Odstupanje od sr.vr.';
   mTotalEntries =       'Ukupno ulazaka';
   mPercentZeros =       'Procenat ul. baz zadrzavanja';
   mSeizingTran =        'Brojac zauzimanja uredjaja';
   mPreemptingTran =     'Brojac prijempcija';
   mSimFinished =        'Simulacija je uspesno zavrsena';
   mSimInterrupted =     'Simulacija je prekinuta';
{$ELSE}

{$R ena/main.dfm} // Ovde ukljucujemo prevedeni dfm fajl
   mHelpFile = 'readme.txt';

   mErrorsInModel =     'There are errors in the model!!!';
   mNothingToPrint   =  'There are no results to print!';
   mSimResults       =  'Simulation results : (%s)';
   mFrequencies      =  'Frequencies';
   mPercentages      =  'Percentages';
   mCumulativePercentages = 'Cumulative percentages';
   mAverageContents  =  'Average contents';
   mNoEntries        =  'Number of entries';
   mAverageUtilization = 'Average utilization';
   mAverageTimeTran=     'Average time/tran';
   mFacilities=          'Facilities';
   mFacility =           'Facility';
   mStorages =           'Storages';
   mTable    =           'Table';
   mQueues   =           'Queues';
   mQueue    =           'Queue';
   mZeroEntries =        'Zero entries';
   mUserChains =         'User chains';
   mUserChain  =         'User chain';
   mMaxContents =        'Maximum contents';
   mStorage =            'Storage';
   mCapacity =           'Capacity';
   mCurrentContents =    'Current Contents';
   mUpperLimit =         'Upper limit';
   mObservedFrequency =  'Frequency';
   mPercentOfTotal =     'Percent of total';
   mCumulativePercentage='Cumulative percentage';
   mCumulativeRemainder= 'Cumulative remainder';
   mMultipleOfMean=      'Multiple of mean';
   mDeviationFromMean=   'Deviation from mean';
   mTotalEntries =       'Total entries';
   mPercentZeros =       'Percent zeros';
   mSeizingTran =        'Seizing tran.';
   mPreemptingTran =     'mPreemptingTran';
   mSimFinished =        'Simulation successfuly finished';
   mSimInterrupted =     'Simulation interrupted';
//Poruke:
{$ENDIF}


implementation
end.
