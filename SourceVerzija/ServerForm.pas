{ Milos Zivadinovic 2015 }
{ Si vis pacem, para bellum }

unit ServerForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, IdBaseComponent, IdComponent, IdTCPServer,
  IdCustomHTTPServer, IdHTTPServer, IdURI, ComCtrls, UGpss87, UAsm, uFile, tekst;

type
  TSForm = class(TForm)
    StartButton: TButton;
    ServerskePorukeMemo: TMemo;
    IdHTTPServer1: TIdHTTPServer;
    RezultatiIzvrsavanjaGroup: TGroupBox;
    RezultatiIzvrsavanjaMemo: TMemo;
    procedure StartButtonClick(Sender: TObject);
    procedure IdHTTPServer1CommandGet(AThread: TIdPeerThread;
      ARequestInfo: TIdHTTPRequestInfo;
      AResponseInfo: TIdHTTPResponseInfo);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure IdHTTPServer1CreatePostStream(ASender: TIdPeerThread;
      var VPostStream: TStream);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Server: TSForm;
  result: TStringList;
  interim: TStringList;
  direktorijum: string;
  simulation: TStringList;
  
implementation

uses TypInfo;

{$R *.dfm}

procedure TSForm.StartButtonClick(Sender: TObject);
begin
IdHTTPServer1.DefaultPort := 9001;
ServerskePorukeMemo.Lines.Append('Port: 9001');
IdHTTPServer1.Active := True;
ServerskePorukeMemo.Lines.Append('Server pokrenut u ' + DateTimeToStr(now));
end;

procedure TSForm.IdHTTPServer1CommandGet(AThread: TIdPeerThread;
  ARequestInfo: TIdHTTPRequestInfo; AResponseInfo: TIdHTTPResponseInfo);
  var
    i:      Integer;
  begin

  direktorijum := ExtractFilePAth (Application.ExeName);
  chdir(direktorijum);

  if ARequestInfo.Command = 'POST' then
  begin
    interim := TStringList.Create;
    result := TStringList.Create;
    simulation := TStringList.Create;

    DeleteFile('.gps');

    ExtractStrings(['='], [], PChar(TIdURI.URLDecode((ARequestInfo.PostStream as TStringStream).DataString)), result);
    ExtractStrings(['#'], [], PChar(result[1]), interim);

    interim.SaveToFile('.gps');

    if not GPSSASM_START('') then
    begin
       GPSS_START('', RezultatiIzvrsavanjaGroup);
       
       For i := 0 to (mRez.Count-1) do
        AResponseInfo.ContentText := AResponseInfo.ContentText + mRez.Strings[i];
       
       AResponseInfo.ContentType := 'application/json';
       AResponseInfo.ContentText := AResponseInfo.ContentText + '{"message": "Who is John Galt?"}]';
       
       mRez.Free;
       DeleteFile('.gps');
    end
     else
     begin
       AResponseInfo.ContentText := '<div>' + mErrorsInModel + '</div>';
       For i := 0 to (mLst.Count-1) do
        AResponseInfo.ContentText := AResponseInfo.ContentText + '<div>' + mLst.Strings[i] + '</div>';
        mLst.Clear;
      end;
  end;

  if (ARequestInfo.Command = 'GET') And (ARequestInfo.Document = '/') then
    begin

      AResponseInfo.ContentText :=
     '<form action="/" method="post">' +
        'GPSS kod: <textarea rows="10" columns="100" name="gpss"></textarea><br>' +
        '<input type="submit" value="Posalji">' +
      '</form>';


    end;



end;

procedure TSForm.FormClose(Sender: TObject; var Action: TCloseAction);
begin
IdHTTPServer1.Active := False;
end;

procedure TSForm.IdHTTPServer1CreatePostStream(ASender: TIdPeerThread;
  var VPostStream: TStream);
begin
  VPostStream := TStringStream.Create('');
end;

end.
