{ procedura koja se koristi za snimanj sadrzaja memorije odnosno blokova}
procedure snimi_blokove(vreme : string);
var tf : textfile;
     i : integer;

procedure snimiEntAdr(myEA : Ent_Adr);
begin
    writeLn(tf, ord(myEA.Typ));
    writeLn(tf, myEA.Adrs);
end;

procedure snimiSNA (mySNA : sna);
begin
    writeLn(tf, mySNA.S_Cod);
    snimiEntAdr(mySNA.S_Adr);
    writeLn(tf, mySNA.Values);
end;

procedure snimiOperand(myOp : operand);
begin
    writeLn(tf, ord(myOp.Typ));
    writeLn(tf, myOp.N_op);
    snimiSNA(myOp.S_op);
    snimiEntAdr(myOp.A_Op);
end;

procedure snimiTraPtr(tPtr : Tra_Ptr);
begin
    if tPtr <> nil then
    begin
    if tPtr^.Flink = nil then
      writeLn(tf, 'Flink: nil');
    if tPtr^.Blink = nil then
      writeLn(tf, 'Blink: nil');
    writeLn(tf, tPtr^.M1);
    writeLn(tf, tPtr^.Mp);
    writeLn(tf, tPtr^.Bdt);
    writeLn(tf, tPtr^.Cb);
    writeLn(tf, tPtr^.Nba);
    writeLn(tf, tPtr^.Qin);
    writeLn(tf, tPtr^.Pr);
    writeLn(tf, tPtr^.Sf);
    writeLn(tf, ord(tPtr^.Ch));
    if tPtr^.P = nil then
      writeLn(tf, 'P: nil');
    end;
end;

procedure snimiBlkPtr(bPtr : Blk_Ptr);
var i : integer;
begin
    writeLn(tf, ord(bPtr^.Op_Cod));
    for i := 1 to Max_Opr do
        begin
            writeLn(tf, ord(bPtr^.OP[i].Typ));
            writeLn(tf, bPtr^.OP[i].N_op);
            snimiSNA(bPtr^.OP[i].S_op);
            snimiEntAdr(bPtr^.OP[i].A_Op);
        end;
    writeLn(tf, bPtr^.N);
    writeLn(tf, bPtr^.W);
end;

procedure snimiChaPtr(cPtr : Cha_Ptr);
begin
    if cPtr^.Head = nil then
        writeLn(tf, 'Head: nil');
    if cPtr^.Tail = nil then
        writeLn(tf, 'Tail: nil');
    writeLn(tf, cPtr^.Ca);
    writeLn(tf, cPtr^.Cc);
    writeLn(tf, cPtr^.Ch);
    writeLn(tf, cPtr^.Cm);
    writeLn(tf, cPtr^.Ct);
    writeLn(tf, cPtr^.C0);
    writeLn(tf, cPtr^.Tht);
end;

procedure snimiFacPtr(fPtr : Fac_Ptr);
begin
    writeLn(tf, fPtr^.F);
    writeLn(tf, fPtr^.Fc);
    writeLn(tf, fPtr^.Fr);
    writeLn(tf, fPtr^.Ft);
    writeLn(tf, fPtr^.C0);
    writeLn(tf, fPtr^.Tht);
    if fPtr^.Tr = nil then
        writeLn(tf, 'Tr: nil');
    writeLn(tf, fPtr^.Sc);
    writeLn(tf, fPtr^.Pc);
    if fPtr^.Head = nil then
        writeLn(tf, 'Head: nil');
end;

procedure snimiFunPtr(fPtr : Fun_Ptr);
var i : integer;
begin
    writeLn(tf, fPtr^.V);
    snimiSNA(fPtr^.A);
    writeLn(tf, ord(fPtr^.Typ));
    writeLn(tf, fPtr^.N);
    for i := 1 to Max_Point do
        begin
            writeLn(tf, fPtr^.P[i].X);
            writeLn(tf, fPtr^.P[i].Y);
        end;
end;

procedure snimiRand(myRand : Rand);
begin
  writeLn(tf, myRand.Seed);
  writeLn(tf, myRand.Mult);
  writeLn(tf, myRand.Val);
end;

procedure snimiStoPtr(sPtr : Sto_Ptr);
begin
    writeLn(tf, sPtr^.R);
    writeLn(tf, sPtr^.S);
    writeLn(tf, sPtr^.Sa);
    writeLn(tf, sPtr^.Sc);
    writeLn(tf, sPtr^.Sr);
    writeLn(tf, sPtr^.Sm);
    writeLn(tf, sPtr^.St);
    writeLn(tf, sPtr^.Tht);
    writeLn(tf, sPtr^.C0);
end;

procedure snimiTabPTr(tPtr : Tab_Ptr);
var i : integer;
begin
    snimiSNA(tPtr^.Arg);
    writeLn(tf, tPtr^.Tb);
    writeLn(tf, tPtr^.Tc);
    writeLn(tf, tPtr^.Td);
    writeLn(tf, tPtr^.Sa);
    writeLn(tf, tPtr^.Ni);
    writeLn(tf, tPtr^.Si);
    writeLn(tf, tPtr^.Avo);
    for i := 1 to Max_Intr do
        begin
            writeLn(tf, tPtr^.I[i].Ul);
            writeLn(tf, tPtr^.I[i].Off);
        end;
end;

procedure snimiQuePtr(qPtr : Que_Ptr);
begin
    writeLn(tf, QPtr^.Q);
    writeLn(tf, QPtr^.Qa);
    writeLn(tf, QPtr^.Qc);
    writeLn(tf, QPtr^.Qm);
    writeLn(tf, QPtr^.Qt);
    writeLn(tf, QPtr^.Qx);
    writeLn(tf, QPtr^.Qz);
    writeLn(tf, QPtr^.Tht);
    writeLn(tf, QPtr^.Thz);
    writeLn(tf, QPtr^.C0);
    writeLn(tf, QPtr^.Cz);
end;

procedure snimiVarPtr(vPtr : Var_Ptr);
var i : integer;
begin
    writeLn(tf, vPtr^.V);
    writeLn(tf, vPtr^.N);
     for i := 1 to Max_Vop do
        begin
            writeLn(tf, ord(vPtr^.P_N[i].Typ));
            writeLn(tf, ord(vPtr^.P_N[i].Vopr));
            writeLn(tf, ord(vPtr^.P_N[i].Vop.Typ));
            writeLn(tf, vPtr^.P_N[i].Vop.N_op);
            snimiSNA(vPtr^.P_N[i].Vop.S_op);
            snimiEntAdr(vPtr^.P_N[i].Vop.A_Op);
        end;
end;

begin
    assignfile(tf, 'Blokovi.txt');
    append(tf);
    writeln(tf, 'BLOK:' + vreme);
    writeLn(tf, 'Globalne promenjive:');
    writeLn(tf, 'Ca: ', Ca);
    writeLn(tf, 'C1: ', C1);
    writeLn(tf, 'Scf: ', ord(Scf));
    writeLn(tf, 'Lab: ', lab);
    writeln(tf, 'Opc: ', ord(opc));
    writeLn(tf, 'Trans: ');
    snimiTraPtr(Trans);
    writeln(tf, 'Err_cod: ', err_cod);
    writeln(tf, 'i: ', i);
    writeln(tf, 'j: ', j);
    writeln(tf, 'k: ', k);
    writeln(tf, 'xx: ', xx);
    writeln(tf, 'yy: ', yy);
    writeLn(tf, 'Op: ');
    snimiOperand(op);
    writeln(tf, 'AD: ');
    snimiEntAdr(AD);
    writeln(tf, 'ch: ', ord(ch));
    writeln(tf, 'stop: ', ord(stop));
    writeln(tf, 'p_flg: ', ord(p_flg));
    writeln(tf, 'sim_flg: ', ord(sim_flg));
    writeln(tf, 'opn_flg: ', ord(opn_flg));
    writeln(tf, 'NR_OP');
    for i := 1 to Max_Opc do
        writeln(tf, 'NR_OP[',i,']: ', NR_OP[i]);
    writeln(tf, 'R');
    for i := 1 to Max_Opr do
        writeln(tf, 'R[',i,']: ', R[i]);
    writeln(tf, 'Obj_nm: ', obj_nm);
    writeln(tf, 'Rez_nm: ', rez_nm);

    i := 1;
    writeln(tf, 'Blokovi');
    while Blk_Tbl[i] <> nil do
        begin
            writeln(tf,'Blok ', i:4);
            snimiBlkPtr(blk_tbl[i]);
            inc(i);
        end;

    i := 1;
    writeln(tf, 'Lanci');
    while Cha_Tbl[i] <> nil do
        begin
            writeln(tf,'Lanac ', i:4);
            snimiChaPtr(Cha_tbl[i]);
            inc(i);
        end;

    i := 1;
    writeln(tf, 'Facility');
    while Fac_Tbl[i] <> nil do
        begin
            writeln(tf,'Facility ', i:4);
            snimiFacPtr(Fac_tbl[i]);
            inc(i);
        end;

    i := 1;
    writeln(tf, 'Funkcije');
    while Fun_Tbl[i] <> nil do
        begin
            writeln(tf,'Funkcija ', i:4);
            snimiFunPtr(Fun_tbl[i]);
            inc(i);
        end;

    writeLn(tf, 'Logs: ');
    for i := 1 to max_log do
        write(tf, ord(Log_Tbl[i]));
    writeLn(tf);

    writeLn(tf, 'Rands ');
    for i := 1 to max_rnd do
        begin
            writeLn(tf, 'Rand ', i:4);
            snimiRand(Rnd_Tbl[i]);
        end;

    writeLn(tf, 'IntRands ');
    for i := 1 to Max_Int_Rnd do
        begin
            writeLn(tf, 'IntRand ', i:4);
            snimiRand(IntRnd_Tbl[i]);
        end;

    writeLn(tf, 'Savs: ');
    for i := 1 to max_sav do
        writeln(tf,'Sav[',i,']:', Sav_Tbl[i]);

    i := 1;
    writeln(tf, 'Storage');
    while Sto_Tbl[i] <> nil do
        begin
            writeln(tf,'Storage ', i:4);
            snimiStoPtr(Sto_tbl[i]);
            inc(i);
        end;

    i := 1;
    writeln(tf, 'Tabs');
    while Tab_Tbl[i] <> nil do
        begin
            writeln(tf,'Tab ', i:4);
            snimiTabPtr(Tab_tbl[i]);
            inc(i);
        end;

    i := 1;
    writeln(tf, 'Queues');
    while Que_Tbl[i] <> nil do
        begin
            writeln(tf,'Queue ', i:4);
            snimiQuePtr(Que_tbl[i]);
            inc(i);
        end;

    i := 1;
    writeln(tf, 'Variables');
    while Var_Tbl[i] <> nil do
        begin
            writeln(tf,'Variable ', i:4);
            snimiVarPtr(Var_tbl[i]);
            inc(i);
        end;

    closefile(tf);
end;
