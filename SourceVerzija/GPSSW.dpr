program GPSSW;

uses
  Forms,
  fSplash in 'fSplash.pas' {Splash},
  tekst in 'tekst.pas',
  Main in 'Main.pas' {AForm},
  uFile in 'uFile.pas',
  fAbout in 'fAbout.pas' {About},
  UPrepareSimulation in 'UPrepareSimulation.pas',
  UAsm in 'UAsm.pas',
  UGpss87 in 'UGpss87.pas',
  UFormDijagram in 'UFormDijagram.pas' {FormDijagram},
  UGrafickiSimboli in 'UGrafickiSimboli.pas',
  ServerForm in 'ServerForm.pas' {SForm};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'GPSS for Windows';
  Application.CreateForm(TAForm, AForm);
  Application.CreateForm(TAbout, About);
  Application.CreateForm(TSplash, Splash);
  Application.CreateForm(TFormDijagram, FormDijagram);
  Application.CreateForm(TSForm, Server);
  Application.Run;
end.
