unit Main;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ComCtrls, Menus, AutoMenu, Grids, Chart, Buttons, tekst, TeeProcs,
  ExtCtrls, Series, TeEngine, uFile, UGpss87, ServerForm;

type
  TAForm = class(TForm)
    MainMenu1: TMainMenu;
    Program1: TMenuItem;
    Simulacija1: TMenuItem;
    PageControl: TPageControl;
    TabKod: TTabSheet;
    TabRezultati: TTabSheet;
    TabGreske: TTabSheet;
    Simuliraj1: TMenuItem;
    Stampajrezlutate1: TMenuItem;
    N1: TMenuItem;
    IskopirajrezultatenaClipboard1: TMenuItem;
    TabGraph: TTabSheet;
    TabPomoc: TTabSheet;
    Pomoc: TMemo;
    greske: TMemo;
    kod: TMemo;
    Rezultati: TRichEdit;
    AM: TAutoMenu;
    PageControl1: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    GroupBox4: TGroupBox;
    VrstaCB: TComboBox;
    GroupBox5: TGroupBox;
    mono: TCheckBox;
    Kopiraj: TBitBtn;
    Stampaj: TBitBtn;
    CB3d: TCheckBox;
    chart: TChart;
    Series1: TBarSeries;
    Panel2: TPanel;
    TabelaCB: TComboBox;
    sgTabela: TStringGrid;
    pcSenka: TPageControl;
    TabSenka: TTabSheet;
    Label1: TLabel;
    Graficki1: TMenuItem;
    UnosGPSSprogramaprekografickogblokdijagrama1: TMenuItem;
    Server1: TMenuItem;
    procedure AMUcitaj(naziv: String);
    procedure FormCreate(Sender: TObject);
    procedure PageControlChange(Sender: TObject);
    procedure AMNovi(Sender: TObject);
    procedure AMSnimi(naziv: String);
    procedure Simuliraj1Click(Sender: TObject);
    procedure Stampajrezlutate1Click(Sender: TObject);
    procedure IskopirajrezultatenaClipboard1Click(Sender: TObject);
    procedure TabelaCBChange(Sender: TObject);
    procedure VrstaCBChange(Sender: TObject);
    procedure monoClick(Sender: TObject);
    procedure KopirajClick(Sender: TObject);
    procedure StampajClick(Sender: TObject);
    procedure AMOProgramu(var dozvoli: Boolean);
    procedure kodKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CB3dClick(Sender: TObject);
    procedure AMKraj(Sender: TObject; var CanClose: Boolean);
    procedure FormActivate(Sender: TObject);
    procedure UnosGPSSprogramaprekografickogblokdijagrama1Click(
      Sender: TObject);
    procedure Server1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
      ApDir : string;
      procedure CistiDijagram;
      procedure DodajSeriju(c : TComponent; xleg, y : integer);
      procedure OsveziCB;

  end;

var
  AForm: TAForm;
  modif : boolean;
  boot : boolean;

implementation

uses fAbout, fSplash, UAsm, UFormDijagram;

{$R *.DFM}

procedure TAForm.FormCreate(Sender: TObject);
var cf : TFIleStream;nam : string;pom : string;i : integer;b: boolean;
begin
  pcSenka.Align := alClient;
    boot := true;
  pagecontrol.ActivePage := TabKod;
  AM.Aktiviraj;
  ApDir := ExtractFilePAth (Application.ExeName);
  rezultati.SelectAll;
  rezultati.Paragraph.FirstIndent := 30;
  rezultati.SelLength := 0;
  Cistidijagram;
  rezultati.lines.clear;
  modif := false;
  nam := apdir;
  if nam[length(nam)]<>'\' then nam := nam+'\';
  // pomoc.Lines.LoadFromFile(nam+mHelpFile);
  nam := nam+'config.dat';
  if not fileexists(nam) then exit;
  cf := TFileStream.Create (nam, fmOpenRead);
  pom := Font.name;
  cf.Read (i, sizeof(i));
  SetLength(pom, i);
  cf.Read (pom[1], i);
  cf.Read (b, sizeof(b));
  if b then font.Style := font.style + [fsbold];
  cf.Read (b, sizeof(b));
  if b then font.Style := font.style + [fsitalic];
  cf.free;
  Font.Name := pom;
end;
procedure TAForm.AMUcitaj(naziv: String);
begin
     if fileexists(naziv+'.gps') then
     begin
       Kod.lines.LoadFromFile(naziv+'.gps');
       rezultati.lines.clear;
       greske.lines.clear;
       if fileexists(naziv+'.lst') then
         Greske.Lines.LoadFromFile(Naziv+'.lst');
       if fileexists(naziv+'.rez') then
         Rezultati.Lines.LoadFromFile(Naziv+'.rez');
       pc.free;pc := nil;
       OsveziCB;
       pagecontrol.ActivePage := Tabkod;
       Caption := 'GPSS for Windows';
       if Naziv<>'' then
         Caption := Caption + ' : ' + extractfilename(naziv);
       modif := false;
       AM.Modifikovan := false;
       end
     else
        begin
            ShowMessage('Izabrana datoteka ne postoji!');
        end;
end;

procedure TAForm.PageControlChange(Sender: TObject);
begin
  case pagecontrol.ActivePage.pageindex of
       0 :  kod.SetFocus;
       1 :  greske.SetFocus;
       2 :  rezultati.SetFocus;
   end;
end;

procedure TAForm.AMNovi(Sender: TObject);
begin
     kod.lines.clear; rezultati.lines.clear; greske.lines.clear;
     pc.free;pc := nil; OsveziCB;pagecontrol.ActivePage := Tabkod;
     Caption := 'GPSS for Windows';
     modif := false;
end;
procedure TAForm.AMSnimi(naziv: String);
begin
  kod.lines.savetofile(naziv+'.gps');
  if Fileexists(naziv+'.lst') then deletefile(naziv+'.lst');
  if Fileexists(naziv+'.rez') then deletefile(naziv+'.rez');
  if not modif then
    begin
      greske.lines.savetofile(naziv+'.lst');
      rezultati.lines.savetofile(naziv+'.rez');
    end;
{  greske.lines.clear;
  rezultati.lines.clear;}
  pc.free; pc := nil; OsveziCB;
  Caption := 'GPSS for Windows';
  if Naziv<>'' then
     Caption := Caption + ' : ' + extractfilename(naziv);
  modif := false;
end;
procedure TAForm.Simuliraj1Click(Sender: TObject);
begin
{     if fileexists(AM.Naziv+'.obj') then deletefile(AM.Naziv+'.obj');
     if fileexists(AM.Naziv+'.rez') then deletefile(AM.Naziv+'.rez');}
     Rezultati.lines.clear;
     Greske.lines.clear;
     chdir (ApDir);
       kod.lines.SaveToFile('.gps');
     pagecontrol.ActivePage := TabGreske;

     if not GPSSASM_START('') then
       begin
         GPSS_START('', TabSenka);
         Rezultati.Lines := mRez;
         mRez.Free;
         DeleteFile('.gps');
         // Rezultati.Lines.LoadFromFile('.rez');
         pagecontrol.ActivePage := TabRezultati;
       end
     else
       ShowMessage (mErrorsInModel);
     greske.Lines := mLst;
     mLst.Free;
     // greske.Lines.LoadFromFile('.lst');, Nepotrebno posto se sada sve cuva u memoriji, BJ  6/7/2003
     osvezicb;
     if not AM.Modifikovan then
       begin
         // greske.lines.savetofile(AM.naziv+'.lst');
         // rezultati.lines.savetofile(AM.naziv+'.rez');
       end;
     modif := false;

     // da ucita iz tabela BJ 6/7/2003
     sgTabela.Visible := false;
     if ( TabelaCb.Items.Count >0 ) then
     begin
        TabelaCb.ItemIndex := 0;
        TabelaCBChange(Sender);
     end;

     
end;

procedure TAForm.Stampajrezlutate1Click(Sender: TObject);
begin
   if rezultati.lines.count < 2 then
      showmessage(mNothingToPrint)
   else
      Rezultati.print(format (mSimResults, [AM.naziv]));
end;
procedure TAForm.IskopirajrezultatenaClipboard1Click(Sender: TObject);
begin
     rezultati.SelectAll;
     rezultati.CopyToClipboard;
     rezultati.SelLength:=0;
end;

procedure TAform.OsveziCB;
var i : integer;
begin
   tabelacb.items.clear;
   vrstacb.items.clear;
   if assigned (pc) then
      for i := 0 to pc.PageCount-1 do
        tabelacb.items.add(pc.Pages[i].caption);
         {if pc.Pages[i].tag = 0 then
            tabelacb.items.add(pc.Pages[i].caption)
         else if pc.Pages[i].tag = 1 then
            tabelacb.items.add('Skladista');}
   CistiDijagram;
end;

procedure TAForm.TabelaCBChange(Sender: TObject);
var tabela : TStringGrid;
        i, j : integer;
begin

   vrstacb.items.clear;
   CistiDijagram;
   if tabelacb.ItemIndex<>-1 then
   begin
        // da ucita sadrzaj tabele iz dinamickih tabela BJ 6/7/2003
        sgTabela.Visible := true;
        tabela := TabSenka.Controls[0].Components[tabelacb.ItemIndex].Components[0] as TStringGrid;
        sgTabela.ColCount := tabela.ColCount;
        sgTabela.RowCount := tabela.RowCount;
        sgTabela.DefaultColWidth := tabela.DefaultColWidth;
        for i := 0 to tabela.RowCount - 1 do
          for j:= 0 to tabela.ColCount - 1 do
            sgTabela.Cells[j, i] := tabela.Cells[j, i];
      if pos (mTable, tabelacb.items[tabelacb.ItemIndex])=1 then
        begin
          vrstacb.items.add(mFrequencies);
          vrstacb.items.add(mPercentages);
          vrstacb.items.add(mCumulativePercentages);
        end
      else if (tabelacb.items[tabelacb.ItemIndex])=mStorages then
        begin
          vrstacb.items.add(mAverageContents);
          vrstacb.items.add(mAverageUtilization);
          vrstacb.items.add(mNoEntries);
          vrstacb.items.add(mAverageTimeTran);
        end
      else if (tabelacb.items[tabelacb.ItemIndex])=mFacilities then
        begin
          vrstacb.items.add(mAverageUtilization);
          vrstacb.items.add(mNoEntries);
          vrstacb.items.add(mAverageTimeTran);
        end
      else if (tabelacb.items[tabelacb.ItemIndex])=mQueues then
        begin
          vrstacb.items.add(mNoEntries);
          vrstacb.items.add(mZeroEntries);
          vrstacb.items.add(mAverageContents);
          vrstacb.items.add(mAverageTimeTran);
        end
      else if (tabelacb.items[tabelacb.ItemIndex])=mUserChains then
        begin
          vrstacb.items.add(mNoEntries);
          vrstacb.items.add(mAverageTimeTran);
          vrstacb.items.add(mAverageContents);
          vrstacb.items.add(mMaxContents);
        end
   end;

   // Da ubaci moguce grafike u zavisnosti od tabele, BJ 6/7/2003
   if ( VrstaCb.Items.Count >0 ) then
     begin
        VrstaCb.ItemIndex := 0;
        VrstaCBChange(Sender);
     end;

end;

procedure TAForm.VrstaCBChange(Sender: TObject);
begin
      if pos (mTable, tabelacb.items[tabelacb.ItemIndex])=1 then
        case vrstacb.itemindex of
          0 : {Frekvencije}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 1);
          1 : {Procenti}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 2);
          2 : {Kumulativni procenti}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 3);
        end
      else if tabelacb.items[tabelacb.ItemIndex]=mStorages then
        case vrstacb.itemindex of
          0{2} : {Prosecni sadr`aj}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 2);
          1{0} : {Prosecna iskoriscenost}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 3);
          2{1} : {Broj ulazaka}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 4);
          3 : {Prosecno zadrzavanje}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 5);
          end
      else if tabelacb.items[tabelacb.ItemIndex]=mFacilities then
        case vrstacb.itemindex of
          0 : {Prosecna iskoriscenost}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 1);
          1 : {Broj ulazaka}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 2);
          2 : {Prosecno zadrzavanje}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 3);
          end
      else if tabelacb.items[tabelacb.ItemIndex]=mQueues then
        case vrstacb.itemindex of
          2 : {Prosecni sadrzaj}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 2);
          1 : {Broj ulazaka bez z}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 4);
          0 : {Broj ulazaka}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 3);
          3 : {Prosecno zadrzavanje}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 6);
          end
      else if tabelacb.items[tabelacb.ItemIndex]=mUserChains then
        case vrstacb.itemindex of
          2 : {Prosecni sadrzaj}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 4);
          3 : {Maksimalno u redu}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 5);
          0 : {Broj ulazaka}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 1);
          1 : {Prosecno zadrzavanje}dodajSeriju(pc.Pages[tabelacb.ItemIndex].components[0], 0, 2);
          end;
      chart.Series[0].Title := vrstacb.items[vrstacb.itemindex];
end;


procedure TAFORM.CistiDijagram;
var i : integer;s : TChartSeries;
begin
    for i := 1 to chart.SeriesCount do
       begin
          s := chart.series[0];
          s.Clear;
{          chart.RemoveSeries(s);}
          {s.free;}
       end;
end;
procedure TAForm.DodajSeriju(c : TComponent; xleg, y : integer);
var tab : TStringgrid; ser : TChartSeries;i : integer;
begin
     tab := (c as TStringgrid);
     ser := chart.series[0];
     ser.Clear;
     for i := 1 to tab.rowcount-1 do
       begin
         if tab.cells[y, i]<>'' then
          ser.AddY(strtofloat(tab.cells[y, i]),tab.cells[xleg, i], clblue)
         else
          ser.AddY(0,tab.cells[xleg, i], clblue)
       end;
     ser.active := true;
     chart.MaxPointsPerPage := ser.Count;
end;
procedure TAForm.monoClick(Sender: TObject);
begin  chart.Monochrome := mono.checked; end;

procedure TAForm.KopirajClick(Sender: TObject);
begin     chart.CopyToClipboardBitmap;  end;

procedure TAForm.StampajClick(Sender: TObject);
begin     chart.Print;   end;

procedure TAForm.AMOProgramu(var dozvoli: Boolean);
begin
     dozvoli := false;
     // Nov About, BJ
     {ShowMessage('GPSS for Windows95 ver. 3.03'+#13+
                  '(C) B.Radenkovic, 1985, 1987, 1992, 1995, 1997, 2000'+#13+
                  'SimLab, FON 2000.'+#13+
                  'Adapted for Windows by Milos Dragovic'+#13); }
     About.ShowModal;
end;

procedure TAForm.kodKeyUp(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if kod.Modified then
      begin
        AM.Modifikovan:=true;
        modif := true;
      end;
end;
procedure TAForm.CB3dClick(Sender: TObject);
begin
   chart.View3D := CB3d.Checked;
end;

procedure TAForm.AMKraj(Sender: TObject; var CanClose: Boolean);
var cf : TFIleStream;nam : string;pom : string;i : integer;b: boolean;
begin
   nam := apdir;
   if nam[length(nam)]<>'\' then nam := nam+'\';
   nam := nam+'config.dat';
   cf := TFileStream.Create (nam, fmCreate);
   pom := Font.name;
   i := length(pom);
   cf.Write (i, sizeof(i));
   cf.Write (pom[1], i);
   b := fsbold in font.Style;
   cf.Write (b, sizeof(b));
   b := fsitalic in font.Style;
   cf.Write (b, sizeof(b));
   cf.free;
end;

procedure TAForm.FormActivate(Sender: TObject);
var x , y : integer;
begin
    if boot then
        begin
            x := Aform.Left;
            y := Aform.Top;
            Aform.Left := 2000;
            Aform.Top := 2000;
            Splash.ShowModal;
            Aform.Left := x;
            Aform.Top := y;
            boot := false;
        end;
end;

procedure TAForm.UnosGPSSprogramaprekografickogblokdijagrama1Click(
  Sender: TObject);
begin
  FormDijagram.ShowModal;
end;

procedure TAForm.Server1Click(Sender: TObject);
begin
  Server.Show;
end;

end.
