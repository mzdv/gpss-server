Unit UGpss87 ;   {JA PROMENIO}

interface                  {JA PROMENIO}

  uses dialogs, sysutils, comctrls, classes, controls, grids, tekst, uFile;

          CONST
// Konstante su promenjene u promenjive, zbog dinamickih nizova, BJ 6/7/2003
// Max_blk = 1500;
// Max_cha = 50;
// Max_fac = 50;
// Max_fun = 50;
// Max_log = 250;
// Max_sav = 250;
// Max_sto = 50;
// Max_rnd = 8;
// Max_int_rnd = 3;
// Max_tab = 50;
// Max_que = 50;
// Max_var = 50;

 Max_intr = 100;
 Max_par = 25;
 Max_vop = 50;
 Max_opc = 29;
 Max_opr = 5;
 Max_point = 40;

 Type

 // Enumerativni tip za Operation Code
    OperationCodes = (
                        ocNull,                 {  0 }
                        ocAdvance,              {  1 }
                        ocAssign,               {  2 }
    	                ocClear,                {  3 }
                        ocDepart,               {  4 }
    	                ocStop,                 {  5 }
                        ocEnter,                {  6 }
    	                ocRead_fun,             {  7 }
                        ocGate,                 {  8 }
                        ocError21,              {  9 }
   	                    ocRead_ini,             { 10 }
       	                ocLeave,                { 11 }
       	                ocLink,                 { 12 }
                        ocLogic,                { 13 }
                        ocMark,                 { 14 }
                        ocQue,                  { 15 }
       	                ocRelease,              { 16 }
       	                ocReset,                { 17 }
       	                ocSavevalue,            { 18 }
   	                    ocSeize,                { 19 }
   	                    ocSim_flg,              { 20 }
   	                    ocStart,                { 21 }
   	                    ocRead_sto,             { 22 }
   	                    ocRead_tab,             { 23 }
       	                ocTabulate,             { 24 }
       	                ocTerminate,            { 25 }
       	                ocTest,                 { 26 }
       	                ocTransfer,             { 27 }
       	                ocUnlink,               { 28 }
   	                    ocRead_var);            { 29 }

 Blk_ptr = ^Blok;
 Cha_ptr = ^Chain;
 Fac_ptr = ^Facility;
 Fun_ptr = ^Funct;
 Sto_ptr = ^Storage;
 Tab_ptr = ^Table;
 Tra_ptr = ^Transaction;
 Par_ptr = ^Parametr;
 Que_ptr = ^Queue;
 Var_ptr = ^Variable;

 Op_typ  = (Num_op,Sna_op,Adr_op);
 Fun_typ = (Con,Dis);
 Adr_typ = (Direct,Indirect);
 Vop_typ = (Voperator,Voperand);
 Cha_typ = (CEC_ch,FEC_ch,USR_ch,INT_ch);

 Ent_adr = RECORD
  Typ : Adr_typ;
  Adrs : LONGINT
 END; {Ent_adr}

  Sna = RECORD
    S_cod : LONGINT;
    S_adr : Ent_adr;
    Values : REAL
  END; {Sna}
{}
 Operand = RECORD
  CASE Typ : Op_typ OF
    Num_op : (N_op : LONGINT);
    Sna_op : (S_op : Sna);
    Adr_op : (A_op : Ent_adr)
 END; {Operand}
{}
 Blok = RECORD
  Op_cod : OperationCodes; // Promenjeno iz longint u OperationCodes
      Op : ARRAY [1..Max_opr] OF Operand;
       N : LONGINT; {Ukupan br ulaza u blok}
       W : LONGINT  {Tekuci broj trans. u bloku}
 END; {Blok}
{}
 Chain = RECORD
    Head : Tra_ptr; {Pokazivac na prvu trans. u korisn. redu}
    Tail : Tra_ptr; {Pokazivac na zadnju trans. u korisn. redu}
      Ca : REAL; {Srednji sadrzaj}
      Cc : LONGINT; {Ukupni broj prijavljivanja u red}
      Ch : LONGINT; {Tekuci sadrzaj reda}
      Cm : LONGINT; {Max. sadrzaj reda}
      Ct : REAL; {Srednje vreme po trans.}
      C0 : LONGINT; {Vreme zadnje promene duzine reda}
     Tht : REAL     {Ukupno vreme angazovanja reda}
 END; {Chain}
{}
 Facility = RECORD
       F : 0..1; {Status uredjaja F = 0 Slobodan F = 1 zauzet }
      Fc : LONGINT; {Broj ulazaka u uredjaj}
      Fr : REAL; {Prosecno koriscenje uredjaja}
      Ft : REAL; {Prosecno vreme po transakciji}
      C0 : LONGINT; {Vreme zadnjeg zaposedanja uredjaja}
     Tht : REAL;    {Ukupno vreme koriscenja uredjaja}
      Tr : Tra_ptr; {Pointer na transakciju u uredjaju}
      Sc : LONGINT; {Brojac zauzimanja uredjaja}
      Pc : LONGINT; {Brojac prijempcija}
    Head : Tra_ptr {Pointer na vrh staka prijempcija}
 END; {Facility}
{}
 Parametr = ARRAY [1..Max_par] OF LONGINT;
{}
 Point = RECORD
       X : REAL;
       Y : REAL
 END; {Point}
{}
 Funct = RECORD
       V : REAL; {Vrednost funkcije}
       A : Sna ; {Argument funkcije}
     Typ : Fun_typ;
       N : LONGINT; {Br. tacaka funkcije}
       P : ARRAY [1..Max_point] OF Point
 END; {Funct}
{}
 Rand = RECORD
    Seed : LONGINT; {Seme generatora slucajnih brojeva}
    Mult : LONGINT; {Multiplikator}
     Val : REAL  {Vrednost generatora 0 - 999.999}
 END; {Rand}
{}
 Storage = RECORD
       R : LONGINT; {Kapacitet}
       S : LONGINT; {Tekuci sadrzaj}
      Sa : REAL; {Prosecni sadrzaj}
      Sc : LONGINT; {Broj ulazaka u skladiste}
      Sr : REAL; {Prosecno koriscenje}
      Sm : LONGINT; {Max. sadrzaj}
      St : REAL; {Prosecno vreme po transakciji}
     Tht : REAL;    {Ukupno vreme angazovanja skladista}
      C0 : LONGINT  {Vreme zadnje promene kapaciteta}
 END; {Storage}
{}
 Interval = RECORD
      Ul : LONGINT; {Gornja granica}
     Off : LONGINT  {Observed frequency}
 END; {Interval}
{}
 Transaction = RECORD
   Flink : Tra_ptr; {Pokazivac na narednu trans. u listi}
   Blink : Tra_ptr; {Pokazivac na prethodnu trans. u listi}
      M1 : LONGINT; {Vreme boravka u modelu}
      Mp : LONGINT; {Vreme ulaska u MARK}
     Bdt : LONGINT; {Vreme polaska iz bloka}
      Cb : LONGINT; {Tekuci blok}
     Nba : LONGINT; {Naredni blok}
     Qin : LONGINT; {Vreme ulaska u queue}
      Pr : LONGINT; {Prioritet}
      Sf : BOOLEAN; {Scan flag}
      Ch : Cha_typ; {Indikator liste u koju je ulancana trans.}
       P : Par_ptr  {Pointer na parametre transakcije}
 END; {Transaction}
{}
 Table = RECORD
     Arg : Sna; {Argument tabele}
      Tb : REAL; {Srednja vrednost argumenta}
      Tc : LONGINT; {Brojac ulazaka u tabelu}
      Td : REAL; {Standardna devijacija argumenta}
      Sa : REAL; {Suma argumenata}
      Ni : LONGINT; {Broj intervala tabele}
      Si : LONGINT; {Sirina intervala tabele}
     Avo : REAL;    {Srednja vrdnost Overflowa}
       I : ARRAY [1..Max_intr] OF Interval
 END; {Table}
{}
 Queue = RECORD
       Q : LONGINT; {Tekuci sadrzaj}
      Qa : REAL;{Srednji sadrzaj}
      Qc : LONGINT; {Brojac zaposedanja reda}
      Qm : LONGINT; {Maksimalni sadrzaj reda}
      Qt : REAL;{Prosecno vreme/trans}
      Qx : REAL;{Prosecno $vreme/trans}
      Qz : LONGINT; {Brojac nultih prolaza}
     Tht : REAL;    {Ukupno vreme cekanja u redu}
     Thz : REAL;    {Ukupno vreme cekanja bez nul. prolaza}
      C0 : LONGINT; {Vreme zadnje promene duzine reda}
      Cz : LONGINT {Vreme zadnje promene duzine reda bez nul.pr.}
 END; {Queue}
{}
 Vop = RECORD
  CASE Typ : Vop_typ OF
    Voperator : (Vopr : CHAR);
    Voperand  : (Vop  : Operand)
 END; {Vop}
{}
 Variable = RECORD
       V : REAL   ; {Vrednost promenljive}
       N : LONGINT; {Broj elemenata poljske notacije}
     P_N : ARRAY [1..Max_vop] OF Vop
 END; {Variable}
{}
 String25 = STRING[25];
{}

VAR
     Ca : LONGINT; {Sat apsolutnog vremena simulacije}
     C1 : LONGINT; {Sat relativnog vremena simulacije}
     Tc : LONGINT; {Terminacioni brojac}
    Scf : BOOLEAN; {Scan control flag}
    Lab : LONGINT; {Bafer labele}
    Opc : OperationCodes;    // promenjeno iz LONGINT u OperationCodes, BJ 6/7/2003
                            // {Bafer operacionog koda naredbe}
  Trans : Tra_ptr; {Pointer na transakciju koja se obradjuje}
  err_cod : LONGINT; {Kod greske }
  i,j,k : LONGINT; {Promenljive za tekucu upotrebu}
  xx,yy : BYTE; {Koordinate kursora za clock}
     Op : Operand; {Bafer operanda naredbe}
     Ad : Ent_adr; {Bafer Adrese entiteta}
     Ch : CHAR;    {Karakter za opstu upotrebu}
   Stop : BOOLEAN; {Flag zavrsetka programa}
  P_flg : BOOLEAN; {Flag stampanja rezultata simulacije}
Sim_flg : BOOLEAN; {Flag startovanje simulacije}
Opn_flg : BOOLEAN; {Flag otvorene datoteke rezultata}
  Nr_op : ARRAY [1..Max_opc] OF 0..Max_opr;{Niz broja operanada}
      R : ARRAY [1..Max_opr] OF REAL;
Obj_nm  : String;
Rez_nm  : String;
{}
{ Deklaracija tabela }
{}
    Max_Log : integer;
    Max_Sav : integer;
    Max_Sto : integer;
    Max_Fac : integer;
    Max_Blk : integer;
    Max_Cha : integer;
    Max_Fun : integer;
    Max_Rnd : integer;
    Max_int_rnd : integer;
    Max_Tab : integer;
    Max_que : integer;
    Max_var : integer;
    Blk_tbl : ARRAY OF Blk_ptr;
    Cha_tbl : ARRAY OF Cha_ptr;
    Fac_tbl : ARRAY OF Fac_ptr;
    Fun_tbl : ARRAY OF Fun_ptr;
    Log_tbl : ARRAY OF BOOLEAN;
    Rnd_tbl : ARRAY OF Rand;
 IntRnd_tbl : ARRAY OF Rand;
    Sav_tbl : ARRAY OF LONGINT;
    Sto_tbl : ARRAY OF Sto_ptr;
    Tab_tbl : ARRAY OF Tab_ptr;
    Que_tbl : ARRAY OF Que_ptr;
    Var_tbl : ARRAY OF Var_ptr;
{}
 Cec : RECORD  {Lista tekucih dogadjaja}
     Head : Tra_ptr;
     Tail : Tra_ptr
 END; {Cec}
{}
 Fec : RECORD {Lista buducih dogadjaja}
     Head : Tra_ptr;
     Tail : Tra_ptr
 END; {Cec}
 krug : longint;
{}
// Idemo preko StringList, pa ne treba datoteka, BJ 6/7/2003
// Re_f,,Ob_f,
INPUT,OUTPUT,Err_f : TEXT;

        //*BojanNenadJovicic jul, avgust 2003.*
        //izvucene deklaracije u interface deo da bi bile dostupne
        //proceduri prepareSimulation
        counter, ocounter : integer;
        procedure NapraviPc;
        procedure GPSS_START(OBJNAME : String; Kontejner : TWinControl);
        function Int2OC(znak : integer) : OperationCodes;
        procedure Extend_Blk_Tbl;
        procedure Error (Err : INTEGER);
        procedure initBlkPtr(var bPtr : Blk_Ptr);
        procedure Extend_Fun_Tbl;
        procedure Extend_Sav_Tbl;
        procedure Extend_Tab_Tbl;
        procedure Extend_Var_Tbl;
        procedure Extend_Sto_Tbl;
        procedure initFacPtr(var fPtr : Fac_Ptr);
        procedure initSNA (var mySNA : sna);
        procedure initFunPtr(var fPtr : Fun_Ptr);
        procedure initTabPTr(var tPtr : Tab_Ptr);
        procedure initVarPtr(var vPtr : Var_Ptr);
        PROCEDURE Op_val (Op : Operand;VAR Val : REAL);

var
  Roditelj : TWinControl; PC : TPageControl;

implementation                          {JA PROMENIO}
uses UPrepareSimulation;//*BojanNenadJovicic jul, avgust 2003.*
{uses crt; }{Koriste se rutine za rad sa ekranom}{JA PROMENIO}

function StrExp (in_str : string; n : integer) : string;
var tmp_str : string;
    i : integer;
begin
    tmp_str := in_str;
    for i := length(tmp_str) to n do
        tmp_str := ' ' + tmp_str;
    result := tmp_str;
end;

PROCEDURE Error (Err : INTEGER);
{}
VAR Erf_name : String[25];
    Err_msg  : String[64];
    i : integer;
{}
BEGIN {Error}
{  WRITELN;}
{  WRITELN ('**** Error ',Err:3);}
{  WRITELN;}
  FOR i := 1 TO 4 DO   {Zvucna oznaka greske}
   BEGIN
{    Sound (1000);}    {JA PROMENIO}
{    delay (500);
    Sound (750);
    delay (500);}       {JA PROMENIO}
    beep;
   END;
{  NoSound;} {JA PROMENIO}

  Err_cod := Err;
  Stop := True;
  Erf_name := 'GPSS.ERR';
  ASSIGN (Err_f,Erf_name);
  RESET (Err_f);
  FOR i := 1 TO Err_cod DO READLN (Err_f,Err_msg);
      {WRITELN (Err_msg);}
      ShowMessage('**** Error '+inttostr(Err)+#13+Err_msg);
  CLOSE (Err_f)
END; {Error}

{I INIT.TXT}
// Funkcija za konverziju iz broja (kod datoteke) u odgovarajuci enumerativni, BJ 6/7/2003
function Int2OC(znak : integer) : OperationCodes;
begin
                case znak of
                        0 : Result :=ocNull;                  {  0 }
                        1 : Result :=ocAdvance;               {  1 }
                        2 : Result := ocAssign;               {  2 }
    	                3 : Result := ocClear;                {  3 }
                        4 : Result := ocDepart;               {  4 }
    	                5 : Result := ocStop;                 {  5 }
                        6 : Result := ocEnter;                {  6 }
    	                7 : Result := ocRead_fun;             {  7 }
                        8 : Result := ocGate;                 {  8 }
                        9 : Result := ocError21;              {  9 }
   	                   10 : Result := ocRead_ini;             { 10 }
       	               11 : Result := ocLeave;                { 11 }
       	               12 : Result := ocLink;                 { 12 }
                       13 : Result := ocLogic;                { 13 }
                       14 : Result := ocMark;                 { 14 }
                       15 : Result := ocQue;                  { 15 }
       	               16 : Result := ocRelease;              { 16 }
       	               17 : Result := ocReset;                { 17 }
       	               18 : Result := ocSavevalue;            { 18 }
   	                   19 : Result := ocSeize;                { 19 }
   	                   20 : Result := ocSim_flg;              { 20 }
   	                   21 : Result := ocStart;                { 21 }
   	                   22 : Result := ocRead_sto;             { 22 }
   	                   23 : Result := ocRead_tab;             { 23 }
       	               24 : Result := ocTabulate;             { 24 }
       	               25 : Result := ocTerminate;            { 25 }
       	               26 : Result := ocTest;                 { 26 }
       	               27 : Result := ocTransfer;             { 27 }
       	               28 : Result := ocUnlink;               { 28 }
   	                   29 : Result := ocRead_var;             { 29 }
                        end;

end;

PROCEDURE Init;
BEGIN {Init}
    i := 0;
    krug := 0;
    // Dinamicki i pocetna velicina svih povecana 2 puta
    Max_Log := 500;
    SetLength(Log_Tbl, Max_Log + 1);
    Max_Sav := 500;
    SetLength(Sav_Tbl, Max_Sav + 1);
    Max_Sto := 100;
    SetLength(Sto_Tbl, Max_Sto + 1);
    Max_Fac := 100;
    SetLength(Fac_Tbl, Max_Fac + 1);
    Max_Blk := 3000;
    SetLength(Blk_Tbl, Max_Blk + 1);
    Max_Cha := 100;
    SetLength(Cha_Tbl, Max_Cha + 1);
    Max_Fun := 100;
    SetLength(Fun_Tbl, Max_Fun + 1);
    Max_Rnd := 30;
    SetLength(Rnd_Tbl, Max_Rnd + 1);
    Max_int_rnd := 20;
    SetLength(IntRnd_Tbl, Max_Int_Rnd + 1);
    Max_tab := 100;
    SetLength(Tab_Tbl, Max_Tab + 1);
    Max_que := 100;
    SetLength(Que_Tbl, Max_Que + 1);
    Max_var := 100;
    SetLength(Var_Tbl, Max_Var + 1);

{ Definisanje brojeva operanada u naredbi }

   Nr_op [1] := 2;
   Nr_op [2] := 3;
   Nr_op [3] := 5;
   Nr_op [4] := 1;
   Nr_op [5] := 0;
   Nr_op [6] := 1;
   Nr_op [7] := 2;
   Nr_op [8] := 3;
   Nr_op [9] := 5;
   Nr_op [10] := 2;
   Nr_op [11] := 1;
   Nr_op [12] := 2;
   Nr_op [13] := 2;
   Nr_op [14] := 0;
   Nr_op [15] := 1;
   Nr_op [16] := 1;
   Nr_op [17] := 0;
   Nr_op [18] := 3;
   Nr_op [19] := 1;
   Nr_op [20] := 0;
   Nr_op [21] := 2;
   Nr_op [22] := 1;
   Nr_op [23] := 4;
   Nr_op [24] := 1;
   Nr_op [25] := 1;
   Nr_op [26] := 4;
   Nr_op [27] := 3;
   Nr_op [28] := 4;
   Nr_op [29] := 1;

{ Postavljanje pocetnih vrednosti promenljivih }

   Ca := 0;
   C1 := 0;
   Tc := 0;
   Lab := 0;
   Opc := ocNull; // Bilo je := 0, BJ 6/7/2003
   Err_cod := 0;
   Stop := FALSE;
   P_flg := FALSE;
   Sim_flg := FALSE;
   Opn_flg := FALSE;
   Cec.Head := NIL;
   Cec.Tail := NIL;
   Fec.Head := NIL;
   Fec.Tail := NIL;

   FOR i := 1 TO Max_blk DO Blk_tbl [i] := NIL;
   FOR i := 1 TO Max_cha DO Cha_tbl [i] := NIL;
   FOR i := 1 TO Max_fac DO Fac_tbl [i] := NIL;
   FOR i := 1 TO Max_fun DO Fun_tbl [i] := NIL;
   FOR i := 1 TO Max_log DO Log_tbl [i] := FALSE;
   FOR i := 1 TO Max_rnd DO Rnd_tbl [i].Mult := 16807;
   FOR i := 1 TO Max_int_rnd DO IntRnd_tbl [i].Mult := 16807;
   FOR i := 1 TO Max_sav DO Sav_tbl [i] := 0;
   FOR i := 1 TO Max_sto DO Sto_tbl [i] := NIL;
   FOR i := 1 TO Max_tab DO Tab_tbl [i] := NIL;
   FOR i := 1 TO Max_que DO Que_tbl [i] := NIL;
   FOR i := 1 TO Max_var DO Var_tbl [i] := NIL;

{ Dodeljivanje pocetnih vrednosti semena generatora sl. br.}

   Rnd_tbl [1].Seed := 5;
   Rnd_tbl [2].Seed := 7;
   Rnd_tbl [3].Seed := 9;
   Rnd_tbl [4].Seed := 11;
   Rnd_tbl [5].Seed := 13;
   Rnd_tbl [6].Seed := 15;
   Rnd_tbl [7].Seed := 17;
   Rnd_tbl [8].Seed := 19;
   IntRnd_tbl [1].Seed := 21;
   IntRnd_tbl [2].Seed := 23;
   IntRnd_tbl [3].Seed := 25;
{   Dodeljivanje imena datotekama. }
{       WRITELN;
       WRITELN ('GPSS/FON Ver. 2.01,  Simulating phase.');
       WRITELN ('(C) B.Radenkovic,   1985, 1987, 1992, 1995.');
       WRITELN ;}

{   IF paramcount = 0 THEN
     BEGIN
       WRITE ('GPSS >');           Nema cl parametara !!!!!!!!!!
       READLN (Obj_nm);
     END
      ELSE Obj_nm := paramstr(1); }{Postoje parametri u komandnoj liniji}

   i := POS ('.',Obj_nm) ;
   IF i=0 THEN i:=LENGTH(Obj_nm) ELSE Delete (Obj_nm,i,LENGTH(Obj_nm)-i+1);

   Rez_nm := COPY (Obj_nm,1,LENGTH(Obj_nm));
   Obj_nm := CONCAT (Obj_nm,'.OBJ');
   Rez_nm := CONCAT (Rez_nm,'.REZ');

   // ASSIGN (Ob_f,Obj_nm);
   // ASSIGN (Re_f,Rez_nm);
   mRez := TMSLst.Create;
{$I-}
   // RESET (Ob_f);
{$I+}
   IF NOT (IOresult = 0) THEN
    BEGIN
      {WRITELN;} ShowMessage('**** File not found');
{      WRITELN ('**** File not found');}
      {HALT }
    END ELSE
     BEGIN
{      WRITELN;
      WRITELN ('Simulation in progress, please wait !');
      WRITELN;
      WRITE ('Simulation clock : ');}
{      xx := WhereX;
      yy := WhereY;} {JA PROMENIO}
     END
END;{Init}
{I Init.pas}       // Procedure za inicijalizacuju dinamickih struktura
// Procedure za inicijalizaciju dinamicki stvorenih promenjivih, zbog pojavljivanja cudnih gresaka, BJ 6/8/2003
procedure initSNA (var mySNA : sna);
begin
    mySNA.S_Cod := 0;
    mySNA.S_Adr.Typ := Direct;
    mySNA.S_Adr.Adrs := 0;
    mySNA.Values := 0;
end;

procedure initFunPtr(var fPtr : Fun_Ptr);
var i : integer;
begin
    fPtr^.V := 0;
    initSNA(fPtr^.A);
    fPtr^.Typ := Con;
    fPtr^.N := 0;
    for i := 1 to Max_Point do
        begin
            fPtr^.P[i].X := 0;
            fPtr^.P[i].Y := 0;
        end;
end;

procedure initTabPTr(var tPtr : Tab_Ptr);
var i : integer;
begin
    initSNA(tPtr^.Arg);
    tPtr^.Tb := 0;
    tPtr^.Tc := 0;
    tPtr^.Td := 0;
    tPtr^.Sa := 0;
    tPtr^.Ni := 0;
    tPtr^.Si := 0;
    tPtr^.Avo := 0;
    for i := 1 to Max_Intr do
        begin
            tPtr^.I[i].Ul := 0;
            tPtr^.I[i].Off := 0;
        end;
end;

procedure initVarPtr(var vPtr : Var_Ptr);
var i : integer;
begin
    vPtr^.V := 0;
    vPtr^.N := 0;
     for i := 1 to Max_Vop do
        begin
            vPtr^.P_N[i].Typ := VOperator;
            vPtr^.P_N[i].Vopr := #0;
            vPtr^.P_N[i].Vop.Typ := Num_Op;
            vPtr^.P_N[i].Vop.N_op := 0;
            initSNA(vPtr^.P_N[i].Vop.S_op);
            vPtr^.P_N[i].Vop.A_Op.Typ := Direct;
            vPtr^.P_N[i].Vop.A_Op.Adrs := 0;
        end;
end;

procedure initFacPtr(var fPtr : Fac_Ptr);
begin
    fPtr^.F := 0;
    fPtr^.Fc := 0;
    fPtr^.Fr := 0;
    fPtr^.Ft := 0;
    fPtr^.C0 := 0;
    fPtr^.Tht := 0;
    fPtr^.Tr := nil;
    fPtr^.Sc := 0;
    fPtr^.Pc := 0;
    fPtr^.Head := nil;
end;

procedure initBlkPtr(var bPtr : Blk_Ptr);
var i : integer;
begin
    bPtr^.Op_Cod := ocNull;
    for i := 1 to Max_Opr do
        begin
            bPtr^.OP[i].Typ := Num_Op;
            bPtr^.OP[i].N_op := 0;
            initSNA(bPtr^.OP[i].S_op);
            bPtr^.OP[i].A_Op.Typ := Direct;
            bPtr^.OP[i].A_Op.Adrs := 0;
        end;
    bPtr^.N := 0;
    bPtr^.W := 0;
end;

procedure initTraPtr(var tPtr : Tra_Ptr);
begin
    tPtr^.Flink := nil;
    tPtr^.Blink := nil;
    tPtr^.M1 := 0;
    tPtr^.Mp := 0;
    tPtr^.Bdt := 0;
    tPtr^.Cb := 0;
    tPtr^.Nba := 0;
    tPtr^.Qin := 0;
    tPtr^.Pr := 0;
    tPtr^.Sf := false;
    tPtr^.Ch := CEC_ch;
    tPtr^.P := nil;
end;

procedure initParPtr(var pPtr : Par_Ptr);
var i : integer;
begin
    for i := 1 to Max_Par do
        pPtr^[i] := 0;
end;

procedure initChaPtr(var cPtr : Cha_Ptr);
begin
    cPtr^.Head := nil;
    cPtr^.Tail := nil;
    cPtr^.Ca := 0;
    cPtr^.Cc := 0;
    cPtr^.Ch := 0;
    cPtr^.Cm := 0;
    cPtr^.Ct := 0;
    cPtr^.C0 := 0;
    cPtr^.Tht := 0;
end;
{}
{I Extend.pas}     // Procedure za povecavanje velicine odgovarajucih tabela
// Procedure za prosirivanje dinamicnkih nizova, BJ 6/7/2003
procedure Extend_Log_Tbl;
var i : integer;
begin
    try
        SetLength(Log_Tbl, round (Max_Log * 1.1) + 1);
        for i := Max_Log to round (Max_Log * 1.1)
            do Log_tbl [i] := FALSE;
        Max_Log := round(Max_Log * 1.1);
    except
        Error (34)
    end;
end;

procedure Extend_Sav_Tbl;
var i : integer;
begin
    try
        SetLength(Sav_Tbl, round (Max_Sav * 1.1) + 1);
        for i := Max_Sav to round (Max_Sav * 1.1)
            do Sav_tbl [i] := 0;
        Max_Sav := round(Max_Sav * 1.1);
    except
        Error (13)
    end;
end;

procedure Extend_Sto_Tbl;
var i : integer;
begin
    try
        SetLength(Sto_Tbl, round (Max_Sto * 1.1) + 1);
        for i := Max_Sto to round (Max_Sto * 1.1)
            do Sto_tbl [i] := NIL;
        Max_Sto := round(Max_Sto * 1.1);
    except
        Error (10)
    end;
end;

procedure Extend_Fac_Tbl;
var i : integer;
begin
    try
        SetLength(Fac_Tbl, round (Max_Fac * 1.1) + 1);
        for i := Max_Fac to round (Max_Fac * 1.1)
            do Fac_tbl [i] := NIL;
        Max_Fac := round(Max_Fac * 1.1);
    except
        Error (30)
    end;
end;

procedure Extend_Blk_Tbl;
var i : integer;
begin
    try
        SetLength(Blk_Tbl, round (Max_Blk * 1.1) + 1);
        for i := Max_Blk to round (Max_Blk * 1.1)
            do Blk_tbl [i] := NIL;
        Max_Blk := round(Max_Blk * 1.1);
    except
        Error (2)
    end;
end;

procedure Extend_Cha_Tbl;
var i : integer;
begin
    try
        SetLength(Cha_Tbl, round (Max_Cha * 1.1) + 1);
        for i := Max_Cha to round (Max_Cha * 1.1)
            do Cha_tbl [i] := NIL;
        Max_Cha := round(Max_Cha * 1.1);
    except
        Error (29)
    end;
end;

procedure Extend_Fun_Tbl;
var i : integer;
begin
    try
        SetLength(Fun_Tbl, round (Max_Fun * 1.1) + 1);
        for i := Max_Fun to round (Max_Fun * 1.1)
            do Fun_tbl [i] := NIL;
        Max_Fun := round(Max_Fun * 1.1);
    except
        Error (9)
    end;
end;

procedure Extend_Rnd_Tbl;
var i : integer;
begin
    try
        SetLength(Rnd_Tbl, round (Max_Rnd * 1.1) + 1);
        for i := Max_Rnd to round (Max_Rnd * 1.1)
            do Rnd_tbl [i].Mult := 16807;
        Max_Rnd := round(Max_Rnd * 1.1);
    except
        Error (17)
    end;
end;

// Ova funkcija se nigde ne koristi
procedure Extend_IntRnd_Tbl;
var i : integer;
begin
    try
        SetLength(IntRnd_Tbl, round (Max_Int_Rnd * 1.1) + 1);
        for i := Max_Int_Rnd to round (Max_Int_Rnd * 1.1)
            do IntRnd_tbl [i].Mult := 16807;
        Max_Int_Rnd := round(Max_Int_Rnd * 1.1);
    except
        Error (17) // Koja greska ovde ide?, BJ 6/7/2003
    end;
end;

procedure Extend_Tab_Tbl;
var i : integer;
begin
    try
        SetLength(Tab_Tbl, round (Max_Tab * 1.1) + 1);
        for i := Max_Tab to round (Max_Tab * 1.1)
            do Tab_tbl [i] := NIL;
        Max_Tab := round(Max_Tab * 1.1);
    except
        Error (11)
    end;
end;

procedure Extend_Que_Tbl;
var i : integer;
begin
    try
        SetLength(Que_Tbl, round (Max_Que * 1.1) + 1);
        for i := Max_Que to round (Max_Que * 1.1)
            do Que_tbl [i] := NIL;
        Max_Que := round(Max_Que * 1.1);
    except
        Error (31)
    end;
end;

procedure Extend_Var_Tbl;
var i : integer;
begin
    try
        SetLength(Var_Tbl, round (Max_Var * 1.1) + 1);
        for i := Max_Var to round (Max_Var * 1.1)
            do Var_tbl [i] := NIL;
        Max_Var := round(Max_Var * 1.1);
    except
        Error (7)
    end;
end;
{}
{I RANDU.TXT}
PROCEDURE Randu (VAR Rnd : Rand);
{Generator slucajnih brojeva sa uniformnom raspodelom 0-999}
BEGIN {Randu}
  Rnd.Seed := Rnd.seed * Rnd.Mult;
  IF Rnd.Seed < 0 THEN Rnd.Seed := Rnd.Seed + MaxLongInt + 1;
  Rnd.val := Rnd.Seed / (MaxLongInt DIV 1000);
END; {Randu}

{I OPVAL.TXT}
PROCEDURE Op_val (Op : Operand;VAR Val : REAL);
{}
VAR i : LONGINT;
{}
PROCEDURE Adr_val (A : Ent_adr; VAR Val : LONGINT);
{}
BEGIN {Adr_val}
 CASE A.Typ OF
   Direct : Val := A.Adrs;
   Indirect : IF Trans ^.P <> NIL THEN
              IF A.Adrs > Max_par THEN Error (16) ELSE
               Val := Trans^.P^[A.Adrs] ELSE Val := 0;
   ELSE BEGIN END;
 END {Case}
END; {Adr_val}
{}
PROCEDURE Sna_val (VAR S : Sna);
{}
VAR Adres : LONGINT;
{}
{I FUNVAL.TXT}
PROCEDURE Fun_val (F : Fun_ptr);
{}
VAR i : LONGINT;
BEGIN
 Sna_val (F^.A);{Odredjivanje vrednosti argumenta}
                {25 - Generator slucajnih brojeva - arg.}
 IF F^.A.S_cod = 25 THEN F^.A.Values := F^.A.Values / 1000;
{}
 IF F^.A.Values <= F^.P [1].X THEN F^.V := F^.P[1].Y ELSE
 IF F^.A.Values > F^.P [F^.N].X THEN F^.V := F^.P[F^.N].Y ELSE
{}
  FOR i := 1 TO F^.N - 1 DO {Odredjivanje intervala za argument}
   IF (F^.A.Values > F^.P[i].X) AND (F^.A.Values <= F^.P[i+1].X) THEN
      CASE F^.Typ OF

         Con : F^.V := F^.P [i].Y + (F^.P[i+1].Y - F^.P[i].Y) *
                       (F^.A.Values - F^.P[i].X) / (F^.P[i+1].X - F^.P[i].X);
         Dis : F^.V := F^.P[i+1].Y;

        ELSE BEGIN END;
      END;
END; {Fun_val}
{}
{I VARVAL.TXT}
PROCEDURE Var_val (V : Var_ptr);
{}
VAR i,S : LONGINT;
    Red : ARRAY [1..Max_vop] OF REAL; {Aritmeticki stak}

Function Pow(x:real;n:real):real;
 begin
  IF x=0 THEN pow:=0 ELSE
    IF x > 0 THEN pow:=exp(n*ln(x)) ELSE error(42);
 end;

BEGIN {Var_val}

  S := 0; {Pokazivac vrha staka za aritmetiku}
  FOR i := 1 TO V^.N DO
    CASE V^.P_N [i].Typ OF
      Voperand : BEGIN {Ubacivanje na aritmeticki stak}
                   S := S + 1;
                   Op_val (V^.P_N [i].Vop,Red [s])
                 END;
     Voperator : BEGIN {Operacija sa elementima na vrhu staka}
                   CASE V^.P_N [i].Vopr OF
                     '+' : Red [S-1] := Red[S-1] + Red [S];
                     '-' : Red [S-1] := Red[S-1] - Red [S];
                     '*' : Red [S-1] := Red[S-1] * Red [S];
                     '/' : Red [S-1] := Red[S-1] / Red [S];
                     '^' : Red [S-1] :=Pow( Red[S-1], Red [S]);
                     '@' : Red [S-1] :=ROUND(Red[S-1])
                                        MOD ROUND(Red [S]);
                      ELSE BEGIN END;
                   END; {CASE}
                   S := S - 1
                 END
      ELSE BEGIN END;
    END;{CASE}
IF S <> 1 THEN Error (18) ELSE V^.V := Red [1]
END;{Var_val}
{}
BEGIN {Sna_val}
  WITH S DO
   BEGIN
    Adr_val (S_adr,Adres);
    IF S_cod = 1 THEN  Values := C1 ELSE
    IF S_cod IN [12,13,15,16] THEN IF Trans = NIL THEN Error (32)
     ELSE   {Atributi transakcije}
     CASE S_cod OF
      12 : Values := Ca - Trans ^.MP; {Rezid.time M1}
      13 : Values := Trans ^.MP;      {Mark time}
      15 : IF Adres > Max_par THEN Error (16)
           ELSE if Trans^.P<> nil then
             Values := Trans^.P^[Adres] {desava se da je p = nil?!?}
           else {Showmessage('Null pointer !?!?'); prim. M.D.}
             Values := 0; // dodao M.D. - po primerima bi se reklo
                          // da i ovako radi ispravno
      16 : Values := Trans ^.PR;
      ELSE BEGIN END;
     END {CASE}  ELSE

{ U buducoj verziji GPSS-a sa pascalom sa LONGINT*4
 treba sve REALNE  SNA pomnoziti sa 1000 radi kompatibilnosti}

    IF S_cod IN [2..6] THEN begin
                IF Adres > round(Max_cha * 0.9) THEN Extend_Cha_Tbl;
    IF Cha_tbl [Adres] = NIL THEN
    Values := 0 ELSE
     CASE S_cod OF
       2 : Values := Cha_tbl [Adres]^.Ca;
       3 : Values := Cha_tbl [Adres]^.Cc;
       4 : Values := Cha_tbl [Adres]^.Ch;
       5 : Values := Cha_tbl [Adres]^.Cm;
       6 : Values := Cha_tbl [Adres]^.Ct;
       ELSE;
     END {CASE}end  ELSE
{}
    IF S_cod IN [7,8,10,11]
        THEN begin
            IF Adres > round (Max_fac * 0.9)
                THEN Extend_Fac_Tbl;
                //ELSE
            IF Fac_tbl [Adres] = NIL THEN
     Values := 0 ELSE
      CASE S_cod OF
         7 : Values := Fac_tbl [Adres]^.F;
         8 : Values := Fac_tbl [Adres]^.Fc;
        10 : Values := Fac_tbl [Adres]^.Fr;
        11 : Values := Fac_tbl [Adres]^.Ft;
        ELSE;
      END {CASE} end ELSE
{}
    IF S_cod IN [14,36] THEN  begin
                            IF Adres > round(Max_blk * 0.9)
                                THEN Extend_Blk_Tbl;

     IF Blk_tbl [Adres] = NIL THEN
     Values := 0 ELSE
      CASE S_cod OF
        14 : Values := Blk_tbl [Adres]^.N;
        36 : Values := Blk_tbl [Adres]^.W;
        ELSE;
      END {CASE}end ELSE
    IF S_cod IN [17..23] THEN
    begin
    IF Adres > round(Max_que * 0.9) THEN Extend_Que_Tbl;
    IF Que_tbl [Adres] = NIL THEN
     Values := 0 ELSE
      CASE S_cod OF
        17 : Values := Que_tbl [Adres]^.Q;
        18 : Values := Que_tbl [Adres]^.Qa;
        19 : Values := Que_tbl [Adres]^.Qc;
        20 : Values := Que_tbl [Adres]^.Qm;
        21 : Values := Que_tbl [Adres]^.Qt;
        22 : Values := Que_tbl [Adres]^.Qx;
        23 : Values := Que_tbl [Adres]^.Qz;
        ELSE;
      END {CASE} end ELSE
{}
    IF S_cod = 25 THEN begin
        IF Adres > round(Max_rnd * 0.9) THEN Extend_Rnd_Tbl;

       Randu (Rnd_tbl [Adres]);
       Values := Rnd_tbl [Adres].Val
      end ELSE
{}
    IF S_cod IN [24,26..31]
        THEN
            begin
            IF Adres > round (Max_sto * 0.9)
                THEN Extend_Sto_Tbl;

                    IF Sto_tbl [Adres] = NIL
                        THEN Values := 0
                        ELSE
      CASE S_cod OF
        24 : Values := Sto_tbl [Adres]^.R;
        26 : Values := Sto_tbl [Adres]^.S;
        27 : Values := Sto_tbl [Adres]^.Sa;
        28 : Values := Sto_tbl [Adres]^.Sc;
        29 : Values := Sto_tbl [Adres]^.Sm;
        30 : Values := Sto_tbl [Adres]^.Sr;
        31 : Values := Sto_tbl [Adres]^.St
        ELSE;
      END {CASE}
        end
      ELSE
{}
    IF S_cod IN [32..34] THEN
    begin
    IF Adres > round(Max_tab * 0.9) THEN Extend_Tab_Tbl;
    IF Tab_tbl [Adres] = NIL THEN
     Values := 0 ELSE
      CASE S_cod OF
        32 : Values := Tab_tbl [Adres]^.Tb;
        33 : Values := Tab_tbl [Adres]^.Tc;
        34 : Values := Tab_tbl [Adres]^.Td
        ELSE;
      END {CASE}end ELSE
{}
    IF S_cod = 37
        THEN
            begin
            IF Adres > round (Max_sav * 0.9)
                THEN    Extend_Sav_Tbl;
                // ELSE
                Values := Sav_tbl [Adres]
            end
        ELSE
{}
    IF S_cod = 9 THEN begin

    IF Adres > round(Max_fun * 0.9) THEN Extend_Fun_Tbl;
    IF Fun_tbl [Adres] = NIL THEN
     Values := 0  ELSE
      BEGIN
         Fun_val (Fun_tbl[Adres]);
         Values := Fun_tbl [Adres]^.V
      END end ELSE
{}
    IF S_cod = 35 THEN begin
    IF Adres > round(Max_var * 0.9) THEN Extend_Var_Tbl;
    IF Var_tbl [Adres] = NIL THEN Values := 0 ELSE
      BEGIN
        Var_val (Var_tbl[Adres]);
        Values := Var_tbl [Adres]^.V
      END  end
 END
END; {Sna_val}
{
}
BEGIN {Op_val}
 CASE Op.typ OF
  Num_op : Val := Op.N_op;
  Adr_op : BEGIN
             Adr_val (Op.A_op,i);
             Val := i
           END;
  Sna_op : BEGIN
             Sna_val (Op.S_op);
             Val := Op.S_op.Values
           END
  ELSE BEGIN END;
  END {CASE}
END; {Op_val}

{I INPH.TXT}
{PROCEDURE Inp_ph;
                //*BojanNenadJovicic jul, avgust 2003.*
                //procedura koja je izbacena jer sluzi za ucitavanje obj
                //programa u memoriju da bi se izvrsila simulacija
                // sada njen posao obavlja procedura prepareSimulation

PROCEDURE Read_op (VAR Op : Operand);
BEGIN //Read_op
 REPEAT
 mObj.READ (Ch)
 // Read(Ob_f, Ch);
 UNTIL Ch <>' ';
 CASE Ch OF
   '%' : BEGIN
           Op.Typ := Num_op;
           mObj.READ (Op.N_op);
           // Read(Ob_f, Op.N_op);
         END; //%
   '&' : BEGIN
           Op.Typ := Sna_op;
           mObj.READ (Op.S_op.S_cod);
           // Read(Ob_F, Op.S_op.S_cod);
           REPEAT
           mObj.READ (ch)
           // Read(Ob_F, Ch);
           UNTIL Ch <> ' ';
           CASE ch OF
             'D' : Op.S_op.S_adr.Typ := Direct;
             '*' : Op.S_op.S_adr.Typ := Indirect;
              ELSE  Error (6)
           END;//CASE
           mObj.READ (Op.S_op.S_adr.Adrs)
           // Read(Ob_f, Op.S_op.S_adr.Adrs);
         END; //&
   'D','*' : BEGIN
               Op.Typ := Adr_op;
               IF ch = 'D' THEN Op.A_op.Typ := Direct
                 ELSE   Op.A_op.Typ := Indirect;
               mObj.READ (Op.A_op.Adrs)
               // Read(Ob_F, Op.A_op.Adrs);
             END; //D,*
   '!' : BEGIN END;
   ELSE  Error (6)
 END //CASE
END; //Read_op

PROCEDURE Read_blk;
BEGIN Read_blk//
 IF Lab > round(Max_blk * 0.9) THEN Extend_Blk_Tbl;
 IF Lab < 1 THEN Error (3) ELSE
  BEGIN
    IF Blk_tbl [Lab] = NIL THEN NEW (Blk_tbl[Lab]);
    initBlkPtr(Blk_Tbl[Lab]);
    Blk_tbl [lab]^.Op_cod := Opc;
    // Dodato ord oko Opc zbog izmene, BJ 6/7/2003
    FOR i := 1 TO Nr_OP[ord(Opc)] DO Read_op (Blk_tbl[lab]^.Op[i]);
    Blk_tbl [Lab]^.N := 0;
    Blk_tbl [Lab]^.W := 0
  END
END; //Read_blk

//I READDCL.TXT
PROCEDURE Read_dcl;
//I READFUN.TXT
PROCEDURE Read_fun;
VAR  i : LONGINT;BEGIN  //Read_fun
  IF Lab > round(Max_fun * 0.9) THEN Extend_Fun_Tbl;

     IF Fun_tbl [Lab] = NIL THEN NEW (Fun_tbl [Lab]);
     initFunPtr(Fun_Tbl [Lab]);
     Read_op (Op);
     Fun_tbl [lab]^.A := Op.S_op;
     Read_op (Op);
     j := Op.N_op;
     IF ABS (j) > Max_point THEN Error (15) ELSE
           BEGIN
             IF j > 0 THEN Fun_tbl [Lab]^.Typ := Con ELSE
                              Fun_tbl [Lab]^.Typ := Dis;
             Fun_tbl [Lab]^.N := ABS (j);
             FOR i := 1 TO ABS (j) DO
              BEGIN
                mObj.READ (Fun_tbl [lab]^.P[i].X);
                // Read(Ob_f, Fun_tbl [lab]^.P[i].X);
                mObj.READ (Fun_tbl [lab]^.P[i].Y);
                // Read(Ob_f, Fun_tbl [lab]^.P[i].Y);
              END
           END

END; //Read_fun

//I READINI.TXT
PROCEDURE Read_ini;
VAR R : REAL; i : LONGINT;BEGIN //Read_ini Read_op (Op);
 Op_val (Op,R);
 IF R > round (Max_sav * 0.9)
 THEN Extend_Sav_Tbl;
     i := TRUNC(R);
     Read_op (Op);
     Op_val (Op,R);
     Sav_tbl [i] := TRUNC(R)
END; //Read_ini

//I READTAB.TXT
PROCEDURE Read_tab;
VAR i: LONGINT;BEGIN //Read_tab
  IF Lab > round(Max_tab * 0.9) THEN Extend_Tab_Tbl;
    IF Tab_tbl [lab] = NIL THEN NEW (Tab_tbl [lab]);
    initTabPtr(Tab_Tbl [lab]);
    Tab_tbl [Lab]^.Tb := 0;
    Tab_tbl [Lab]^.Tc := 0;
    Tab_tbl [Lab]^.Td := 0;
    Tab_tbl [Lab]^.Sa := 0;
    Tab_tbl [Lab]^.Avo := 0;
    Read_op (Op);
    Tab_tbl [Lab]^.Arg := OP.S_op;
    Read_op (Op);
    Tab_tbl [Lab]^.I [1].UL := OP.N_op;
    Read_op (Op);
    Tab_tbl [Lab]^.Si := Op.N_op;
    Read_op (Op);
    Tab_tbl [Lab]^.Ni := Op.N_op;
    j := Op.N_op + 1;
    IF Op.N_op > Max_intr - 1 THEN Error (12) ELSE
          BEGIN
            FOR i:= 1 TO Tab_tbl [Lab]^.Ni + 1 DO
             BEGIN
              IF i > 1 THEN
               Tab_tbl [Lab]^.I [i].UL := Tab_tbl [Lab]^.I[1].Ul
                           + Tab_tbl [Lab]^.Si * (i-1);
              Tab_tbl [Lab]^.I[i].Off := 0
             END
          END
END; //Read_tab

//I READVAR.TXT
PROCEDURE Read_var;
VAR i, j : LONGINT ;BEGIN //Read_var
 IF Lab > round(Max_var  * 0.9) THEN Extend_Var_Tbl;
   IF Var_tbl [Lab] = NIL THEN NEW (Var_tbl [Lab]);
   initVarPtr(Var_Tbl [Lab]);
   Read_op (Op);
   Var_tbl [Lab]^.N := Op.N_op;
   IF Op.N_op > Max_vop THEN Error (8) ELSE
     FOR i := 1 TO Op.N_op DO
       BEGIN
         Read_op (Op);
         IF Ch = '!' THEN
           BEGIN
            REPEAT
                mObj.READ (Ch)
                // Read(OB_F, CH);
            UNTIL Ch <> ' ';
            Var_tbl [Lab]^.P_N [i].Typ := Voperator;
            Var_tbl [Lab]^.P_N [i].Vopr := Ch
           END  ELSE
           BEGIN
            Var_tbl [Lab]^.P_N [i].Typ := Voperand;
            Var_tbl [Lab]^.P_N [i].Vop := Op
           END
       END
 END; //Read_var

//I READSTO.TXT
PROCEDURE Read_sto;
VAR R : REAL;
BEGIN //Read_sto
  IF Lab > round (Max_sto * 0.9)
        THEN Extend_Sto_Tbl;
   //BEGIN
     IF Sto_tbl [lab] = NIL THEN NEW (Sto_tbl [Lab]);
     Read_op (Op);
     Op_val (Op,R);
     Sto_tbl [Lab]^.R := TRUNC(R);
     Sto_tbl [Lab]^.S := 0;
     Sto_tbl [Lab]^.Sa := 0;
     Sto_tbl [Lab]^.Sc := 0;
     Sto_tbl [Lab]^.Sr := 0;
     Sto_tbl [Lab]^.Sm := 0;
     Sto_tbl [Lab]^.St := 0;
     Sto_tbl [Lab]^.Tht := 0;
     Sto_tbl [Lab]^.C0 := 0
   //END
END; //Read_sto

BEGIN //Read_dcl
 CASE Opc OF         // Promenjeno iz brojeva u odgovarajuce enumerativne, BJ 6/7/2003
    ocRead_Fun : Read_fun;
    ocRead_ini : Read_ini;
    ocRead_sto : Read_sto;
    ocRead_tab : Read_tab;
    ocRead_var : Read_var;
   ELSE BEGIN END;
 END //CASE
END; Read_dcl

//I CTRL.TXT
PROCEDURE Read_ctr;

//I RESET.TXT
PROCEDURE Reset;

//I CECREM.TXT
PROCEDURE Cec_rem (VAR Tr:Tra_ptr);

BEGIN  //Cec_rem
  IF Tr^.Blink = NIL THEN
   BEGIN //Azurira se Head CEC
    Cec.Head := Tr^.Flink;
    IF Cec.Head <> NIL THEN Cec.Head^.Blink  := NIL ELSE Cec.Tail := NIL;
   END
    ELSE IF Tr^.Flink = NIL THEN
      BEGIN Azurira se tail CEC//
        Cec.Tail := Tr^.Blink;
        IF Cec.Tail <> NIL THEN Cec.Tail^.Flink := NIL ELSE Cec.Head := NIL;
      END
       ELSE
        BEGIN Azuriraju se pokazivaci prethodne i sledece trans
          TR^.Flink^.Blink := Tr^.Blink;
          TR^.Blink^.Flink := Tr^.Flink
        END
END; //Cec_rem

//I FECINS.TXT
PROCEDURE Fec_ins ( VAR  Tr : Tra_ptr );
//Ubacuje transakciju na koju pokazuje pokazivac Tr
// u FEC uredjenu po Bdt,Pr
VAR Next,Prev : Tra_ptr; //Pointeri na predhodnu i sledecu transakciju u FEC
    Flg : BOOLEAN;

BEGIN //Fec_ins
  IF Fec.Head = NIL THEN
   BEGIN //Ubacivanje transakcije u praznu listu
     Fec.Head := Tr;
     Fec.Tail := Tr;
     Tr^.Flink := NIL;
     Tr^.Blink := NIL
   END  ELSE
   BEGIN
     Prev := Fec.Tail;
     Next := NIL;
    //Odredjivanje mesta za Tr u FEC
     Flg := TRUE;
     WHILE  (Prev <> NIL) AND Flg DO
      BEGIN
       Flg := ((Prev^.Bdt > Tr^.Bdt) OR
            ((Prev^.Bdt = Tr^.Bdt) AND (Prev^.Pr < Tr^.Pr)));
       IF Flg THEN
        BEGIN
          Next := Prev;  //Prelazak na sledecu transakciju
          Prev := Prev^.Blink
        END;
      END;
     IF Prev = NIL THEN
      BEGIN  //Ubacivanje na pocetak FEC
        Fec.Head := Tr;
        Next^.Blink := Tr;
        Tr^.Flink := Next;
        Tr^.Blink := NIL;
      END ELSE
      IF Next = NIL THEN
       BEGIN //Ubacivanje na kraj FEC
         Fec.Tail := Tr;
         Prev^.Flink := Tr;
         Tr^.Flink := NIL;
         Tr^.Blink := Prev
       END
       ELSE
        BEGIN
          Tr^.Flink := Next;
          Tr^.Blink := Prev;
          Prev^.Flink := Tr;
          Next^.Blink := Tr
        END
   END;
   Tr^.Ch := FEC_ch
END; //Fec_ins

//I FECREM.TXT
PROCEDURE Fec_rem (VAR Tr:Tra_ptr);
BEGIN  //Fec_rem
  IF Tr^.Blink = NIL THEN
   BEGIN //Azurira se Head FEC
    Fec.Head := Tr^.Flink;
    IF Fec.Head <> NIL THEN  Fec.Head^.Blink  := NIL ELSE Fec.Tail := NIL;
   END
   ELSE IF Tr^.Flink = NIL THEN
    BEGIN //Azurira se tail FEC
      Fec.Tail := Tr^.Blink;
      IF Fec.Tail <> NIL THEN Fec.Tail^.Flink := NIL ELSE Fec.Head := NIL;
    END ELSE
     BEGIN //Azuriraju se pokazivaci prethodne i sledece trans
      Tr^.Flink^.Blink := Tr^.Blink;
      Tr^.Blink^.Flink := Tr^.Flink
     END
END; //Fec_rem

VAR i,j : LONGINT;
    Trans_tmp : Tra_ptr;

BEGIN //Reset

 Trans_tmp := FEC.Head;
 WHILE Trans_tmp <> NIL DO  //Podesavanje vremena u FEC
  BEGIN
    Trans_tmp^.Bdt := Trans_tmp^.Bdt - C1;
    Trans_tmp := Trans_tmp^.Flink
  END;

 WHILE Cec.head <> NIL DO //Anuliranje vremena u CEC i prebacivanje u FEC
  BEGIN
    Trans_tmp := CEC.Head;
    Trans_tmp^.Bdt := 1;
    Cec_rem(Trans_tmp);
    Fec_ins(Trans_tmp)
  END;

 Trans_tmp := FEC.Head;
 WHILE Trans_tmp <> NIL DO
  BEGIN
    Trans := Trans_tmp;
    Trans_tmp := Trans_tmp^.Flink;
    // Enumerativni umesto 9, BJ 6/7/2003
    IF Blk_tbl[Trans^.Cb]^.Op_cod = ocError21 THEN
      BEGIN
       Fec_rem(Trans); //Generate
       Dispose(Trans);
       Trans := NIL
      END;
  END;

 Trans := NIL;
 C1 := 0; //Anuliranje sata
 //Brisanje statistike
 FOR i:=1 TO Max_blk DO IF Blk_tbl[i] <> NIL THEN Blk_tbl[i]^.N:=Blk_tbl[i]^.W;
 FOR i := 1 TO Max_cha DO
  IF Cha_tbl [i] <> NIL THEN
   BEGIN
     Cha_tbl [i]^.Ca := 0;
     Cha_tbl [i]^.Cc := Cha_tbl [i]^.Ch;
     Cha_tbl [i]^.Cm := 0;
     Cha_tbl [i]^.Ct := 0;
     Cha_tbl [i]^.Tht := 0;
     Cha_tbl [i]^.C0 := 0;
   END;

 FOR i := 1 TO Max_fac DO
  IF Fac_tbl [i] <> NIL THEN
   BEGIN
     Fac_tbl [i]^.Fc := Fac_tbl [i]^.F;
     Fac_tbl [i]^.Fr := 0;
     Fac_tbl [i]^.Ft := 0;
     Fac_tbl [i]^.C0 := 0;
     Fac_tbl [i]^.Tht := 0;
   END;

 FOR i := 1 TO Max_sto DO
  IF Sto_tbl [i] <> NIL THEN
   BEGIN
     Sto_tbl [i]^.Sa := 0;
     Sto_tbl [i]^.Sc :=  Sto_tbl [i]^.S;
     Sto_tbl [i]^.Sr := 0;
     Sto_tbl [i]^.Sm := 0;
     Sto_tbl [i]^.St := 0;
     Sto_tbl [i]^.Tht := 0;
     Sto_tbl [i]^.C0 := 0
   END;

 FOR i := 1 TO Max_tab DO
  IF Tab_tbl [i] <> NIL THEN
   BEGIN
     Tab_tbl [i]^.Tb := 0;
     Tab_tbl [i]^.Tc := 0;
     Tab_tbl [i]^.Td := 0;
     Tab_tbl [i]^.Sa := 0;
     Tab_tbl [i]^.Avo:= 0;
     FOR j := 1 TO Max_intr DO Tab_tbl [i]^.I [j].Off := 0;
  END;

 FOR i := 1 TO Max_que DO
  IF Que_tbl [i] <> NIL THEN
   BEGIN
     Que_tbl [i]^.Qa := 0;
     Que_tbl [i]^.Qc := Que_tbl [i]^.Q;
     Que_tbl [i]^.Qm := 0;
     Que_tbl [i]^.Qt := 0;
     Que_tbl [i]^.Qx := 0;
     Que_tbl [i]^.Qz := 0;
     Que_tbl [i]^.Tht := 0;
     Que_tbl [i]^.Thz := 0;
     Que_tbl [i]^.C0 := 0;
     Que_tbl [i]^.Cz := 0
   END;
END; //Reset

//I CLEAR.TXT
PROCEDURE Clear;
VAR S : ARRAY [1..5] OF word;
    i : LONGINT;

PROCEDURE Free (VAR Head : Tra_ptr); //Oslobadja transakcije
VAR Temp : Tra_ptr;
BEGIN //Free
   WHILE Head <> NIL DO
     BEGIN
       Temp := Head^.Flink;
       IF Head^.P <> NIL THEN DISPOSE (Head^.P);
       DISPOSE (Head);
       Head := Temp
     END
END; //Free

BEGIN //Clear
  FOR i:= 1 TO 5 DO  //Ulaz opreanada za CLEAR
   BEGIN
     Read_op (Op);
     S [i] := Op.A_op.Adrs
   END;

  Reset; //Anuliranje sata i ciscenje statistike

  //Anuliranje SNA koje RESET ne anulira

  Free (CEC.Head); CEC.Tail := NIL; //Oslobadjanje CEC
  Free (FEC.Head); FEC.Tail := NIL; //Oslobadjanje FEC

  FOR i := 1 TO Max_cha DO          //Oslobadjanje UC
   IF Cha_tbl[i] <> NIL THEN
    BEGIN
     Free (Cha_tbl[i]^.Head);
     Cha_tbl [i]^.Tail := NIL
    END;

 //Oslobadjanje prijemptovanih transakcija

  FOR i := 1 TO Max_fac DO IF Fac_tbl[i] <> NIL THEN Free (Fac_tbl[i]^.Head);


  FOR i := 1 TO Max_blk DO IF Blk_tbl [i] <> NIL
     THEN
       BEGIN
         Blk_tbl [i]^.W := 0;
         Blk_tbl [i]^.N := 0
       END;

  FOR i := 1 TO Max_cha DO IF Cha_tbl [i] <> NIL THEN Cha_tbl [i]^.Ch := 0;

  FOR i := 1 TO Max_fac DO IF Fac_tbl [i] <> NIL
     THEN BEGIN
            Fac_tbl [i]^.Sc := 0;
            Fac_tbl [i]^.F := 0;
            Fac_tbl [i]^.Pc := 0
          END;

  FOR i := 1 TO Max_sto DO IF Sto_tbl [i] <> NIL
     THEN Sto_tbl [i]^.S := 0;

  FOR i := 1 TO Max_que DO IF Que_tbl [i] <> NIL
     THEN Que_tbl [i]^.Q := 0;

  FOR i := 1 TO Max_sav DO //Anuliranje Savevalue
   IF NOT (i IN [ S[1],S[2],S[3],S[4],S[5] ]) THEN Sav_tbl [i] := 0;
END; //Clear

PROCEDURE Start;
BEGIN //Start
    Read_op (Op);
    Tc := Op.N_op;
    Read_op (Op);
    IF Op.N_op = 0 THEN P_flg := FALSE ELSE P_flg := TRUE
END; //Start

BEGIN //Read_ctr
  CASE opc OF // Promenjeno u odgovarajuce enumarativne, BJ 6/7/2003
   ocClear : Clear;
   ocStop : Stop := TRUE; //END Naredba//
   ocReset : Reset;
   ocSim_flg : Sim_flg := TRUE; //Simulate1
   ocStart : Start;
  ELSE BEGIN END;
  END //CASE
END; //Read_ctr

VAR read_int : integer; // Znak za pomoc pri konverziji pri ucitavanju, BJ 6/7/2003
    i : integer;
BEGIN //Inp_ph
  REPEAT    // iz 0 u ocNull, BJ 6/7/2003
    Lab := 0; Opc := ocNull;
    mObj.READ (Lab);
    // Read(Ob_f, Lab);
    mObj.READ (read_int);
    // Read(Ob_F, read_int);
    Opc := Int2OC(read_int);
    IF Opc IN [ ocAdvance,
                ocAssign,
                ocDepart,
                ocEnter,
                ocGate,
                ocError21,
                ocLeave..ocRelease,
                ocSavevalue,
                ocSeize,
                ocTabulate..ocUnlink] THEN Read_blk
    ELSE
    IF Opc IN [ ocRead_fun,
                ocRead_ini,
                ocRead_sto,
                ocRead_tab,
                ocRead_var] THEN Read_dcl ELSE
    IF Opc IN [ ocClear,
                ocStop,
                ocReset,
                ocSim_flg,
                ocStart] THEN Read_ctr ELSE Error (1);

    mObj.READLN;
    inc(krug);
    // snimi_blokove(inttostr(krug));
    // Readln(Ob_f);
  UNTIL STOP OR (NOT Stop AND (Tc > 0))  OR mObj.EOF; // EOF(ob_f);
  IF   (mObj.EOF)
      //  EOF(ob_F)
        AND (Opc <> ocStop) THEN Error (14);
  // snimi_blokove(FloatToStr(Now));
END; //Inp_ph }//*BojanNenadJovicic jul, avgust 2003.*

{I FECINS.TXT}
PROCEDURE Fec_ins ( VAR  Tr : Tra_ptr );

{Ubacuje transakciju na koju pokazuje pokazivac Tr
 u FEC uredjenu po Bdt,Pr }

VAR Next,Prev : Tra_ptr; { Pointeri na predhodnu i sledecu
                           transakciju u FEC }
    Flg : BOOLEAN;

BEGIN {Fec_ins}
  IF Fec.Head = NIL THEN
   BEGIN {Ubacivanje transakcije u praznu listu}
     Fec.Head := Tr;
     Fec.Tail := Tr;
     Tr^.Flink := NIL;
     Tr^.Blink := NIL
   END  ELSE
   BEGIN
     Prev := Fec.Tail;
     Next := NIL;
    {Odredjivanje mesta za Tr u FEC}
     Flg := TRUE;
     WHILE  (Prev <> NIL) AND Flg DO
      BEGIN
       Flg := ((Prev^.Bdt > Tr^.Bdt) OR
            ((Prev^.Bdt = Tr^.Bdt) AND (Prev^.Pr < Tr^.Pr)));
       IF Flg THEN
        BEGIN
          Next := Prev;  {Prelazak na sledecu transakciju}
          Prev := Prev^.Blink
        END;
      END;
     IF Prev = NIL THEN
      BEGIN  {Ubacivanje na pocetak FEC}
        Fec.Head := Tr;
        Next^.Blink := Tr;
        Tr^.Flink := Next;
        Tr^.Blink := NIL;
      END ELSE
      IF Next = NIL THEN
       BEGIN {Ubacivanje na kraj FEC}
         Fec.Tail := Tr;
         Prev^.Flink := Tr;
         Tr^.Flink := NIL;
         Tr^.Blink := Prev
       END
       ELSE
        BEGIN
          Tr^.Flink := Next;
          Tr^.Blink := Prev;
          Prev^.Flink := Tr;
          Next^.Blink := Tr
        END
   END;
   Tr^.Ch := FEC_ch
END; {Fec_ins}



{I FECGET.TXT}
PROCEDURE Fec_get (VAR Tr : Tra_ptr);

VAR Temp : Tra_ptr;

BEGIN {Fec_get}
  IF Fec.Head = NIL THEN Tr := NIL ELSE
    BEGIN
      Tr := Fec.Head;
      Fec.Head := Tr^.Flink;
      IF Fec.Head = NIL THEN Fec.Tail := NIL ELSE Fec.Head^.Blink := NIL
    END
END; {Fec_get}

FUNCTION Fec_time : LONGINT;
BEGIN
  if Fec.Head = nil {desava se da je nil???? prim. M. D.}
     then result := 0
     else Fec_time := Fec.Head^.Bdt
END; {Fec_time}

FUNCTION Fec_empty : BOOLEAN;
BEGIN
  IF Fec.Head = NIL THEN Fec_empty := TRUE ELSE Fec_empty := FALSE
END; {Fec_empty}

{I CECINS.TXT}
PROCEDURE Cec_ins ( VAR Tr : Tra_ptr );

{Ubacuje transakciju na koju pokazuje pokazivac Tr
 u CEC uredjenu po PR,BDT }

VAR Next,Prev : Tra_ptr; { Pointeri na predhodnu i sledecu
                           transakciju u CEC }
    Flg : BOOLEAN;
BEGIN {Cec_ins}
  IF Cec.Head = NIL THEN
   BEGIN {Ubacivanje transakcije u praznu listu}
     Cec.Head := Tr;
     Cec.Tail := Tr;
     Tr^.Flink := NIL;
     Tr^.Blink := NIL
   END  ELSE
   BEGIN
     Prev := Cec.Tail;
     Next := NIL;

    {Odredjivanje mesta za Tr u CEC}

     Flg := true;
     WHILE (Prev <> NIL) AND Flg DO
     BEGIN
      Flg := ((Prev^.Pr < Tr^.Pr) OR
            ((Prev^.Pr = Tr^.Pr) AND (Prev^.Bdt > Tr^.Bdt)));
      IF Flg THEN
        BEGIN
          Next := Prev;  {Prelazak na sledecu transakciju}
          Prev := Prev^.Blink
        END;
     END;
     IF Prev = NIL THEN
      BEGIN  {Ubacivanje na pocetak CEC}
        Cec.Head^.Blink := Tr;
        Tr^.Flink := Cec.Head;
        Tr^.Blink := NIL;
        Cec.Head := Tr;
      END ELSE
      IF Next = NIL THEN
       BEGIN {Ubacivanje na kraj CEC}
         Cec.Tail^.Flink := Tr;
         Tr^.Flink := NIL;
         Tr^.Blink := Cec.Tail;
         Cec.Tail := Tr;
       END
       ELSE
        BEGIN
          Tr^.Flink := Next;
          Tr^.Blink := Prev;
          Prev^.Flink := Tr;
          Next^.Blink := Tr
        END
   END;
   Tr^.Ch := CEC_ch
END; {Cec_ins}

{I CECREM.TXT}
PROCEDURE Cec_rem (VAR Tr:Tra_ptr);

BEGIN  {Cec_rem}
  IF Tr^.Blink = NIL THEN
   BEGIN {Azurira se Head CEC}
    Cec.Head := Tr^.Flink;
    IF Cec.Head <> NIL THEN Cec.Head^.Blink  := NIL ELSE Cec.Tail := NIL;
   END
    ELSE IF Tr^.Flink = NIL THEN
      BEGIN {Azurira se tail CEC}
        Cec.Tail := Tr^.Blink;
        IF Cec.Tail <> NIL THEN Cec.Tail^.Flink := NIL ELSE Cec.Head := NIL;
      END
       ELSE
        BEGIN {Azuriraju se pokazivaci prethodne i sledece trans}
          TR^.Flink^.Blink := Tr^.Blink;
          TR^.Blink^.Flink := Tr^.Flink
        END
END; {Cec_rem}

{I CRETRANS.TXT}
PROCEDURE Cre_trans (k : LONGINT; St_time : LONGINT);

VAR Ov : ARRAY [1..Max_opr] OF REAL;
     j : LONGINT;

BEGIN {Cre_trans}
  FOR j := 1 TO 5 DO Op_val (Blk_tbl [k]^.Op [j],Ov [j]);

  IF (Blk_tbl [k]^.N < (Ov[4]-1)) OR ((Blk_tbl[k]^.N=0) AND (Ov[4]=1.0)) THEN
   IF {MemAvail > 2000} true THEN  {JA PROMENIO}
    BEGIN
     NEW (Trans);
     initTraPtr(Trans);
     Trans^.Cb := k;
     Trans^.Nba := k + 1;
     Trans^.Pr := TRUNC (Ov [5]);
     Trans^.Sf := TRUE;
     Trans^.Qin := 0;
     Trans^.P := NIL;
     IF (Ov [3] > 0) AND (St_time = 0)
      THEN Trans^.Bdt := TRUNC(Ov [3]) ELSE
           BEGIN
             WITH Blk_tbl [k]^ DO
               BEGIN
                 IF Op [2].Typ = Sna_op THEN
                  IF Op [2].S_op.S_cod = 9 THEN { 9 FUNCTION }
                     BEGIN  {Mnozenje vred. funkcije sa vr.odl.}
                       Ov [1] := Ov [1] * Ov [2];
                       Ov [2] := 0
                     END;
                END; {WITH}
             IF Ov [2] > 0 THEN Randu (IntRnd_tbl [1]);
             Trans^.Bdt := TRUNC(Ov[1]+(IntRnd_tbl[1].Val-500)*2.0E-3*Ov[2])
           END;

        IF Trans^.Bdt < 0 THEN Error (19) ELSE
           BEGIN
             Trans^.Bdt := St_time + Trans^.Bdt;
             IF Trans^.Bdt = 0 THEN Trans^.Bdt := 1;
             Trans^.M1 := 0;  {Rezident time}
             Trans^.Mp := Ca + Trans^.Bdt - C1; {Mark time}
             Fec_ins (Trans)
           END
    END ELSE Error(4)
END; {Cre_trans}

{I SFCUPF.TXT}
PROCEDURE CUP;

VAR Tr : Tra_ptr;

BEGIN
  IF NOT Stop THEN
   IF NOT Fec_empty THEN
     BEGIN
       Ca := Ca + Fec_time - C1;
       C1 := Fec_time;
       {GotoXY(xx,yy);
       HighVideo;     }{JA PROMENIO}
{       WRITE (C1:10);}
{       NormVideo;}  {JA PROMENIO}
       WHILE Fec_time = C1 DO
        BEGIN
          Fec_get (Tr);
          IF Blk_tbl [Tr^.Cb]^.Op_cod = ocError21 THEN  Cre_trans (Tr^.Cb,Tr^.Bdt);
          Cec_ins (Tr)
        END
     END ELSE Error (20);
END; {CUP}

PROCEDURE SP;

PROCEDURE Move_trans; {Pokrece transakciju Trans kroz model }

VAR Prev_blk,Curr_blk : LONGINT; {Predhodni i tekuci blok }
{I BLKENTRY.TXT}
 FUNCTION Blk_entry (Blk:LONGINT):BOOLEAN;

 VAR n : LONGINT;
     r : REAL;

  BEGIN {Blk_entry}
    IF Blk_tbl [Blk] = NIL THEN Error (3) ELSE
    BEGIN
      Op_val (Blk_tbl [Blk]^.Op [1],r);
      n := TRUNC(r);
      Blk_entry := FALSE;
      CASE Blk_tbl [Blk]^.Op_cod OF
          { ENTER }
        ocEnter : begin
                IF n > round (Max_sto * 0.9)
                    THEN Extend_Sto_Tbl;
              IF Sto_tbl [n] = NIL THEN Error (33)
               ELSE IF Sto_tbl [n]^.R > Sto_tbl [n]^.S THEN Blk_entry := TRUE;
          end;{ SEIZE }
       ocSeize : BEGIN
              IF n > Max_fac THEN Error (38) ELSE
               IF Fac_tbl [n] <> NIL THEN
                WITH Fac_tbl [n]^DO
                  BEGIN
                   IF F = 1 THEN
                     BEGIN
                       IF (Tr^.Pr < Trans^.Pr) AND (Tr^.Ch = FEC_ch)
                         THEN Blk_entry := TRUE;
                     END ELSE Blk_entry := TRUE
                  END ELSE Blk_entry := TRUE
            END;
       ELSE Blk_entry := TRUE
      END  {CASE}
    END
  END; {Blk_entry}
{}
{I ADVANCE.TXT}
PROCEDURE Advance;

VAR i,j : LONGINT;

 BEGIN {Advance}      {Odredjivanje vrednosti operanada}
   FOR i := 1 TO 2 DO Op_val (Blk_tbl[Trans^.Nba]^.Op[i],R[i]);

   WITH Blk_tbl [Trans^.Nba]^ DO
      BEGIN  {FUNCTION drugi operand}
        IF Op [2].Typ = Sna_op THEN
         IF Op [2].S_op.S_cod = 9 THEN  {FUNCTION - OP2}
            BEGIN
              R [1] := R [1] * R [2];
              R [2] := 0
            END;
      END; {WITH}
   IF R [2] > 0 THEN Randu (IntRnd_tbl[2]); {Odredjivanje slucajnog broja}

 { Odredjivanje vremena zakasnjenja transakcije }

   j := ROUND (R [1] + (IntRnd_tbl [2].Val - 500) * 2.0E-3 * R [2]);
   IF j < 0 THEN Error (23) ELSE
   BEGIN
    {Azuriranje brojaca prethodnog i narednog bloka}
    Blk_tbl [Trans^.Cb]^.W  := Blk_tbl [trans^.Cb]^.W - 1;
    Blk_tbl [Trans^.Nba]^.W := Blk_tbl [trans^.Nba]^.W + 1;
    Blk_tbl [Trans^.Nba]^.N := Blk_tbl [trans^.Nba]^.N + 1;

    Trans^.Sf  := FALSE;         {Postavljane idikatora za skaniranje}
    Trans^.Bdt := ROUND(C1) + j; {Vreme zadrzavanja transakcije u Bdt}
    Trans^.Cb  := Trans^.Nba;    {Azuriranje tek. i sled. bloka}
    Trans^.Nba := Trans^.Cb + 1;
    Cec_rem (Trans);             {Vadjenje iz CEC}
    Fec_ins (Trans);             {Ubacivanje u FEC}
    Scf := TRUE                  {Postavljanje flaga za ponovno skaniranje CEC}
   END
 END; {Advance}

PROCEDURE Assign;
VAR i : LONGINT;
 BEGIN  {Assign}
   FOR i := 1 TO 3 DO Op_val (Blk_tbl[Trans^.Nba]^.Op[i],R[i]);
   IF R [2] <> 0 THEN R [3] := R [2] * R [3];
   IF TRUNC (R[1]) > Max_par THEN Error (35) ELSE
   IF Trans^.P = NIL THEN
    BEGIN
      NEW (Trans^.P);  {Kreiranje pointera za parametre}
      initParPtr(Trans^.P);
      // FOR i := 1 TO Max_par DO Trans^.P^[i] := 0;
    END;
   Trans^.P^[TRUNC(R[1])] := Trans^.P^[TRUNC(R[1])]  * ABS (TRUNC(R[2])) + TRUNC(R [3]);
   Trans^.Cb := Trans^.Nba;
   Trans^.Nba := Trans^.Cb + 1
 END; {Assign}


PROCEDURE Savevalue;

VAR i,j : LONGINT;

 BEGIN {Savevalue}
   FOR i := 1 TO 3 DO Op_val (Blk_tbl[Trans^.Nba]^.Op[i],R[i]);
   IF R [2] <> 0 THEN R [3] := R [2] * R [3];
   IF  Blk_tbl [Trans^.Nba]^.Op[1].Typ = Sna_op
    THEN
     BEGIN
      j := Blk_tbl[Trans^.Nba]^.Op[1].S_op.S_adr.Adrs; {adresa savevalue}
      IF Blk_tbl[trans^.nba]^.Op[1].S_op.S_cod <> 37 THEN Error (5);
     END ELSE
      IF Blk_tbl[Trans^.Nba]^.Op[1].Typ = Adr_op
        THEN
         { j := Blk_tbl[Trans^.Nba]^.Op[1].A_op.Adrs ELSE Error (5)}
         j:= TRUNC(R[1]);
   IF (j > round (Max_sav * 0.9))
        THEN Extend_Sav_Tbl;
    IF Trans^.Cb <> Trans^.Nba
      THEN  Sav_tbl [j] := Sav_tbl[j] * ABS (TRUNC(R[2])) + TRUNC(R [3]);
   Trans^.Cb := Trans^.Nba;
   Trans^.Nba := Trans^.Cb + 1
 END; {Savevalue}
{}
{I ENTER.TXT}
PROCEDURE Enter;
VAR Adrs : LONGINT;
BEGIN {Enter}
  Op_val (Blk_tbl [Trans^.Nba]^.Op [1],R [1]);
  Adrs := TRUNC(R [1]);
  IF Adrs > round (Max_sto * 0.9)
        THEN Extend_Sto_Tbl;
        // ELSE
        IF Sto_tbl [Adrs] = NIL
   THEN Error (33) ELSE
    WITH Sto_tbl [Adrs]^ DO
     BEGIN
      IF S < R THEN
       BEGIN  {Racunanje statistike}
         Tht := Tht + (C1 - C0) * 1.0E0 * S;
         Sa := Tht / C1;
         Sr := Sa / R;
         IF Sc > 0 THEN St := Tht / Sc;
         Sc := Sc + 1; {Brojac ulaza u Storage}
         C0 := C1;{Azuriranje vremena zadnje promene kapaciteta}
         S := S + 1;  {Povecanje angazovanog kapaciteta}
         IF S > Sm THEN Sm := S; {Azuriranje max. sadrzaja}
         Trans^.Cb := Trans^.Nba;{Azuriranje pokazivaca blokova}
         Trans^.Nba := Trans^.Nba + 1
       END
         ELSE Trans^.Sf := FALSE {Skladiste puno}
     END {WITH}
END; {Enter}
{}
PROCEDURE Leave;
Var Adrs : LONGINT;
BEGIN {Leave}
  Op_val (Blk_tbl [Trans^.Nba]^.Op [1],R [1]);
  Adrs := TRUNC(R [1]);
  IF Adrs > round (Max_sto * 0.9)
        THEN Extend_Sto_Tbl;
        //ELSE
        IF Sto_tbl [Adrs] = NIL
   THEN Error (33) ELSE
    WITH Sto_tbl [Adrs]^ DO
     BEGIN  {Azuriranje statistike}
      Tht := Tht + (C1 - C0) * 1.0E0 * S;
      Sa := Tht / C1;
      Sr := Sa / R;
      St := Tht / Sc;
      C0 := C1;{Azuriranje vremena zadnje promene kapaciteta}
      S := S - 1; {Smanjivanje kapaciteta}
      Trans^.Cb := Trans^.Nba;{Azuriranje pokazivaca bloka}
      Trans^.Nba := Trans^.Nba + 1;
      Scf := TRUE {Postavljanje flaga za ponovno skaniranje}
     END
END; {Leave}
{}
{I LINK.TXT}
PROCEDURE Link;

VAR Adrs,Ind : LONGINT;
   Next,Prev: Tra_ptr; { Pointeri na predhodnu i sledecu
                           transakciju u korisnickom redu }
    Flg : BOOLEAN;
BEGIN {Link}
  FOR i := 1 TO 2 DO Op_val (Blk_tbl [Trans^.Nba]^.Op [i],R[i]);
  Adrs := TRUNC(R [1]);
  Ind := TRUNC(R [2]);
  IF Adrs > Max_cha THEN Error (40) ELSE
   BEGIN
     IF Cha_tbl [Adrs] = NIL THEN
        BEGIN
          NEW (Cha_tbl [Adrs]);
          initChaPtr(Cha_tbl [Adrs]);
          // WITH Cha_tbl [Adrs]^ DO
          //  BEGIN
          //   Head := NIL ; Tail := NIL;
          //    Ca := 0; Cc := 0; Ch := 0;
          //    Cm := 0; Ct := 0; C0 := 0; Tht := 0
          //  END {WITH}
        END;
     WITH Cha_tbl [Adrs]^ DO
        BEGIN
          Cec_rem (Trans);
          Trans^.Cb := Trans^.Nba;
          Scf := TRUE;
          CASE Ind OF
            1..Max_par:
                 BEGIN
                  {Ubacuje transakciju u korisnicki red
                   sortiran po parametru transakcije}
                  IF Head = NIL THEN
                     BEGIN {Ubacivanje transakcije u praznu listu}
                      Head := Trans;
                      Tail := Trans;
                      Trans^.Flink := NIL;
                      Trans^.Blink := NIL
                     END  ELSE
                      BEGIN
                       Prev := Tail;
                       Next := NIL;
                       {Odredjivanje mesta za Trans u korisnickom redu}
                       Flg := true;
                       WHILE (Prev <> NIL) AND Flg DO
                        BEGIN
                          Flg := (Prev^.P^[ind] >= Trans^.P^[ind]);
                          IF Flg THEN
                           BEGIN
                            Next := Prev;  {Prelazak na sledecu transakciju}
                            Prev := Prev^.Blink
                           END;
                        END;
                       IF Prev = NIL THEN
                        BEGIN  {Ubacivanje na pocetak}
                         Head^.Blink := Trans;
                         Trans^.Flink := Head;
                         Trans^.Blink := NIL;
                         Head := Trans;
                        END ELSE
                         IF Next = NIL THEN
                          BEGIN {Ubacivanje na kraj}
                           Tail^.Flink := Trans;
                           Trans^.Flink := NIL;
                           Trans^.Blink := Tail;
                           Tail := Trans;
                          END
                          ELSE
                           BEGIN
                            Trans^.Flink := Next;
                            Trans^.Blink := Prev;
                            Prev^.Flink := Trans;
                            Next^.Blink := Trans
                           END
                      END;
                 END;

            51 : BEGIN  {FIFO - Ubacivanje na kraj reda}
                  Trans^.Blink := Tail;
                  Trans^.Flink := NIL;
                  IF Tail = NIL THEN Head := Trans ELSE
                          Tail^.Flink := Trans;
                  Tail := Trans
                END; {FIFO}

            52 : BEGIN {LIFO - Ubacivanje na pocetak reda}
                  Trans^.Flink := Head;
                  Trans^.Blink := NIL;
                  IF Head = NIL THEN Tail := Trans ELSE
                           Head^.Blink := Trans;
                  Head := Trans
                END;{LIFO}
            ELSE Error (4)
          END; {CASE}
          Trans^.Ch := USR_ch;
          Tht := Tht + ( C1 - C0 ) * 1.0E0 * Ch;
          Ca := Tht / C1;
          IF Cc > 0 THEN Ct := Tht / Cc;
          C0 := C1;
          Cc := Cc + 1;
          Ch := Ch + 1;
          IF Ch > Cm THEN Cm := Ch;
        END {WITH}
   END
END; {Link}


PROCEDURE Unlink;

VAR Adrs1,Adrs2,Adrs3,N,i : LONGINT;
    Tr : Tra_ptr;

BEGIN {Unlink}
  FOR i := 1 TO 4 DO Op_val (Blk_tbl [Trans^.Nba]^.Op [i],R[i]);
  Adrs1 := TRUNC(R [1]); Adrs2 := TRUNC(R [2]); Adrs3 := TRUNC(R [4]);
  N := TRUNC(R [3]);
  IF Adrs1 > Max_cha THEN Error (40) ELSE
   IF Cha_tbl [Adrs1] = NIL THEN Error (41) ELSE
    WITH Cha_tbl [Adrs1]^ DO
     BEGIN
       i := 0;
       WHILE (i < N ) AND ( Head <> NIL) DO
        BEGIN  {Izbacivanje trans. iz UC u CEC}
          i := i + 1;
          Tr := Head;
          IF Head^.Flink <> NIL THEN Head^.Flink^.Blink := NIL
             ELSE Tail := NIL;
          Head := Head^.Flink;
          Tr^.Nba := Adrs2;
          Tr^.Bdt := C1;
          Cec_ins (Tr)
        END; {WHILE}
       Tht := Tht + ( C1 - C0 ) * 1.0E0 * Ch;
       Ca := Tht / C1;
       Ct := Tht / Cc;
       Ch := Ch - i;
       C0 := C1;
       Trans^.Cb := Trans^.Nba;
       Trans^.Nba := Adrs3;
       Scf := TRUE
     END  {WITH}
END; {Unlink}
{}
{I QUEUE.TXT}
PROCEDURE Que;
{
}
VAR Adrs: LONGINT;
{
}
BEGIN {Queue}
  Op_val (Blk_tbl [Trans^.Nba]^.Op [1],R [1]);
  Adrs := TRUNC(R [1]);
  IF Adrs > Max_que THEN Error (36) ELSE
   BEGIN
    IF Que_tbl [Adrs] = NIL THEN
     BEGIN
       NEW (Que_tbl [Adrs]);
       WITH Que_tbl [Adrs]^ DO
        BEGIN   {Anuliranje statistike}
          Q := 0;
          Qa := 0;
          Qc := 0;
          Qm := 0;
          Qt := 0;
          Qx := 0;
          Qz := 0;
          Tht := 0;
          C0 := 0
        END; {WITH}
     END;
{}
  {Azuriranje statistike}
{}
    WITH Que_tbl [Adrs]^ DO
        BEGIN
          Trans^.Qin := C1;
          Tht := Tht + (C1 - C0) * 1.0E0 * Q;
          Qa := Tht / C1;
          IF Qc > 0 THEN Qt := Tht / Qc;
          C0 := C1;
          Q := Q + 1;
          Qc := Qc + 1;
          IF Q > Qm THEN Qm := Q;
          Trans^.Cb := Trans^.Nba;
          Trans^.Nba := Trans^.Nba + 1
        END {WITH}
   END
END; {Queue}
{
}
PROCEDURE Depart;
{
}
VAR Adrs : LONGINT;
{
}
BEGIN {Depart}
  Op_val (Blk_tbl [Trans^.Nba]^.Op [1],R [1]);
  Adrs := TRUNC(R [1]);
  IF Adrs > Max_Que THEN Error (36) ELSE
   IF Que_tbl [Adrs] = NIL THEN Error (37) ELSE
    WITH Que_tbl [Adrs]^ DO
      BEGIN  {Azuriranje statistike}
        IF Trans^.Qin = C1 THEN Qz := Qz + 1; {Zero entry}
        Trans^.Qin := 0;
        Tht := Tht + (C1 - C0) * 1.0E0 * Q;
        Qa := Tht / C1;
        Qt := Tht / Qc;
        C0 := C1;
        Q := Q - 1;
        Trans^.Cb := Trans^.Nba;    {Azuriranje pokazivaca}
        Trans^.Nba := Trans^.Nba + 1
      END
END; {Depart}
{}
{I SEIZE.TXT}
PROCEDURE Seize;

VAR Adres : LONGINT;

{I FECREM.TXT}
PROCEDURE Fec_rem (VAR Tr:Tra_ptr);

BEGIN  {Fec_rem}
  IF Tr^.Blink = NIL THEN
   BEGIN {Azurira se Head FEC}
    Fec.Head := Tr^.Flink;
    IF Fec.Head <> NIL THEN  Fec.Head^.Blink  := NIL ELSE Fec.Tail := NIL;
   END
   ELSE IF Tr^.Flink = NIL THEN
    BEGIN {Azurira se tail FEC}
      Fec.Tail := Tr^.Blink;
      IF Fec.Tail <> NIL THEN Fec.Tail^.Flink := NIL ELSE Fec.Head := NIL;
    END ELSE
     BEGIN {Azuriraju se pokazivaci prethodne i sledece trans}
      Tr^.Flink^.Blink := Tr^.Blink;
      Tr^.Blink^.Flink := Tr^.Flink
     END
END; {Fec_rem}
{}
BEGIN {Seize}
 IF Blk_entry (Trans^.Nba) THEN
  BEGIN
   Op_val (Blk_tbl [Trans^.Nba]^.Op [1],R [1]);
   Adres := TRUNC(R[1]);
   IF Adres > Max_fac THEN Error (38) ELSE
    IF Fac_tbl [Adres] = NIL THEN
     BEGIN
      NEW (Fac_tbl [Adres]);
      initFacPtr( Fac_Tbl [Adres]);
      WITH Fac_tbl [Adres]^ DO
         BEGIN
           F := 0; Fc := 0;
           Fr := 0; Ft := 0;
           C0 := 0; Sc := 0;
           Pc := 0; Tht := 0;
           Tr := NIL; Head := NIL
         END
     END;
     WITH Fac_tbl [Adres]^ DO
       BEGIN
        IF F = 1 THEN
          BEGIN {Prijempcija}
            Fec_rem (Tr);
            Tr^.Flink := Head;
            Tr^.Bdt := Tr^.Bdt - C1;
            Head := Tr;
            Pc := Pc + 1
          END;
        Tr := Trans;
        Tht := Tht + (C1 - C0) * 1.0E0 * F;
        Fr := Tht / C1;
        IF Fc > 0 THEN Ft := Tht / Fc;
        C0 := C1;
        F := 1;
        Sc := Sc + 1;
        Fc := Fc + 1;
        Trans^.Cb := Trans^.Nba;
        Trans^.Nba := Trans^.Nba + 1
       END
  END ELSE Trans^.Sf := FALSE
END; {Seize}


PROCEDURE Release;
VAR Adres : LONGINT;
    Tr1 : Tra_ptr;
    Flg : BOOLEAN;

BEGIN {Release}
  Op_val (Blk_tbl [Trans^.Nba]^.Op [1],R [1]);
  Adres := TRUNC (R [1]);
  IF Adres > Max_fac THEN Error (38) ELSE
   IF Fac_tbl [Adres] = NIL THEN Error (39) ELSE
    WITH Fac_tbl [Adres]^ DO
     BEGIN
       Flg := FALSE;
       IF Head <> NIL THEN
         BEGIN  {Oslobadjanje prijemptovane trans}
           Tr1 := Head;
           Head := Tr1^.Flink;
           Tr1^.Bdt := Tr1^.Bdt + C1;
           Fec_ins (Tr1);
           Pc := Pc - 1;
           Flg := TRUE
         END;
       Sc := Sc - 1;
       Tht := Tht + (C1 - C0) * 1.0E0 * F;
       Fr := Tht / C1;
       Ft := Tht / Fc;
       C0 := C1;
       Trans^.Cb := Trans^.Nba;
       Trans^.Nba := Trans^.Nba + 1;
       IF Flg THEN F := 1 ELSE F := 0
     END;
  Scf := TRUE
END; {Release}
{}
{I TABULATE.TXT}
PROCEDURE Tabulate;
VAR n,Adrs : LONGINT;

BEGIN {Tabulate}
  Op_val (Blk_tbl [Trans^.Nba]^.Op [1], R [1]);
  Adrs := TRUNC(R [1]);
  IF Adrs > round(Max_tab * 0.9) THEN Extend_Tab_Tbl;
  IF Tab_tbl [Adrs] = NIL THEN Error (27) ELSE
     WITH Tab_tbl [Adrs]^ DO
      BEGIN
        Op.Typ := Sna_op;
        Op.S_op := Arg;
        Op_val (Op,R[1]); {Racunanje vrednosti argumenta}
        Tc := Tc + 1; {Povecanje brojaca ulazaka u tabelu}
        Sa := Sa + R [1]; {Povecanje sume argumenata}
        Tb := Sa / Tc; {Racunanje srednje vrednosti argumenta}
        Td := Td + (R[1]*R[1]-Td) / Tc;{Kof.Td za varijansu}
        n := TRUNC(1.999 + (R[1] - I [1].Ul) / Si); {Broj interv.vidi TRUNC??}
        IF n > Ni THEN n := Ni + 1  {Ogranicenje n za overflow}
          ELSE IF n < 1 THEN n := 1;{      i underflow        }
        I [n].Off := I [n].Off + 1; {Azuriranje br. frekvencija}
        IF n = Ni + 1 THEN Avo := Avo + R [1];{Suma  overflow-a}
        Trans^.Cb := Trans^.Nba; {Azuriranje pokazivaca blokova}
        Trans^.Nba := Trans^.Nba + 1
      END {WITH}
END; {Tabulate}
{}
{I TERMINAT.TXT}
PROCEDURE Terminate;
{
}
BEGIN {Terminate}
  Op_val (Blk_tbl [Trans^.Nba]^.Op [1],R[1]);
  Tc := Tc - TRUNC(R [1]);
  Cec_rem (Trans);
  IF Trans^.P <> NIL THEN DISPOSE (Trans^.P);
  DISPOSE (Trans);
  Trans := NIL;
END; {Terminate}
{}
{I TEST.TXT}
PROCEDURE Mark;
 BEGIN  {Mark}
   Trans^.Mp := Ca;
   Trans^.Cb := Trans^.Nba;
   Trans^.Nba := Trans^.Nba + 1
 END; {Mark}


PROCEDURE Test;
 VAR Adrs,Pfx : LONGINT;
         Flg : BOOLEAN;
BEGIN {Test}
  FOR i := 1 TO 4 DO Op_val (Blk_tbl [Trans^.Nba]^.Op[i],R [i]);
  Pfx := TRUNC(R [1]);
  Adrs :=TRUNC(R [4]);
  Flg := FALSE;
  k := Trans^.Nba;
  IF Trans^.Cb <> Trans^.Nba THEN
   BEGIN
     Blk_tbl [Trans^.Nba]^.N := Blk_tbl [Trans^.Nba]^.N + 1;
     Blk_tbl [Trans^.Nba]^.W := Blk_tbl [Trans^.Nba]^.W + 1
   END;
  CASE Pfx OF
    1 : IF R [2] > R [3] THEN Flg := TRUE;  {G}
    2 : IF R [2] >= R [3] THEN Flg := TRUE; {GE}
    3 : IF R [2] = R [3] THEN Flg := TRUE;  {E}
    4 : IF R [2] <> R [3] THEN Flg := TRUE; {NE}
    5 : IF R [2] < R [3] THEN Flg := TRUE;  {L}
    6 : IF R [2] <= R [3] THEN Flg := TRUE; {LE}
    ELSE Error (24)
  END; {CASE}
  IF Flg THEN Trans^.Nba := Trans^.Nba + 1
         ELSE IF Adrs > 0 THEN Trans^.Nba := Adrs
             ELSE Trans^.Sf := FALSE;
  IF  Trans^.Cb <> k THEN {Azuriranje brojaca predh. bloka}
      Blk_tbl [Trans^.Cb]^.W := Blk_tbl [Trans^.Cb]^.W - 1;
  Trans^.Cb := k
END; {Test}
{}
PROCEDURE Logic;
VAR Adrs,Pfx : LONGINT;
BEGIN {Logic}
 FOR i := 1 TO 2 DO Op_val (Blk_tbl [Trans^.Nba]^.Op[i],R [i]);
 Pfx := TRUNC(R [1]);
 Adrs :=TRUNC(R [2]);
 IF Adrs > round (Max_log * 0.9)
    THEN Extend_Log_Tbl;
    // ELSE
  CASE Pfx OF
   1 : Log_tbl [Adrs] := FALSE;              {R}
   2 : Log_tbl [Adrs] := TRUE;               {S}
   3 : Log_tbl [Adrs] := NOT Log_tbl [Adrs]; {I}
   ELSE Error (25)
  END;{CASE}
 Trans^.Cb := Trans^.Nba;
 Trans^.Nba := Trans^.Nba + 1
END; {Logic}
{
}
PROCEDURE Gate;
VAR Pfx,Lab,Adrs : LONGINT;
    Flg : BOOLEAN;
BEGIN {Gate}
  FOR i := 1 TO 3 DO Op_val (Blk_tbl [Trans^.Nba]^.Op[i],R[i]);
  Pfx := TRUNC(R [1]);
  Adrs := TRUNC(R [2]);
  Lab := TRUNC(R [3]);
  Flg := FALSE;
  k := Trans^.Nba;
  IF Trans^.Cb <> Trans^.Nba THEN
     BEGIN
       Blk_tbl [Trans^.Nba]^.N := Blk_tbl [Trans^.Nba]^.N + 1;
       Blk_tbl [Trans^.Nba]^.W := Blk_tbl [Trans^.Nba]^.W + 1
     END;
  CASE Pfx OF
    1 : BEGIN
         IF Adrs > round (Max_log * 0.9)
                THEN Extend_Log_Tbl;
                //ELSE
          IF Log_tbl [Adrs] = FALSE THEN Flg := TRUE; {LR}
        END;
    2 : BEGIN
         IF Adrs > round (Max_log * 0.9)
                THEN Extend_Log_Tbl;
                //ELSE
          IF Log_tbl [Adrs] = TRUE THEN Flg := TRUE;  {LS}
        END;
    3 : BEGIN
          IF Sto_tbl [Adrs] = NIL THEN Error (33) ELSE
           IF Sto_tbl [Adrs]^.S = 0 THEN Flg := TRUE;  {SE}
        END;
    4 : BEGIN
          IF Sto_tbl [Adrs] = NIL THEN Error (33) ELSE
           IF Sto_tbl [Adrs]^.S = Sto_tbl [Adrs]^.R THEN Flg := TRUE;  {SF}
        END;

    5 : BEGIN
          IF Sto_tbl [Adrs] = NIL THEN Error (33) ELSE
           IF Sto_tbl [Adrs]^.S > 0 THEN Flg := TRUE;  {SNE}
        END;
    6 : BEGIN
          IF Sto_tbl [Adrs] = NIL THEN Error (33) ELSE
           IF Sto_tbl [Adrs]^.S < Sto_tbl [Adrs]^.R THEN Flg := TRUE;  {SNF}
        END;
    7 : BEGIN
          IF Fac_tbl [Adrs] = NIL THEN Flg := TRUE ELSE
           IF Fac_tbl [Adrs]^.F = 0 THEN Flg := TRUE;  {NU}
        END;
    8 : BEGIN
          IF Fac_tbl [Adrs] = NIL THEN Flg := FALSE ELSE
           IF Fac_tbl [Adrs]^.F = 1 THEN Flg := TRUE;  {U}
        END;
    ELSE Error (26)
  END;{Case}
  IF Flg THEN Trans^.Nba := Trans^.Nba + 1
         ELSE IF Lab > 0 THEN Trans^.Nba := Lab
                            ELSE Trans^.Sf := FALSE;{Ostaje}
  IF Trans^.Cb <> k THEN
       Blk_tbl [Trans^.Cb]^.W := Blk_tbl [Trans^.Cb]^.W - 1;
  Trans^.Cb := k
END; {Gate}
{}
{I TRANSFER.TXT}
PROCEDURE Transfer;
{
}
VAR Blk_1,Blk_2 : LONGINT;
{
}
BEGIN {Transfer}
  FOR i := 1 TO 3 DO Op_val(Blk_tbl[Trans^.Nba]^.Op[i],R[i]);
  Blk_1 := TRUNC(R [2]);
  Blk_2 := TRUNC(R [3]);
  k := Trans^.Nba; {Sacuva se broj bloka}
  IF Trans^.Cb <> Trans^.Nba THEN
   BEGIN {Azuriranje brojaca pri prvom ulasku u Transfer}
     BLk_tbl [Trans^.Nba]^.N := BLk_tbl [Trans^.Nba]^.N + 1;
     BLk_tbl [Trans^.Nba]^.W := BLk_tbl [Trans^.Nba]^.W + 1;
   END;
  IF R [1] = 1000 THEN {Transfer BOTH}
      IF Blk_entry (Blk_1) THEN  {Ulaz u prvi blok}
          Trans^.Nba := Blk_1 ELSE
            IF Blk_entry (Blk_2) THEN {Ulaz u drugi blok}
               Trans^.Nba := Blk_2
                 ELSE Trans^.Sf := FALSE {Trans. ne moze dalje}
    ELSE
      BEGIN {Statisticki i bezuslovni transfer}
        Randu (IntRnd_tbl [3]);
        IF IntRnd_tbl [3].Val >= R [1] THEN Trans^.Nba := Blk_1
           ELSE Trans^.Nba := Blk_2
      END;
  IF Trans^.Cb <> k THEN
           Blk_tbl [Trans^.Cb]^.W := Blk_tbl [Trans^.Cb]^.W - 1;
  Trans^.Cb := k
END; {Transfer}
{}
BEGIN {Move_trans}
  REPEAT
   { Pokretanje transakcije kroz Curr_blk ako je moguce }
   Prev_blk := Trans^.Cb; {Blok u kome se nalazi trans.}
   Curr_blk := Trans^.Nba;{Blok u koji trans. treba da ide}
   IF Curr_blk > Max_blk THEN Error (3) ELSE
    BEGIN
     IF (Blk_tbl [Prev_blk]^.Op_cod = ocError21) AND  Trans^.Sf THEN
          BEGIN {Ubacena trans. iz Generate}
            Blk_tbl [Prev_blk]^.N :=  Blk_tbl [Prev_blk]^.N + 1;
            Blk_tbl [Prev_blk]^.W :=  Blk_tbl [Prev_blk]^.W + 1;
          END;
     Trans^.Sf := TRUE;
     CASE Blk_tbl [Curr_blk]^.Op_cod OF
        ocAdvance : Advance;
        ocAssign : Assign;
        ocDepart : Depart;
        ocEnter : Enter;
        ocGate : Gate;
        ocError21 : Error (21);
       ocLeave : Leave;
       ocLink : Link;
       ocLogic : Logic;
       ocMark : Mark;
       ocQue : Que;
       ocRelease : Release;
       ocSavevalue : Savevalue;
       ocSeize : Seize;
       ocTabulate : Tabulate;
       ocTerminate : Terminate;
       ocTest : Test;
       ocTransfer : Transfer;
       ocUnlink : Unlink;
       ELSE Error (22)
     END; {CASE}
     IF Trans <> NIL THEN
      BEGIN
        IF Trans^.Sf AND NOT (Blk_tbl [Curr_blk]^.Op_cod IN [ ocGate,
                                                              ocTest,
                                                              ocTransfer])
         THEN {Nisu Transfer , Test i Gate}
          BEGIN {Azuriranje brojaca predhodnog i tekuceg bloka}
            Blk_tbl [Prev_blk]^.W := Blk_tbl [Prev_blk]^.W - 1;
            Blk_tbl [Curr_blk]^.W := Blk_tbl [Curr_blk]^.W + 1;
            Blk_tbl [Curr_blk]^.N := Blk_tbl [Curr_blk]^.N + 1
          END;
      END
        ELSE
          BEGIN {Terminate}
            Blk_tbl [Prev_blk]^.W := Blk_tbl [Prev_blk]^.W - 1;
            Blk_tbl [Curr_blk]^.N := Blk_tbl [Curr_blk]^.N + 1
          END
    END
  UNTIL ((Trans = NIL) OR Scf OR NOT Trans^.Sf OR Stop)

END; {Move_trans}

BEGIN {SP}
  Scf := FALSE;
  Trans := CEC.Head;
  WHILE Trans <> NIL DO
    BEGIN
      Move_trans;
      IF  (Trans <> NIL) AND NOT Scf THEN Trans := Trans^.Flink
       ELSE
         BEGIN
           Scf := FALSE;
           Trans := CEC.Head
         END
    END
END; {SP}

{I REPORT.TXT}
PROCEDURE Report;
{}
VAR Flg : BOOLEAN;{Premestio ovde, bilo je posle deklaracije REport, vratio}
    i : LONGINT;
{}
PROCEDURE Out_blk;
{Stampa rezultate simulacije za aktivne blokove}
VAR i : LONGINT;
counter: LONGINT;
block: Array of Integer;
current: Array of Integer;
total: Array of Integer;
blockMixin: String;
currentMixin: String;
totalMixin: String;
template: String;

BEGIN {Out_blk}
  template := '{ "blockCount" : { "block": [@block@], "current": [@current@], "total": [@total@] } },';
  
  counter := 0;

   FOR i := 1 TO Max_blk DO
    IF Blk_tbl[i] <> NIL THEN
      counter := counter + 1;
      
   SetLength(block, counter);
   SetLength(current, counter);
   SetLength(total, counter);

  FOR i := 1 TO counter DO
   IF Blk_tbl[i] <> NIL THEN
   begin
    block[i-1] := i;
    current[i-1] := Blk_tbl[i]^.W;
    total[i-1] := Blk_tbl[i]^.N;
   end;

    for i := 0 to counter do
      if(i <> counter-1) then
        begin
          blockMixin := blockMixin + IntToStr(block[i]) + ',';
          currentMixin := currentMixin + IntToStr(current[i]) + ',';
          totalMixin := totalMixin + IntToStr(total[i]) + ',';
        end
      else
      begin
          blockMixin := blockMixin + IntToStr(block[i]);
          currentMixin := currentMixin + IntToStr(current[i]);
          totalMixin := totalMixin + IntToStr(total[i]);

          template := StringReplace(template, '@block@', blockMixin, [rfReplaceAll]);
          template := StringReplace(template, '@current@', currentMixin, [rfReplaceAll]);
          template := StringReplace(template, '@total@', totalMixin, [rfReplaceAll]);

          mRez.WRITELN(template);
          break;
      end;

END; {Out_blk}
{}
PROCEDURE Out_sto;
{Stampa rezultate simulacije za aktivne STORAGE}
VAR i : LONGINT;strana : TTabSheet;tabela : TStringGrid; counter: LONGINT;
storage: String; capacity: String; avgCont: String; avgUtil: String; template: String;
entr: String; avgTT: String; currCt: String; maxCt: String; iStorage: Array of String;
 iCapacity: Array of String; iavgCont: Array of String; iavgUtil: Array of String; iEntr: Array of String;
iavgTT: Array of String; icurrCt: Array of String; imaxCt: Array of String;
BEGIN {Out_sto}
// mRez.WRITELN ('Storage Capacity  Average   Average   Entries' +
//               '  Average  Current  Maximum');
// mRez.WRITELN ('                 Contents Utilisation        ' +
//               ' Time/tran Contents Contents');
template := '{ "storageSet" : { "storage": [@storage@], "capacity": [@capacity@], "averageContents": [@avgCont@],  "averageUtilisation": [@avgUtil@], "entries": [@entr@], "averageTimeTran": [@avgTT@], "currentContents": [@currCt@], "maxContents": [@maxCt@] } },';
counter := 0;

NapraviPC;
strana := TTabSheet.Create(PC);
strana.pagecontrol := pc;
strana.parent := pc;
strana.caption :=mStorages;
strana.tag := 1;
tabela := TStringGrid.create(strana);
tabela.parent := strana;
tabela.Align := ALClient;
tabela.DefaultColWidth := trunc(Roditelj.parent.width/8)-3;
tabela.ColCount := 8;
tabela.RowCount := 2;
tabela.fixedrows := 1;
tabela.fixedcols := 0;
tabela.cells[0,0] := mStorage;
tabela.cells[1,0] := mCapacity;
tabela.cells[2,0] := mAverageContents;
tabela.cells[3,0] := mAverageUtilization;
tabela.cells[4,0] := mNoEntries;
tabela.cells[5,0] := mAverageTimeTran;
tabela.cells[6,0] := mCurrentContents;
tabela.cells[7,0] := mMaxContents;
tabela.Options := tabela.Options + [goColSizing];

FOR i:= 1 to Max_sto DO
  IF Sto_tbl [i] <> NIL THEN
    counter := counter + 1;

SetLength(iStorage, counter);
SetLength(iCapacity, counter);
SetLength(iavgCont, counter);
SetLength(iavgUtil, counter);
SetLength(ientr, counter);
SetLength(iavgTT, counter);
SetLength(icurrCt, counter);
SetLength(imaxCt, counter);

FOR i := 1 TO counter DO
  WITH Sto_tbl [i]^ DO
   BEGIN
    iStorage[i-1] := inttostr(i);
    iCapacity[i-1] := inttostr(r);
    iavgCont[i-1] := floattostr(round(sa*100)/100);
    iavgUtil[i-1] := floattostr(round(sr*100)/100);
    ientr[i-1] := inttostr(sc);
    iavgTT[i-1] := floattostr(round(st*100)/100);
    icurrCt[i-1] := inttostr(s);
    imaxCt[i-1] := inttostr(sm);
    {
    mRez.WRITELN (StrExp(IntToStr(i),5) + StrExp(IntToStr(R),11) + StrExp(FormatFloat('.000',Sa),9) + StrExp(formatFloat('.000', Sr),12)  + StrExp(IntToStr(Sc), 8) + StrExp(formatFloat('.000', St),10) + StrExp(IntToStr(S),9) + StrExp(IntToStr(Sm),9));
             tabela.rowcount := i+1;
             tabela.cells[0,i] :=inttostr(i);
             tabela.cells[1,i] :=inttostr(r);
             tabela.cells[2,i] :=floattostr(round(sa*100)/100);
             tabela.cells[3,i] :=floattostr(round(sr*100)/100);
             tabela.cells[4,i] :=inttostr(sc);
             tabela.cells[5,i] :=floattostr(round(st*100)/100);
             tabela.cells[6,i] :=inttostr(s);
             tabela.cells[7,i] :=inttostr(sm);
    }
   END;

for i := 0 to counter do
      if(i <> counter-1) then
        begin
          storage := storage + iStorage[i] + ',';
          capacity := capacity + iCapacity[i] + ',';
          avgCont := avgCont + '[' + iavgCont[i] + '],';
          avgUtil := avgUtil + '[' + iavgUtil[i] + '],';
          entr := entr + ientr[i] + ',';
          avgTT := avgTT + '[' + iavgTT[i] + '],';
          currCt := currCt + icurrCt[i] + ',';
          maxCt := maxCt + imaxCt[i] + ',';
        end
      else
      begin
          storage := storage + iStorage[i];
          capacity := capacity + iCapacity[i];
          avgCont := avgCont + '[' + iavgCont[i] + ']';
          avgUtil := avgUtil + '[' + iavgUtil[i] + ']';
          entr := entr + ientr[i];
          avgTT := avgTT + '[' + iavgTT[i] + ']';
          currCt := currCt + icurrCt[i];
          maxCt := maxCt + imaxCt[i];

          template := StringReplace(template, '@storage@', storage, [rfReplaceAll]);
          template := StringReplace(template, '@capacity@', capacity, [rfReplaceAll]);
          template := StringReplace(template, '@avgCont@', avgCont, [rfReplaceAll]);
          template := StringReplace(template, '@avgUtil@', avgUtil, [rfReplaceAll]);
          template := StringReplace(template, '@entr@', entr, [rfReplaceAll]);
          template := StringReplace(template, '@avgTT@', avgTT, [rfReplaceAll]);
          template := StringReplace(template, '@currCt@', currCt, [rfReplaceAll]);
          template := StringReplace(template, '@maxCt@', maxCt, [rfReplaceAll]);

          mRez.WRITELN(template);
          break;
      end;
// mRez.WRITELN ('')
END; {Out_sto}
{}
PROCEDURE Out_tab;
{Stampa rezultate simulacije za aktivne tabele}
{}
VAR Cf,Sda,Prct,Cpr,Cre,Mom,Dfm : REAL;
    i, counter : LONGINT;strana : TTabSheet; tabela : TStringGrid;index : integer;
    template, tableTemplate, innerTable, tableRowDump, tableDump, padding: String; templateHeaders, tableRows: Array of String;   // templateHeaders i tableRows se na kraju iteracije brisu
BEGIN
  counter := 0;
  padding := ' { "tableData": [ @entries@ ] }';
  template := ' "tables":[ @tableTemplate@ ],';
  tableTemplate := ' { "entries": @ttentries@, "marg": "@ttmarg@", "stdev": "@ttstdev@", "sumarg": "@ttsumarg@", "table": [ @innerTable@ ] }';
  innerTable := ' { "uplim": @ituplim@ , "obfr": @itobfr@ , "pertot": "@itpertot@", "cumpert": "@itcumpert@", "cumrem": "@itcumrem@", "mulofmean": "@itmulofmean@", "devfrommean": "@itdevfrommean@" }';
  // stavi se header tabele, to je tableTemplate, onda se vrti innerTable za svaki red tabele i na kraju se nalepi u template
 FOR k := 1 TO Max_tab DO
  IF Tab_tbl [k] <> NIL THEN
    counter := counter + 1;

 SetLength(templateHeaders, counter);


 FOR k := 1 TO counter DO
  WITH Tab_tbl [k]^ DO
  BEGIN
   IF (Tc - 1) * (Td - Tb * Tb) > 0 THEN                                        
   Sda := SQRT(Tc / (Tc -1) * (Td - Tb * Tb)) {Stand.devijacija}
     ELSE Sda := 0;


   templateHeaders[k-1] := StringReplace(tableTemplate, '@ttentries@', IntToStr(Tc), [rfReplaceAll]);
   templateHeaders[k-1] := StringReplace(templateHeaders[k-1], '@ttstdev@', formatFloat('.000', Sda), [rfReplaceAll]);
   templateHeaders[k-1] := StringReplace(templateHeaders[k-1], '@ttsumarg@', formatFloat('.000', Sa), [rfReplaceAll]);
   templateHeaders[k-1] := StringReplace(templateHeaders[k-1], '@ttmarg@', formatFloat('.000', Tb), [rfReplaceAll]);

   SetLength(tableRows, Ni);

   FOR j := 1 TO Ni DO
    BEGIN
     IF Tc > 0 THEN  {Izbegavanje deljenja sa nulom}
     BEGIN
       Prct := I [j].Off / Tc * 100; {Procenat od ukupno}
       Cf := Cf + I [j].Off; {Kumulativne frekvencije}
       Cpr := Cf / Tc * 100; {Procenat kum. frekvencija}
       Cre:= (Tc - Cf) / Tc * 100; {Procenat ost. frekv.}
       IF Tb > 0 THEN
         Mom := I [j].Ul / Tb {Multiple of mean}
            ELSE Mom := 0;
       IF Sda > 0 THEN
         Dfm := (I [j].Ul - Tb) / Sda  {Deviation from mean}
           ELSE Dfm := 0;
     END ELSE
          BEGIN
             Prct := 0;
             Cpr := 0;
             Cre := 0;
             Mom := 0;
             Dfm := 0
          END;
      {                                                                         TO-DO
      IF (j = Ni + 1) AND (I [j].Off > 0) THEN
        BEGIN
         tabela.rowcount := j+1;
         mRez.WRITELN ('Overflow'  + StrExp(IntToStr(I[j].Off),10) + StrExp(formatFloat('.000', Prct),9) + StrExp(formatFloat('.000', Cpr),11));
         tabela.cells[0,j] :='Overflow';
         tabela.cells[1,j] :=floattostr(I[j].Off);
         tabela.cells[2,j] :=FloatToStr(round(Prct*100)/100);
         tabela.cells[3,j] :=floattostr(round(Cpr*100)/100);
         mRez.WRITELN ('Average value of overflow' + StrExp(formatFloat('.000', Avo/I[j].Off), 11))
        END
      }
       //  ELSE  IF j <= Ni THEN
       //    begin
             tableRows[j-1] := StringReplace(innerTable, '@ituplim@', IntToStr(I[j].Ul), [rfReplaceAll]);
             tableRows[j-1] := StringReplace(tableRows[j-1], '@itobfr@', IntToStr(I[j].Off), [rfReplaceAll]);
             tableRows[j-1] := StringReplace(tableRows[j-1], '@itpertot@', formatFloat('.000', Prct), [rfReplaceAll]);
             tableRows[j-1] := StringReplace(tableRows[j-1], '@itcumpert@', formatFloat('.000',Cpr), [rfReplaceAll]);
             tableRows[j-1] := StringReplace(tableRows[j-1], '@itcumrem@', formatFloat('.000',Cre), [rfReplaceAll]);
             tableRows[j-1] := StringReplace(tableRows[j-1], '@itmulofmean@', formatFloat('.000', Mom), [rfReplaceAll]);
             tableRows[j-1] := StringReplace(tableRows[j-1], '@itdevfrommean@', formatFloat('.000', Dfm), [rfReplaceAll]);
             //ShowMessage(IntToStr(length(tableRows)));

    END; // FOR za redove
   //mRez.WRITELN ('')
   tableRowDump := '';
   for j:= 0 to Ni-1 do
    if(j <> Ni-1) then
      tableRowDump := tableRowDump + tableRows[j] + ','
   else
      tableRowDump := tableRowDump + tableRows[j];

   templateHeaders[k-1] := StringReplace(templateHeaders[k-1], '@innerTable@', tableRowDump, [rfReplaceAll]);
  END; {WITH}

  for j:=0 to counter-1 do
    if(j <> counter-1) then
        tableDump := tableDump + templateHeaders[j] + ','
      else
       tableDump := tableDump + templateHeaders[j];

  tableTemplate := StringReplace(tableDump, '@tableTemplate@', tableDump, [rfReplaceAll]);
  padding := StringReplace(padding, '@entries@', tableTemplate, [rfReplaceAll]);
 //mRez.WRITELN ('')
 //END; // FOR veliki
 mRez.WRITELN(padding + ',');
END;{Out_tab}
{}
PROCEDURE Out_que;

VAR
Qzc : REAL;  {Procenat nultih prolaza}
    i:LONGINT;
    counter:LONGINT;
    template:String;
    headers:String;
    tableDump:String;
    tableRows:Array of String;
BEGIN {Outque}

   template := '{ "queueData": [@headers@] },';
   headers := '{ "queue": @queue@, "maxCont": "@maxCont@", "avgCont": "@avgCont@", "totEntr": "@totEntr@", "zeroEntr": "@zeroEntr@", "percentZeros": "@percentZeros@", "avgtimetrans": "@avgtimetrans@", "currCont": "@currCont@"}';
   counter := 0;

   FOR i := 1 to Max_que DO
    IF Que_tbl [i] <> NIL THEN
      counter := counter + 1;

   SetLength(tableRows, counter);

  FOR i := 1 TO counter DO
     WITH Que_tbl [i]^ DO
      BEGIN
       IF Qc > 0 THEN Qzc := 1.0E2 * Qz / Qc ELSE Qzc := 0;
       tableRows[i-1] := StringReplace(headers, '@queue@', StrExp(IntToStr(i),5), [rfReplaceAll]);
       tableRows[i-1] := StringReplace(tableRows[i-1], '@maxCont@', StrExp(IntToStr(Qm),8), [rfReplaceAll]);
       tableRows[i-1] := StringReplace(tableRows[i-1], '@avgCont@', StrExp(formatFloat('.000', Qa),10), [rfReplaceAll]);
       tableRows[i-1] := StringReplace(tableRows[i-1], '@totEntr@', StrExp(IntToStr(Qc),7), [rfReplaceAll]);
       tableRows[i-1] := StringReplace(tableRows[i-1], '@zeroEntr@', StrExp(IntToStr(Qz),8), [rfReplaceAll]);
       tableRows[i-1] := StringReplace(tableRows[i-1], '@percentZeros@', StrExp(formatFloat('.000', Qzc),8), [rfReplaceAll]);
       tableRows[i-1] := StringReplace(tableRows[i-1], '@avgtimetrans@', StrExp(formatFloat('.000',Qt),11), [rfReplaceAll]);
       tableRows[i-1] := StringReplace(tableRows[i-1], '@currCont@', StrExp(IntToStr(Q),10), [rfReplaceAll]);
      END;
  tableDump := '';
  FOR i := 0 to counter-1 DO
    IF(i <> counter-1) THEN
      tableDump := tableDump + tableRows[i] + ','
    ELSE
      tableDump := tableDump + tableRows[i];
      
  template := StringReplace(template, '@headers@', tableDump, [rfReplaceAll]);

  mRez.WRITELN (template);
END; {Out_que}
{}
PROCEDURE Out_fac;
VAR i : LONGINT;tabela : TStringGrid; strana : TTabSheet;
BEGIN {Out_fac}
 {mRez.WRITELN ('');
   NapraviPC;
   strana := TTabSheet.Create(PC);
   strana.pagecontrol := pc;
   strana.parent := pc;
   strana.tag := 3;
   strana.caption :=mFacilities;
   tabela := TStringGrid.create(strana);
   tabela.parent := strana;
   tabela.Align := ALClient;
   tabela.ColCount := 6;
   tabela.RowCount := 2;
   tabela.FixedRows := 1;
   tabela.fixedcols := 0;
   tabela.DefaultColWidth := trunc(Roditelj.parent.width/6)-3;
   tabela.Options := tabela.Options + [goColSizing];
 mRez.WRITELN ('Facility   Average    Number  Average' +
               '   Seizing   Preempting');
 mRez.WRITELN ('         utilisation entries time/tran' +
               '  transact. transaction');
   tabela.cells[0,0] := mFacility;tabela.cells[1,0] := mAverageUtilization;tabela.cells[2,0] := mNoEntries;
   tabela.cells[3,0] := mAverageTimeTran;tabela.cells[4,0] := mSeizingTran;tabela.cells[5,0] := mPreemptingTran;
 FOR i := 1 TO Max_fac DO
  IF Fac_tbl [i] <> NIL THEN
   WITH Fac_tbl [i]^ DO begin
    mRez.WRITELN (StrExp(IntToStr(i),5) + StrExp(formatFloat('.000',Fr),12) +  StrExp(IntToStr(Fc),9) + StrExp(formatFloat('.000', Ft),12) +  StrExp(IntToStr(Sc),9) + StrExp(IntToStr(Pc),12));
       tabela.RowCount := i+1;
       tabela.cells[0,i] := inttostr(i);tabela.cells[1,i] := floattostr(round(Fr*1000)/1000);
       tabela.cells[2,i] := inttostr(Fc);tabela.cells[3,i] := floattostr(round(Ft*1000)/1000);
       tabela.cells[4,i] := inttostr(Sc);tabela.cells[5,i] := inttostr(Pc);
    end;
  mRez.WRITELN ('')}
  mRez.WRITELN('{"factorials": "Not yet implemented"},');
END; {Out_fac}
{
}
PROCEDURE Out_cha;
VAR i : LONGINT;strana : TTabSheet; tabela : TStringgrid;
BEGIN {Out_cha}
  {mRez.WRITELN ('');
   NapraviPC;
   strana := TTabSheet.Create(PC);
   strana.pagecontrol := pc;
   strana.parent := pc;
   strana.tag := 4;
   strana.caption :=mUserChains;
   tabela := TStringGrid.create(strana);
   tabela.parent := strana;
   tabela.Align := ALClient;
   tabela.ColCount := 6;
   tabela.RowCount := 2;
   tabela.FixedRows := 1;
   tabela.fixedcols := 0;
   tabela.DefaultColWidth := trunc(Roditelj.parent.width/6)-3;
   tabela.Options := tabela.Options + [goColSizing];
  mRez.WRITELN ('User chain  Total    Average   Current' +
                '   Average  Maximum');
  mRez.WRITELN ('           entries time/trans contents' +
                '  contents contents');
   tabela.cells[0,0] := mUserChain;tabela.cells[1,0] := mTotalEntries;tabela.cells[2,0] := mAverageTimeTran;
   tabela.cells[3,0] := mCurrentContents;tabela.cells[4,0] := mAverageContents;tabela.cells[5,0] := mMaxContents;
  FOR i := 1 TO Max_cha DO
   IF Cha_tbl [i] <> NIL THEN WITH Cha_tbl [i]^ DO begin
    mRez.WRITELN (StrExp(IntToStr(i),5)  + StrExp(IntToStr(Cc),12) + StrExp(formatFloat('.000',Ct),11) + StrExp(IntToStr(Ch),9) + StrExp(formatFloat('.000', Ca),11) + StrExp(IntToStr(Cm),9));
      tabela.RowCount := i+1;
      tabela.cells[0,i] := inttostr(i);tabela.cells[1,i] := inttostr(Cc);
      tabela.cells[2,i] := floattostr(round(Ct*1000)/1000);tabela.cells[3,i] := inttostr(Ch);
      tabela.cells[4,i] := floattostr(round(Ca*1000)/1000);tabela.cells[5,i] := inttostr(Cm);
    end;
  mRez.WRITELN ('')  }
  mRez.WRITELN('{"chains": "Not yet implemented"},');
 END; {Out_cha}
{}

PROCEDURE Out_sav;
 VAR i : LONGINT;
BEGIN {Out_sav}
{  mRez.WRITELN ('');
  mRez.WRITELN ('SaveValues');
  FOR i:=1 TO Max_sav DO
   IF Sav_tbl[i] <> 0 THEN mRez.WRITELN ('X$' + IntToStr(i) + ' = ' + IntToStr(Sav_Tbl [i]));
  mRez.WRITELN ('')   }
END; {Out_sav}

BEGIN {Report}

 IF NOT Stop THEN
  IF P_flg THEN
  BEGIN
   IF NOT Opn_flg THEN
    BEGIN
      // REWRITE (Re_f); {Otvaranje datoteke rezultata}
      Opn_flg := True; {Setovanje flaga za otvorenu datoteku}
      mRez.WRITELN ('[{"author": "Milos Zivadinovic", "year": "2015", "message": "Si vis pacem, para bellum"}, ');
      //mRez.WRITELN ('')
    END;
{} mRez.WRITELN('{"clocks": {"relativeClock": ' + IntToStr(C1) + ', "absoluteClock": ' + IntToStr(Ca) + '}},');
   Flg := FALSE;
   FOR i := 1 TO Max_blk DO
       IF Blk_tbl [i] <> NIL THEN Flg := TRUE;
   IF Flg THEN Out_blk;
{}
   Flg := FALSE;
   FOR i := 1 TO Max_sto DO
       IF Sto_tbl [i] <> NIL THEN Flg := TRUE;
   IF Flg THEN Out_sto;
{}
   Flg := FALSE;
   FOR i := 1 TO Max_tab DO
       IF Tab_tbl [i] <> NIL THEN Flg := TRUE;
   IF Flg THEN Out_tab;
{}
   Flg := FALSE;
   FOR i := 1 TO Max_que DO
       IF Que_tbl [i] <> NIL THEN Flg := TRUE;
   IF Flg THEN Out_que;
{}
   Flg := FALSE;
   FOR i := 1 TO Max_fac DO
       IF Fac_tbl [i] <> NIL THEN Flg := TRUE;
   IF Flg THEN Out_fac;
{}
   Flg := FALSE;
   FOR i := 1 TO Max_cha DO
       IF Cha_tbl [i] <> NIL THEN Flg := TRUE;
   IF Flg THEN Out_cha;

   Flg := FALSE;
   FOR i:= 1 TO Max_Sav DO IF Sav_tbl [i] <> 0 THEN Flg := TRUE;
   IF Flg THEN Out_sav;

  END;
END; {Report}

Procedure GPSS_Start(OBJNAME : string; Kontejner : TWinControl);
BEGIN {Gpss}
  Obj_nm := OBJNAME;
  Init; {Inicijalizacija programa}
  PC.free;
  PC:=nil;
  counter := 0;
  if not assigned(Roditelj) then  Roditelj := kontejner;
  REPEAT
     //Inp_ph; {Faza ulaza}//*BojanNenadJovicic jul, avgust 2003.*
     prepareSimulation;  // prenosi podatke iz procedure asembliranja
                        // u fazu procesiranja *BojanNenadJovicic jul, avgust 2003.*
     IF Sim_flg THEN
      BEGIN
        i := 1;   {Kreiranje prvih transakcija}
        WHILE NOT((Blk_tbl [i] = NIL) OR (i > Max_blk)) DO
          BEGIN
            IF Blk_tbl [i]^.Op_cod = ocError21 THEN Cre_trans (i,0);
            i := i + 1
          END;
        WHILE (Tc > 0) AND NOT Stop  DO
          BEGIN
            CUP; {Faza azuriranja casovnika}
            SP;   {Faza skaniranja listi}
          END;
      END  ELSE Error (28);
     Report;  {Stampanje rezultata simulacije}
  UNTIL Stop;
  // CLOSE (Ob_f);
  // Brisanje obj datoteke posle zavrsenog ucitavanje, BJ 6/8/2003
  // DeleteFile('.obj');
  //mObj.Free;//*BojanNenadJovicic jul, avgust 2003.*
  // Ovo je nepotrebno posto sada nema vise snimanja rezultata u datoteku
  // IF Opn_flg THEN CLOSE (Re_f);

{ WRITELN;     }{Stampanje poruke o zavrsetku simulacije}
{ WRITELN;       }

// IF Err_cod = 0 THEN
//   BEGIN
{    WRITELN ('Simulation successfuly finished ');
    WRITE ('Simulation results are in file : ');
    WRITELN (Rez_nm);}
//    ShowMessage(mSimFinished);
//  END
//    ELSE ShowMessage(mSimInterrupted);
          {WRITELN ('Simulation interrupted');}
 {WRITELN;}
end;
procedure NapraviPC;
begin
 if not assigned (PC) then
     begin
      //PC:=TPageControl.create(Roditelj);
      //PC.Parent := Roditelj;
      //PC.Align := ALClient;
     end;
end;


END. {Gpss}

