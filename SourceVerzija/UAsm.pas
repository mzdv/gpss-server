unit UAsm;

interface   {JA DODAO}

const
         //*BojanNenadJovicic jul, avgust 2003.*
        //izvucene deklaracije u interface deo da bi bile dostupne
        //proceduri prepareSimulation
      linelen = 255; {Max duzina linije}
      kmax = 3; {Max duzina numerala}
      lmax = 999; {Max brojna vrednost labele}
      cmax = MaxLongInt; {Max brojna vrednost konstante}
      idlen = 12; {Max duzina identifikatora}
      maxpar = 25; {Max broj parametra transakcije}
      nop = 29; {Broj operatora}
      nsna = 37; {Broj sna kodova}

      maxstmt = 1500; {Max br. naredbi}
      maxblk = 1500; {Max br. blokova}
      maxcha = 50; {Max br. korisnickih redova}
      maxfac = 50; {Max br. uredjaja}
      maxfun = 30; {Max br. funkcija}
      maxlog = 250; {Max br. prekidaca}
      maxsav = 250; {Max br. mem. lokacija}
      maxsto = 50; {Max br. skladista}
      maxtab = 50; {Max.br. tabela}
      maxque = 50; {Max br. redova}
      maxvar = 50; {Max br. varijabli}
      maxpoint = 40; {Max. br. tacaka funkcije}
      maxvop = 50; {Max. br. operatora i operanada u VARIABLE }
  TYPE
    // SNA Codes, BJ 6/7/2003
   snaCodes = (
                        snaNull,                {  0 }
                        snaC1,                  {  1 }
                        snaCA,                  {  2 }
                        snaCC,                  {  3 }
                        snaCH,                  {  4 }
                        snaCM,                  {  5 }
                        snaCT,                  {  6 }
                        snaF,                   {  7 }
                        snaFC,                  {  8 }
                        snaFN,                  {  9 }
                        snaFR,                  { 10 }
                        snaFT,                  { 11 }
                        snaM1,                  { 12 }
                        snaMP,                  { 13 }
                        snaN,                   { 14 }
                        snaP,                   { 15 }
                        snaPR,                  { 16 }
                        snaQ,                   { 17 }
                        snaQA,                  { 18 }
                        snaQC,                  { 19 }
                        snaQM,                  { 20 }
                        snaQT,                  { 21 }
                        snaQX,                  { 22 }
                        snaQZ,                  { 23 }
                        snaR,                   { 24 }
                        snaRN,                  { 25 }
                        snaS,                   { 26 }
                        snaSA,                  { 27 }
                        snaSC,                  { 28 }
                        snaSM,                  { 29 }
                        snaSR,                  { 30 }
                        snaST,                  { 31 }
                        snaTB,                  { 32 }
                        snaTC,                  { 33 }
                        snaTD,                  { 34 }
                        snaV,                   { 35 }
                        snaW,                   { 36 }
                        snaX                    { 37 }
                        );

      stmttyp = (decl , block , control); {Tipovi naredbi}
      numtype = (numlab , con , blkno , parno);{Num. tipovi }
      identtype = (lab , blkname , snatype , opcode , oppfx);
      expadrtyp = (eadr , sadr); {Ocekivani tip adrese}
      adrtyp = (direct , indirect); {Tip adrese}
      optype = (numop , adrop , snaop , missop); {Tip operanda}
      voptype = (voperator,voperand); {Tip operanda u Variable}
      stringid = PACKED ARRAY [1..idlen] OF CHAR;
      String64 = STRING [64];
      string25 = STRING [25];
      string3  = PACKED ARRAY [1..3] OF CHAR;
{}
      entadr = RECORD {Adresa entiteta}
         CASE adrs : adrtyp OF
              direct : (nr : LONGINT;
                        name : stringid);
            indirect : (par : 1..maxpar)
      END;{entadr}
{}
      sna = RECORD {Standardni numericki atribut}
         snacod : snaCodes;  // Bilo je LongINT, sada snaCodes, BJ 6/7/2003
         snaadr : entadr
      END; {sna}
{}
      operand = RECORD
         CASE typop : optype OF
            numop : (nop : LONGINT);
            snaop : (sop : sna);
            adrop : (aop :entadr)
      END;{operand}
{***********************************}
      StatementPtr = ^Statement;
      Statement = RECORD  {Naredba}
           typ : stmttyp;
            nr : LONGINT;
           lab : stringid;
            oc : LONGINT;
            op : ARRAY [1..5] OF operand
       END;{statement}
{}
      ent = RECORD  {Entitet}
          nr : LONGINT;
        name : stringid
      END; {ent}
{}
      points = RECORD  {Tacke za definisanje funkcije}
           x : REAL;
           y : REAL
      END; {Points}
{}
      fun = RECORD  {Funkcija}
          nr : LONGINT;
        name : stringid;
       point : ARRAY [1..maxpoint] OF points
      END; {fun}
{}
      VoprPtr = ^Vopr;
      Vopr = RECORD {Operator i operand za Poljsku notaciju}
        CASE typ : voptype OF
       voperator : (oprtr : CHAR);
       voperand  : (oprnd : operand);
      END; {vopr}
{}
      variable = RECORD {Promenljiva}
          nr : LONGINT;
        name : stringid;
          op : ARRAY [1..maxvop] OF VoprPtr
      END;

  

VAR   linebuff :String [255];
      Bilo_gresaka : boolean;
      ch : CHAR;
      errorcod,idcod,nlab,lineno,next,j,k,l,m,n,
      blokno,stmcount,currlen,
      blkcount,chacount,faccount,funcount,logcount,savcount,
      stocount,tabcount,quecount,varcount : LONGINT;
      errflg,startflg,stop : BOOLEAN;
      srnm,lsnm,obnm,oblnm : string;
      progfile : TEXT;
      // Ovo su ranije bile datoteke, sada su StringListe, BJ 6/7/2003
      // listfile, oblfile, objfile,
      errfile : TEXT;
      adrbuf : entadr;
      opbuff : operand;
      id,slab : stringid;
{}
      {Tabele permanentnih entiteta}
{}
      OPKWT : ARRAY [1..nop] OF stringid;
      SNAKWT : ARRAY [1..nsna] OF string3;
      STMTBL : ARRAY [1..maxstmt] OF StatementPtr;
      BLKTBL : ARRAY [1..maxblk] OF stringid;
      CHATBL : ARRAY [1..maxcha] OF ent;
      FACTBL : ARRAY [1..maxfac] OF ent;
      FUNTBL : ARRAY [1..maxfun] OF fun;
      LOGTBL : ARRAY [1..maxlog] OF ent;
      SAVTBL : ARRAY [1..maxsav] OF ent;
      STOTBL : ARRAY [1..maxsto] OF ent;
      QUETBL : ARRAY [1..maxque] OF ent;
      TABTBL : ARRAY [1..maxtab] OF ent;
      VARTBL : ARRAY [1..maxvar] OF variable;


      Function GPSSASM_Start(n : string) : boolean;  {JA DODAO}

implementation        {JA DODAO}

uses SysUtils, uFile;

{I ERROR.TXT}

function StrExp (in_str : string; n : integer) : string;
var tmp_str : string;
    i : longint;
begin
    tmp_str := in_str;
    for i := length(tmp_str) to n do
        tmp_str := ' ' + tmp_str;
    result := tmp_str;
end;
function Int2SC(znak : integer) : snaCodes;
begin

                case znak of
                        0 : Result := snaNull;                {  0 }
                        1 : Result := snaC1;                  {  1 }
                        2 : Result := snaCA;                  {  2 }
                        3 : Result := snaCC;                  {  3 }
                        4 : Result := snaCH;                  {  4 }
                        5 : Result := snaCM;                  {  5 }
                        6 : Result := snaCT;                  {  6 }
                        7 : Result := snaF;                   {  7 }
                        8 : Result := snaFC;                  {  8 }
                        9 : Result := snaFN;                  {  9 }
                       10 : Result := snaFR;                  { 10 }
                       11 : Result := snaFT;                  { 11 }
                       12 : Result := snaM1;                  { 12 }
                       13 : Result := snaMP;                  { 13 }
                       14 : Result := snaN;                   { 14 }
                       15 : Result := snaP;                   { 15 }
                       16 : Result := snaPR;                  { 16 }
                       17 : Result := snaQ;                   { 17 }
                       18 : Result := snaQA;                  { 18 }
                       19 : Result := snaQC;                  { 19 }
                       20 : Result := snaQM;                  { 20 }
                       21 : Result := snaQT;                  { 21 }
                       22 : Result := snaQX;                  { 22 }
                       23 : Result := snaQZ;                  { 23 }
                       24 : Result := snaR;                   { 24 }
                       25 : Result := snaRN;                  { 25 }
                       26 : Result := snaS;                   { 26 }
                       27 : Result := snaSA;                  { 27 }
                       28 : Result := snaSC;                  { 28 }
                       29 : Result := snaSM;                  { 29 }
                       30 : Result := snaSR;                  { 30 }
                       31 : Result := snaST;                  { 31 }
                       32 : Result := snaTB;                  { 32 }
                       33 : Result := snaTC;                  { 33 }
                       34 : Result := snaTD;                  { 34 }
                       35 : Result := snaV;                   { 35 }
                       36 : Result := snaW;                   { 36 }
                       37 : Result := snaX;                   { 37 }
                       end;

end;


PROCEDURE error (cod: LONGINT);
var i: longint;//*BojanNenadJovicic jul, avgust 2003.*
BEGIN{error}
   IF NOT errflg THEN
      BEGIN
        Bilo_gresaka := true;
        errorcod := cod; {Signalizacija greske}
        errflg := TRUE;
        stop := TRUE;
        {Stampanje poruke o gresci}
        mLst.WRITE('***  ');
        i:= 0;
        FOR i:= 1 TO NEXT DO mLst.WRITE(linebuff[i]);
        mLst.WRITELN('');
        FOR i:= 1 TO next + 8 DO  mLst.WRITE (' ');
        mLst.WRITELN ('^ Error ' +  StrExp( inttostr(errorcod), 3))
      END;
END;{error}

{I NEXTCHAR.TXT}
PROCEDURE nextchar;
BEGIN {nextchar}
   IF NOT stop THEN
    BEGIN
      next := next + 1;
      IF next = currlen  THEN
      BEGIN
         error(8);
         ch := '*' {Dummy character}
      END
       ELSE ch := UPCASE( linebuff [next])
    END;
END; {nextchar}

{I GETLINE.TXT}
PROCEDURE getline;
VAR j: LONGINT; {Bilo je i aloi se nije koristilo ?????}
BEGIN {getline}
IF NOT stop THEN
  BEGIN
   next := 0; lineno := lineno + 1;
   linebuff := '';
   mLst.WRITE (strexp(inttostr(lineno),3)  + '|');
   IF NOT EOF (progfile) THEN
      BEGIN
         READLN (progfile,linebuff);
           WHILE Pos(Chr(9),linebuff) <> 0 DO
           BEGIN
            j:=Pos(Chr(9),linebuff);
            linebuff := copy (linebuff,1,j-1) + '     '+
                        copy (linebuff,j+1,length(linebuff)-j);
           END;
         IF LENGTH (linebuff) > linelen - 3 THEN error (2) ELSE
          BEGIN
            linebuff := CONCAT (linebuff,'   ');
            currlen := LENGTH (linebuff);
          END
      END
         ELSE error (1) {nema end naredbe}
  END;
END;{getline}

{I GETID.TXT}
PROCEDURE getid (VAR   code : LONGINT);
VAR   m,i,j,k : LONGINT;
      sna : string3;
PROCEDURE testsna;
BEGIN
 i:=1; j:= nsna;
 REPEAT
  k:= (i+j) DIV 2;
  IF SNAKWT[k] >= sna THEN j:=k-1;
  if SNAKWT[k] <= sna THEN i:=k+1
 UNTIL i > j;
 IF SNAKWT[k] = sna THEN code := - k ELSE code := 0;
END; {testsna}
{}PROCEDURE testop;BEGIN
 i:=1; j:=nop;
 REPEAT
  k := (i+j) DIV 2;
  IF OPKWT[k] >= id THEN j:= k-1;
  IF OPKWT[k] <= id then i:= k+1
 UNTIL i > j;
 IF OPKWT[k] = id THEN code := k ELSE code := 0;
END; {testop}
{}
BEGIN {getid}
IF NOT stop THEN
 BEGIN
  FOR i:= 1 TO 3 DO sna[i] := ' '; {brisanje bafera}
  FOR i:= 1 TO idlen DO id[i] := ' ';
  m := 1;
  id [m] := ch;
  IF NOT (ch IN ['A'..'Z']) THEN error (15);
  nextchar;
  IF NOT (ch IN ['A'..'Z']) THEN
     IF ch IN ['1'..'9','$','*'] THEN
       BEGIN
         sna [1] := id [1];
         IF (ch = '1') AND ((id [1] = 'C') OR (id [1] = 'M')) THEN
           BEGIN   {M1,C1}
            m := m + 1;
            id [m] := ch;
            sna [2] := ch;
            nextchar
           END;
         testsna;
       END
     ELSE error(13)
  ELSE
    BEGIN
      m:=2;
      id[m] := ch;
      nextchar;
      IF NOT (ch IN ['A'..'Z']) THEN
        IF ch IN ['1'..'9','$','*']  THEN
          BEGIN
            sna[1] := id[1] ; sna[2] := id[2];
            testsna
          END  ELSE error(13)
      ELSE
        BEGIN
           REPEAT
             m := m + 1;
             IF m > idlen THEN error(8) ELSE
                   id [m] := ch;
                   nextchar
           UNTIL NOT (ch IN ['A'..'Z','0'..'9']) OR stop;
        testop;
        END;
    END;
  END;
 END;{getid}

{I GETNUM.TXT}
PROCEDURE getnum ( typ:numtype; VAR n:LONGINT);
VAR k : LONGINT;
BEGIN  {getnum}
IF NOT stop THEN
 BEGIN
  k:=0;
  n:=0;
  IF NOT (ch IN ['0'..'9']) THEN error (20) ELSE
  BEGIN
  REPEAT
    n:=n*10 + ORD(ch) - ORD('0');
    k:=k+1;
    nextchar
  UNTIL NOT (ch IN ['0'..'9']);
  CASE typ OF
   numlab : BEGIN
              IF NOT (ch=' ') THEN error (9);
              IF k > kmax THEN error (5);
              IF n > lmax THEN error (4)
            END;
      con : BEGIN
              IF k > 9 THEN error (16);
            END;
    blkno : BEGIN
              IF n > maxblk THEN  error (9);
            END;
    parno : BEGIN
              IF n > maxpar THEN error (6);
            END;
  END {CASE}
  END
 END;
END;{getnum}

{I LIN1E.TXT}
PROCEDURE line ;
var i : longint;
{I STMT.TXT}
PROCEDURE stmt;
var i : longint;
{}
{I GETADR.TXT}
PROCEDURE getadr (expadr : expadrtyp; VAR adrbuf : entadr);
BEGIN {getadr}
IF NOT stop THEN
 BEGIN
 IF ch IN ['0'..'9'] THEN
  BEGIN
    getnum (con,n);
    adrbuf.adrs := direct;
    adrbuf.nr := n;
    adrbuf.name := '            ';
   END
 ELSE
 IF ch = '*' THEN
  BEGIN
   nextchar;
   getnum (parno,n);
   adrbuf.adrs := indirect;
   adrbuf.par := n
  END
  ELSE
  IF expadr = sadr THEN
   IF ch = '$' THEN
   BEGIN
    nextchar;
    getid (idcod);
    IF idcod <> 0 THEN error (21) ELSE
      BEGIN
       adrbuf.adrs := direct;
       adrbuf.nr := 0;
       adrbuf.name := id
      END;
   END
   ELSE error (21)
  ELSE
  IF NOT (ch IN ['A'..'Z']) THEN error (18) ELSE
  BEGIN
   getid (idcod);
   IF idcod <> 0 THEN error (21) ELSE
   BEGIN
     adrbuf.adrs := direct;
     adrbuf.nr := 0;
     adrbuf.name := id
   END
  END
 END;
END;{getadr}
{}
{I GETOP.TXT}
PROCEDURE getop (expop : optype ; VAR opbuf:operand);
BEGIN {getop}
IF NOT stop THEN
 BEGIN
  IF ch IN [' ',','] THEN opbuf.typop := missop
  ELSE
  BEGIN
  IF ch IN ['0'..'9'] THEN
   CASE expop  OF
   numop : BEGIN
            getnum (con,n);
            opbuf.typop := numop;
            opbuf.nop := n
           END;{numop}
   adrop : BEGIN
            getadr (eadr,adrbuf);
            opbuf.typop := adrop;
            opbuf.aop := adrbuf
           END;{adrop}
   END{CASE}
   ELSE
    BEGIN
    IF NOT(ch IN ['A'..'Z','*']) THEN error (18)
    ELSE
     IF ch = '*' THEN
       BEGIN
         IF expop <> adrop THEN error (23) ELSE
            BEGIN
              getadr (eadr,adrbuf);
              opbuf.typop := adrop;
              opbuf.aop := adrbuf
            END
       END
         ELSE
     BEGIN
     getid (idcod);
     IF NOT stop THEN
      BEGIN
       CASE expop OF
       numop : IF idcod >= 0 THEN error (22) ELSE
                BEGIN
                 opbuf.typop := snaop;
                 opbuf.sop.snacod := Int2SC(ABS(idcod)); // BJ 6/7/2003
                 IF (idcod = -1) OR (idcod = -12) OR
                   (idcod = -13) OR (idcod = -16) THEN
                  BEGIN {C1,M1,MP,PR}
                   adrbuf.adrs := direct;
                   adrbuf.nr := 0;
                   adrbuf.name := '            '
                  END
                 ELSE getadr (sadr,adrbuf);
                 opbuf.sop.snaadr := adrbuf
                END;
       adrop : IF idcod = 0 THEN
                BEGIN
                 opbuf.typop := adrop;
                 opbuf.aop.adrs := direct;
                 opbuf.aop.nr := 0;
                 opbuf.aop.name := id;
                END
                ELSE IF idcod < 0 THEN
                 BEGIN
                  opbuf.typop := snaop;
                  opbuf.sop.snacod := Int2SC(ABS(idcod)); // BJ 6/7/2003
                  IF (idcod = -1) OR (idcod = -12) OR
                   (idcod = -13) OR (idcod = -16)  THEN
                   BEGIN {C1,M1,MP,PR}
                    adrbuf.adrs := direct;
                    adrbuf.nr := 0;
                    adrbuf.name := '            '
                   END
                  ELSE getadr (sadr,adrbuf);
                  opbuff.sop.snaadr := adrbuf
                 END
                  ELSE error (21);
       END;{CASE}
      END;
     END
    END
  END
 END;
END;{getop}
{}
{I declstmt.TXT}
PROCEDURE declstmt;
{I INITIAL.TXT}
PROCEDURE initial;
VAR i,j : LONGINT;
BEGIN {initial}
 IF NOT stop THEN
  BEGIN
   IF (nlab <> 0) OR (slab <> '            ') THEN error (43);
   IF ch <> ' ' THEN error (9) ELSE
    BEGIN
     nextchar;
     IF ch <> 'X' THEN error (11) ELSE
      BEGIN
        nextchar;
        getadr (sadr,adrbuf);
        IF adrbuf.adrs =  indirect THEN error (21) ELSE
        IF ch <> ',' THEN error (10) ELSE
         BEGIN
          j := 1; {Ispitivanje dali adresa postoji}
          FOR i := 1 TO savcount DO
           IF ((adrbuf.name = SAVTBL [i].name ) AND
               (adrbuf.name <> '            ')) OR
              ((adrbuf.nr = SAVTBL [i].nr) AND (adrbuf.nr > 0))
             THEN j := 0;
          IF (j = 0) AND (NOT startflg) THEN error (42) ELSE
           IF j = 1 THEN
            BEGIN
             savcount := savcount + 1;
             IF (adrbuf.nr > maxsav) OR (savcount > maxsav)
                THEN error (44) ELSE
              BEGIN
               STMTBL [stmcount]^.op [1].typop := adrop;
               STMTBL [stmcount]^.op [1].aop := adrbuf;
               SAVTBL [savcount].nr := adrbuf.nr;
               SAVTBL [savcount].name := adrbuf.name;
               nextchar;
               IF ch = '-' THEN
                 BEGIN
                  j:= -1;
                  nextchar
                 END  ELSE j := 1;
               getnum (con,n);
               IF ch <> ' ' THEN error (9) ELSE
                 BEGIN
                  STMTBL [stmcount]^.op [2].typop := numop;
                  STMTBL [stmcount]^.op [2].nop := n * j;
                 END
              END
            END
         END
      END
    END
  END;
END; {initial}
{}
{I STORAGE.TXT}
PROCEDURE storage;
VAR i,j : LONGINT;
BEGIN {storage}
 IF NOT stop THEN
  BEGIN
   IF (nlab = 0) AND (slab = '            ') THEN error (19);
   j := 1; {Ispitivanje dali naziv postoji}
   FOR i := 1 TO stocount DO
   IF ((slab = STOTBL [i].name) AND (slab <> '            '))  OR
      ((nlab = STOTBL [i].nr) AND (nlab > 0))
      THEN j := 0;
   IF (j = 0) AND NOT startflg THEN error (42) ELSE
   IF j = 1 THEN
    BEGIN {Azuriranje tabele skladista}
     stocount := stocount + 1;
     IF (stocount > maxsto) OR (nlab > maxsto) THEN error (41) ELSE
      BEGIN
       STOTBL [stocount].nr := nlab;
       STOTBL [stocount].name := slab;
      END
    END;
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      getnum (con,n);
      STMTBL [stmcount]^.op [1].typop := numop;
      STMTBL [stmcount]^.op [1].nop := n;
     END
  END;
END; {storage}
{}
{I FUNCTION.TXT}
PROCEDURE funct;

VAR i,j,pointcount : LONGINT;
    x,y : REAL;

PROCEDURE getfloat (VAR a : REAL);
VAR c,d,k : REAL;
        j : LONGINT;
BEGIN {getfloat}
 IF NOT stop THEN
  BEGIN
   c := 0;
   d := 0;
   k := 10;
   j := 1;
   IF ch = '-' THEN
      BEGIN
       j := -1;
       nextchar
      END;
   IF NOT (ch IN ['.','0'..'9']) THEN error (39) ELSE
   WHILE ch IN ['0'..'9'] DO  {Celobrojni deo}
    BEGIN
     c := c * 10 + ORD (ch) - ORD ('0');
     nextchar
    END;
   IF ch = '.' THEN {Decimalni deo}
    BEGIN
     nextchar;
     WHILE ch IN ['0'..'9'] DO
      BEGIN
       d := d + (ORD (ch) - ORD ('0')) / k;
       nextchar;
       k := k * 10
      END
    END;
   a := (c + d ) * j
  END;
END; {getfloat}

BEGIN {function}
 IF NOT stop THEN
  BEGIN
   IF (nlab = 0) AND (slab = '            ')  THEN error (19);
   j := 1; {Ispitivanje dali naziv postoji}
   FOR i := 1 TO funcount DO
   IF ((slab = FUNTBL [i].name) AND (slab <> '            '))  OR
      ((nlab = FUNTBL [i].nr) AND (nlab > 0))
      THEN j := 0;
   IF (j = 0) AND NOT startflg THEN error (51) ELSE
   IF j = 1 THEN
    BEGIN {Azuriranje tabele funkcija}
     funcount := funcount + 1;
     IF (funcount > maxfun) OR (nlab > maxfun) THEN error (50) ELSE
      BEGIN
       FUNTBL [funcount].nr := nlab;
       FUNTBL [funcount].name := slab;
      END
    END;
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      getop (numop,opbuff);
      IF opbuff.typop <> snaop THEN error (23) ELSE
      IF opbuff.sop.snaadr.adrs <> direct THEN error (55) ELSE
      IF ch <> ',' THEN error (10) ELSE
       BEGIN
        STMTBL [stmcount]^.op [1] := opbuff;
        nextchar;
        IF NOT (ch IN ['C','D']) THEN error (52) ELSE
         BEGIN
          IF ch = 'C' THEN j := 1 ELSE j := -1;
          nextchar;
          getnum (con,n);
          IF n > maxpoint THEN error (54) ELSE
          IF ch <> ' ' THEN error (9) ELSE
           BEGIN
            STMTBL [stmcount]^.op [2].typop := numop;
            STMTBL [stmcount]^.op [2].nop := n * j;

         { Citanje vrednosti tacaka funkcije iz sledece linije }

            pointcount := 0;
            WHILE (pointcount < n) AND NOT stop DO
             BEGIN
               mLst.WRITE ('     ');
               FOR i := 1 TO currlen DO mLst.WRITE (linebuff [i]);
               mLst.WRITELN ('');
               getline; {Preskakanje linije}
               REPEAT nextchar UNTIL ch <> ' ';
               REPEAT
                 getfloat (x);
                 IF ch <> ',' THEN error (10) ELSE
                   BEGIN
                     nextchar;
                     getfloat (y);
                     pointcount := pointcount + 1;
                     IF pointcount > maxpoint THEN error (54) ELSE
                      BEGIN
                        FUNTBL [funcount].point [pointcount].x := x;
                        FUNTBL [funcount].point [pointcount].y := y;
                        IF NOT(ch IN ['/',' ']) THEN error (53) ELSE
                         IF (ch = '/') AND (pointcount  < n) THEN  nextchar;
                      END
                   END
               UNTIL stop OR (ch = ' ') OR (pointcount = n);
             END
           END
         END
       END
     END
  END;
END; {function}
{}
{I TABLE.TXT}
PROCEDURE table;
VAR i,j : LONGINT;
BEGIN {table}
 IF NOT stop THEN
  BEGIN
   IF (nlab = 0) AND (slab = '            ') THEN error (19);
   j := 1; {Ispitivanje dali naziv postoji}
   FOR i := 1 TO tabcount DO
   IF ((slab = TABTBL [i].name) AND (slab <> '            '))  OR
      ((nlab = TABTBL [i].nr) AND (nlab > 0))
      THEN j := 0;{Naziv postoji}
   IF (j = 0) AND NOT startflg THEN error (48) ELSE
   IF j = 1 THEN
    BEGIN {Azuriranje tabele }
     tabcount := tabcount + 1;
     IF (tabcount > maxtab) OR (nlab > maxtab) THEN error (47) ELSE
      BEGIN
       TABTBL [tabcount].nr := nlab;
       TABTBL [tabcount].name := slab;
      END
    END;
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      getop (numop,opbuff);
      IF ch <> ',' THEN error (10) ELSE
      IF opbuff.typop <> snaop THEN error (22) ELSE
      IF opbuff.sop.snaadr.adrs <> direct THEN error (49) ELSE
       BEGIN
        STMTBL [stmcount]^.op [1] := opbuff;
        nextchar;
        getnum (con,n);
        IF ch <> ',' THEN error (10) ELSE
         BEGIN
          STMTBL [stmcount]^.op [2].typop := numop;
          STMTBL [stmcount]^.op [2].nop := n;
          nextchar;
          getnum (con,n);
          IF ch <> ',' THEN error (10) ELSE
           BEGIN
            STMTBL [stmcount]^.op [3].typop := numop;
            STMTBL [stmcount]^.op [3].nop := n;
            nextchar;
            getnum (con,n);
            IF ch <> ' ' THEN error (9) ELSE
             BEGIN
              STMTBL [stmcount]^.op [4].typop := numop;
              STMTBL [stmcount]^.op [4].nop := n;
             END
           END
         END
       END
     END
  END;
END; {table}
{}
{I VARIABLE.TXT}
PROCEDURE variabl;
VAR i,j,opcount : LONGINT;
{
}
PROCEDURE expression;
VAR opr: CHAR;

  PROCEDURE term;
    VAR opr_term : CHAR;

  PROCEDURE factor;
{
}
  BEGIN {factor}
     IF NOT stop THEN
     BEGIN
       IF ch  = '(' THEN   {Expression}
          BEGIN
            nextchar;
            expression;
            IF ch <> ')' THEN error (58) ELSE nextchar;
          END
        ELSE  {Identifier}
          BEGIN
             getop (numop,opbuff);
             opcount := opcount + 1;
             new (VARTBL[varcount].op [opcount]);
             IF opcount > maxvop THEN error (59) ELSE
              BEGIN
               VARTBL [varcount].op [opcount]^.typ := voperand;
               VARTBL [varcount].op [opcount]^.oprnd := opbuff;
              END
          END
   END;
  END; {factor}
{
}
  BEGIN {term}
   IF NOT stop THEN
   BEGIN
    factor;
    WHILE (ch IN ['*','/','@','^']) AND NOT stop DO
      BEGIN
       opr_term:= ch;
       nextchar;
       factor;
       opcount := opcount + 1;
       new (VARTBL[varcount].op [opcount]);
       IF opcount > maxvop THEN error (59) ELSE
        BEGIN
         VARTBL [varcount].op [opcount]^.typ := voperator;
         VARTBL [varcount].op [opcount]^.oprtr := opr_term
        END
      END
   END;
  END; {term}
{
}
BEGIN {expression}
 IF NOT stop THEN
 BEGIN
  term;
  WHILE ((ch = '+') OR (ch = '-')) AND NOT stop DO
     BEGIN
      opr:= ch;
      nextchar;
      term;
      opcount := opcount + 1;
      new (VARTBL [varcount].op[opcount]);
      IF opcount > maxvop THEN error (59) ELSE
       BEGIN
         VARTBL [varcount].op [opcount]^.typ := voperator;
         VARTBL [varcount].op [opcount]^.oprtr := opr
       END
     END
 END;
END; {expression}
{
}
BEGIN {variable}
 IF NOT stop THEN
  BEGIN
   opcount := 0;
   IF (nlab = 0) AND (slab = '            ') THEN error (19);
   j := 1; {Ispitivanje dali naziv postoji}
   FOR i := 1 TO varcount DO
   IF ((slab = VARTBL [i].name) AND (slab <> '            '))  OR
      ((nlab = VARTBL [i].nr) AND (nlab > 0))
      THEN j := 0;{Naziv postoji}
   IF (j = 0) AND NOT startflg THEN error (56) ELSE
   IF j = 1 THEN
    BEGIN {Azuriranje tabele }
     varcount := varcount + 1;
     IF (varcount > maxvar) OR (nlab > maxvar) THEN error (57) ELSE
      BEGIN
       VARTBL [varcount].nr := nlab;
       VARTBL [varcount].name := slab;
      END
    END;
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      expression;   {Dobijanje Poljske notacije}
      IF ch <> ' ' THEN error (9) ELSE
       BEGIN   {Ubacivanje podatka o br. operanada i operatora}
        STMTBL [stmcount]^.op [1].typop := numop;
        STMTBL [stmcount]^.op [1].nop := opcount
       END
     END
  END;
END; {variable}
{}
BEGIN {declstmt}
 IF NOT stop THEN
 BEGIN
 STMTBL [stmcount]^.typ := decl;
 STMTBL [stmcount]^.nr := nlab;
 CASE idcod OF
    7 : funct;
   10 : initial;
   22 : storage;
   23 : table;
   29 : variabl;
 END; {CASE}
 END;
END; {declstmt}

{I blokstmt.TXT}
PROCEDURE blokstmt;
VAR i,j : LONGINT;
{
}
{I ADVANCE.TXT}
PROCEDURE advance;
VAR i : LONGINT;
BEGIN {advance}
 IF NOT stop THEN
  BEGIN
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      FOR i := 1 TO 2 DO
       BEGIN
        getop (numop,opbuff);
        IF NOT stop THEN
         IF (opbuff.typop = missop) AND (i = 1) THEN error (27) ELSE
           BEGIN
             IF opbuff.typop = missop  THEN
              BEGIN
                opbuff.typop := numop;
                opbuff.nop := 0
              END;
             STMTBL [stmcount]^.op [i] := opbuff;
             IF (i = 2) AND (ch = ',') THEN error (9) ELSE
               IF ch = ',' THEN
                  BEGIN
                    nextchar;
                    IF ch = ' ' THEN error (9);
                  END;
           END
       END {FOR}
     END
  END;
END; {advance}
{}
{I GATE.TXT}
PROCEDURE gate;
{
}
VAR i : LONGINT; pfx : stringid;
{
}
BEGIN {gate}
 IF NOT stop THEN
  BEGIN
   i := 0;
   pfx := '            ';
   IF ch <> ' ' THEN error (9) ELSE
    BEGIN
     REPEAT
      nextchar;
      i := i + 1;
      IF i > 12 THEN error (3) ELSE pfx [i] := ch
     UNTIL NOT (ch IN ['A'..'Z']);
     IF ch <> ' ' THEN error (9) ELSE
      BEGIN
       STMTBL [stmcount]^.op [1].typop := numop;
       IF pfx =  'LR          ' THEN
        STMTBL [stmcount]^.op [1].nop := 1 ELSE
       IF pfx = 'LS          ' THEN
        STMTBL [stmcount]^.op [1].nop := 2 ELSE
       IF pfx = 'SE          ' THEN
        STMTBL [stmcount]^.op [1].nop := 3 ELSE
       IF pfx = 'SF          ' THEN
        STMTBL [stmcount]^.op [1].nop := 4 ELSE
       IF pfx = 'SNE         ' THEN
        STMTBL [stmcount]^.op [1].nop := 5 ELSE
       IF pfx = 'SNF         ' THEN
        STMTBL [stmcount]^.op [1].nop := 6 ELSE
       IF pfx = 'NU          '  THEN
        STMTBL [stmcount]^.op [1].nop := 7 ELSE
       IF pfx = 'U           ' THEN
        STMTBL [stmcount]^.op [1].nop := 8 ELSE error (36);
       nextchar;
       getop (adrop,STMTBL [stmcount]^.op [2]);
       IF ch = ' ' THEN
        BEGIN {Missing operand}
         STMTBL [stmcount]^.op [3].typop := adrop;
         STMTBL [stmcount]^.op [3].aop.adrs := direct;
         STMTBL [stmcount]^.op [3].aop.nr := 0;
         STMTBL [stmcount]^.op [3].aop.name := '            '
        END
       ELSE
        IF ch <> ',' THEN error (10) ELSE
         BEGIN
          nextchar;
          getop (adrop,opbuff);
          STMTBL [stmcount]^.op [3] := opbuff;
          IF opbuff.typop = adrop THEN
            IF opbuff.aop.adrs = direct THEN
             IF opbuff.aop.nr > maxblk THEN error (7);
          IF ch <> ' ' THEN error (9);
         END
      END
    END
  END;
END; {gate}
{}
{I LINK.TXT}
PROCEDURE link;
{
}
VAR buf : stringid;
    i,j,k : LONGINT;
{
}
BEGIN {link}
 IF NOT stop THEN
  BEGIN
   buf := '            ';
   IF ch <> ' ' THEN error (9) ELSE
    BEGIN
      nextchar;
      getop (adrop,opbuff);
      STMTBL [stmcount]^.op [1] := opbuff;
      IF opbuff.typop = adrop THEN
        IF opbuff.aop.adrs = direct THEN
         IF opbuff.aop.nr > maxcha THEN error (40) ELSE
        BEGIN
          j := 0;
          FOR i := 1 TO chacount  DO
           IF ((opbuff.aop.nr = CHATBL [i].nr) AND
               (opbuff.aop.nr  > 0)) OR
              ((opbuff.aop.name = CHATBL [i].name) AND
               (opbuff.aop.name <> '            ')) THEN j := i;
          IF j = 0 THEN
           BEGIN   {Azuriranje tabele korisnickih redova}
             chacount := chacount + 1;
             IF chacount > maxcha THEN error (40) ELSE
                 BEGIN
                   CHATBL [chacount].nr := opbuff.aop.nr;
                   CHATBL [chacount].name := opbuff.aop.name
                 END
           END;
        END;
      IF ch <> ',' THEN error (10) ELSE
       BEGIN
        nextchar;
        STMTBL [stmcount]^.op [2].typop := numop;
        IF  ch IN ['L','F']   THEN
                  BEGIN
                    buf [1] := ch;
                    nextchar; buf [2] := ch ;
                    nextchar; buf [3] := ch ;
                    nextchar; buf [4] := ch ;
                    nextchar; buf [5] := ch ;
                    IF buf = 'FIFO        ' THEN
                       STMTBL [stmcount]^.op [2].nop :=   51 ELSE
                    IF buf <> 'LIFO        ' THEN error (37)
                     ELSE  STMTBL [stmcount]^.op [2].nop :=  52;
                  END
               ELSE
                 IF ch IN ['0'..'9'] THEN
                   BEGIN
                    getnum (parno,k);
                    STMTBL [stmcount]^.op [2].nop := k
                   END
                    ELSE
                     IF ch IN ['P','p'] THEN
                      BEGIN
                       nextchar;
                       getnum (parno,k);
                       STMTBL [stmcount]^.op [2].nop := k
                     END
                 ELSE   error (37);
       END
    END
  END;
END; {link}
{}
{I LOGIC.TXT}
PROCEDURE logic;
{
}
VAR i,j : LONGINT; pfx : stringid;
{
}
BEGIN {logic}
 IF NOT stop THEN
  BEGIN
   i := 0;
   pfx := '            ';
   IF ch <> ' ' THEN error (9) ELSE
    BEGIN
     REPEAT
      nextchar;
      i := i + 1;
      IF i > 12 THEN error (3) ELSE pfx [i] := ch
     UNTIL NOT (ch IN ['A'..'Z']);
     IF ch <> ' ' THEN error (9) ELSE
      BEGIN
       STMTBL [stmcount]^.op [1].typop := numop;
       IF pfx = 'R           ' THEN
        STMTBL [stmcount]^.op [1].nop := 1 ELSE
       IF pfx = 'S           ' THEN
        STMTBL [stmcount]^.op [1].nop := 2 ELSE
       IF pfx = 'I           ' THEN
        STMTBL [stmcount]^.op [1].nop := 3 ELSE error (36);
       nextchar;
       getop (adrop,opbuff);
       STMTBL [stmcount]^.op [2] := opbuff;
       IF ch <> ' ' THEN error (9) ELSE
        IF opbuff.typop = missop THEN error (21) ELSE
        BEGIN
         IF opbuff.typop = adrop THEN
          IF opbuff.aop.adrs = direct THEN
            IF  opbuff.aop.nr > maxlog THEN  error (46) ELSE
            BEGIN
                j := 0;
                FOR i := 1 TO logcount DO
                 IF ((opbuff.aop.name = LOGTBL [i].name)  AND
                    (opbuff.aop.name <> '            ')) OR
                    ((opbuff.aop.nr = LOGTBL [i].nr) AND
                     (opbuff.aop.nr > 0))
                    THEN j := i;
                IF j = 0 THEN
                 BEGIN
                  logcount := logcount + 1;
                  IF logcount > maxlog THEN error (46) ELSE
                   BEGIN
                    LOGTBL [logcount].nr := opbuff.aop.nr;
                    LOGTBL [logcount].name := opbuff.aop.name;
                   END
                 END;
            END;
        END
      END
    END
  END;
END; {logic}
{}
{I SAVEASS.TXT}
PROCEDURE saveass;
{
}
BEGIN {saveass}
 IF NOT stop THEN
  BEGIN
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      getop (adrop,opbuff);
      STMTBL [stmcount]^.op [1] := opbuff;
      IF NOT stop THEN
         BEGIN
         IF (STMTBL [stmcount]^.oc = 2) AND (opbuff.typop = adrop)
          THEN IF   ((opbuff.aop.adrs <> direct) OR
               ((opbuff.aop.adrs = direct) AND (opbuff.aop.nr = 0 )))
                THEN error (30);
          STMTBL [stmcount]^.op [2].typop := numop;
          CASE ch OF
              ',' : BEGIN  { 0 - Postavlja se na vrednost OP-3}
                      STMTBL [stmcount]^.op [2].nop := 0
                    END;{','}
              '+' : BEGIN  { 1 - Dodaje se vrednost OP-3 }
                      STMTBL [stmcount]^.op [2].nop := 1;
                      nextchar;
                      IF ch <> ',' THEN error (10);
                    END; {'+'}
               '-': BEGIN  {-1 - Oduzima se vrednost OP -3 }
                      STMTBL [stmcount]^.op [2].nop := -1;
                      nextchar;
                      IF ch <> ',' THEN error (10);
                    END; {'-'}
               ELSE error (23);
          END;{CASE}
          nextchar;
          getop (numop,opbuff);
          IF opbuff.typop = missop THEN error (27) ELSE
          IF ch <> ' ' THEN error (9) ELSE
             STMTBL [stmcount]^.op [3] := opbuff;
         END;
     END
  END;
END; {saveass}
{}
{I MTELSRQD.TXT}
PROCEDURE mtelsrqd;
VAR i,j : LONGINT;
{
}
BEGIN {mtelsrqd}
 IF NOT stop THEN
  BEGIN
   IF ch <> ' ' THEN error (9) ELSE
     IF idcod <> 14 {MARK} THEN
     BEGIN
      nextchar;
      IF (ch = ' ') OR (ch = CHR(13)) THEN error (27) ELSE
       BEGIN
        getop (adrop,opbuff);
        STMTBL [stmcount]^.op [1] := opbuff;
        IF ch <> ' ' THEN error (9) ELSE
        IF opbuff.typop = Adrop THEN
         IF opbuff.aop.adrs = direct THEN
         CASE STMTBL [stmcount]^.oc OF
                {DEPART}
            4 : IF opbuff.aop.nr > maxque THEN error (29);
                {ENTER,LEAVE}
         6,11 : IF opbuff.aop.nr > maxsto THEN error (41);
           15 : BEGIN {QUEUE}
                 IF opbuff.aop.nr > maxque THEN error (29) ELSE
                 BEGIN
                  j := 0;
                  FOR i := 1 TO quecount DO
                     {Ispitivanje jedinstvenosti imena}
                     IF ((opbuff.aop.nr = QUETBL [i].nr)
                     AND (opbuff.aop.nr > 0))  OR
                     ((opbuff.aop.name = QUETBL [i].name)
                     AND (opbuff.aop.name <>'            '))
                      THEN j := i;
                  IF j = 0 THEN
                   BEGIN
                     quecount := quecount + 1 ;
                     IF quecount > maxque THEN error (29) ELSE
                       BEGIN
                         QUETBL [quecount].nr := opbuff.aop.nr;
                         QUETBL [quecount].name := opbuff.aop.name
                       END
                   END;
                 END
                END;{QUEUE}
                {RELEASE}
           16 : IF opbuff.aop.nr > maxfac THEN error (31);
{
}
           19 : BEGIN {FAC}
                 IF opbuff.aop.nr > maxfac THEN error (31) ELSE
                 BEGIN
                  j := 0;
                  FOR i := 1 TO faccount  DO
                    {Ispitivanje jedinstvenosti imena}
                     IF ((opbuff.aop.nr = FACTBL [i].nr)
                        AND (opbuff.aop.nr > 0)) OR
                        ((opbuff.aop.name = FACTBL [i].name)
                        AND (opbuff.aop.name <> '            '))
                        THEN  j := i;
                  IF j = 0 THEN
                    BEGIN
                     faccount := faccount + 1;
                     IF faccount > maxfac THEN error (31) ELSE
                      BEGIN
                       FACTBL [faccount].nr := opbuff.aop.nr;
                       FACTBL [faccount].name := opbuff.aop.name
                      END
                    END;
                 END
                END;{FAC}
                {TABLE}
           24 : IF opbuff.aop.nr > maxtab THEN error (47);
{
}
         END;{CASE}
       END
     END;
  END;
END;{mtelsrqd}
{}
{I GENERATE.TXT}
PROCEDURE generate;
{
}
VAR i : LONGINT;
{
}
BEGIN {generate}
 IF NOT stop THEN
  BEGIN
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      FOR i := 1 TO 5 DO
       BEGIN
        getop (numop,opbuff);
        IF NOT stop THEN
        IF (i = 1) AND (opbuff.typop = missop) THEN error (27) ELSE
         BEGIN
          IF opbuff.typop = missop THEN
           BEGIN                  {Nedostajuci operand}
            opbuff.typop := numop;
            opbuff.nop := 0;
            IF i = 4 THEN opbuff.nop := MaxLongInt;{ Max. br. trans.}
            STMTBL [stmcount]^.op [i] := opbuff
           END
           ELSE
            BEGIN
             IF (i = 5) AND (opbuff.typop = numop) THEN
                IF (opbuff.nop > 127)  OR (opbuff.nop < 0)
                THEN error (32);
             IF (opbuff.typop = snaop) AND
               (opbuff.sop.snaadr.adrs= indirect) THEN error (28)
               ELSE   STMTBL [stmcount]^.op [i] := opbuff
{
}
            END;
            IF (i = 5) AND (ch = ',') THEN error (9) ELSE
               IF ch = ',' THEN
                  BEGIN
                    nextchar;
                    IF (i < 5) AND (ch = ' ') THEN error (9);
                  END;
         END
       END {FOR}
     END
  END;
END; {Generate}
{}
{I TERMINAT.TXT}
PROCEDURE terminate;
{
}
BEGIN {terminate}
 IF NOT stop THEN
  BEGIN
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      IF (ch = ' ') OR (ch = CHR(13)) THEN {Missing operand}
       BEGIN
        STMTBL [stmcount]^.op [1].typop := numop;
        STMTBL [stmcount]^.op [1].nop := 0
       END
       ELSE {Terminacioni brojac}
        BEGIN
          getop (numop,STMTBL [stmcount]^.op [1]);
          IF ch <> ' ' THEN error (9);
        END
     END
  END;
END;{Terminate}
{}
{I TEST.TXT}
PROCEDURE test;
{
}
VAR i : LONGINT; pfx : stringid;
{
}
BEGIN {test}
 IF NOT stop THEN
  BEGIN
   i := 0;
   pfx := '            ';
   IF ch <> ' ' THEN error (9) ELSE
    BEGIN
     REPEAT
      nextchar;
      i := i + 1;
      IF i > 12 THEN error (3) ELSE pfx [i] := ch
     UNTIL NOT (ch IN ['A'..'Z']);
     IF ch <> ' ' THEN error (9) ELSE
      BEGIN
       STMTBL [stmcount]^.op [1].typop := numop;
       IF pfx =  'G           ' THEN
        STMTBL [stmcount]^.op [1].nop := 1 ELSE
       IF pfx = 'GE          ' THEN
        STMTBL [stmcount]^.op [1].nop := 2 ELSE
       IF pfx = 'E           ' THEN
        STMTBL [stmcount]^.op [1].nop := 3 ELSE
       IF pfx = 'NE          ' THEN
        STMTBL [stmcount]^.op [1].nop := 4 ELSE
       IF pfx = 'L           ' THEN
        STMTBL [stmcount]^.op [1].nop := 5 ELSE
       IF pfx = 'LE          ' THEN
        STMTBL [stmcount]^.op [1].nop := 6 ELSE error (36);
       nextchar;
       getop (numop,opbuff);
       IF ch <> ',' THEN error (10) ELSE
       IF opbuff.typop = missop THEN error (27) ELSE
         BEGIN
           STMTBL [stmcount]^.op [2] := opbuff;
           nextchar;
           getop (numop,opbuff);
           IF opbuff.typop = missop THEN error (27) ELSE
           BEGIN
           STMTBL [stmcount]^.op [3] := opbuff;
           IF ch = ' ' THEN
            BEGIN {Missing operand}
             STMTBL [stmcount]^.op [4].typop := adrop;
             STMTBL [stmcount]^.op [4].aop.adrs := direct;
             STMTBL [stmcount]^.op [4].aop.nr := 0;
             STMTBL [stmcount]^.op [4].aop.name := '            '
            END
            ELSE
             IF ch <> ',' THEN error (10) ELSE
             BEGIN
              nextchar;
              getop (adrop,opbuff);
              STMTBL [stmcount]^.op [4] := opbuff;
              IF opbuff.typop = adrop THEN
               IF opbuff.aop.adrs = direct THEN
                IF opbuff.aop.nr > maxblk THEN error (7);
              IF ch <> ' ' THEN error (9);
             END
           END
         END
      END
    END
  END;
END; {test}
{}
{I TRANSFER.TXT}
PROCEDURE transfer;
{
}
VAR i,k,n,flg : LONGINT;
         buf  : stringid;
{
}
BEGIN {transfer}
 IF NOT stop THEN
  BEGIN
   IF ch <> ' ' THEN error (9) ELSE
     BEGIN
      nextchar;
      CASE ch OF
{}
       ',' : BEGIN   {Bezuslovni TRANSFER}
              flg := 1;
              STMTBL [stmcount]^.op [1].typop := numop;
              STMTBL [stmcount]^.op [1].nop := 0;
              nextchar;
              getop (adrop,opbuff);
              IF NOT stop THEN
                 IF ch <> ' ' THEN error (9) ELSE
                   BEGIN
                    STMTBL [stmcount]^.op [2] := opbuff;
                    STMTBL [stmcount]^.op [3].typop := adrop;
                    STMTBL [stmcount]^.op [3].aop.adrs := direct;
                    STMTBL [stmcount]^.op [3].aop.nr := 0;
                    STMTBL [stmcount]^.op [3].aop.name := '            '
                   END;
             END; {','}
{
}
       '.' : BEGIN   {Statisticki TRANSFER}
              flg := 2;
              k := 1;
              n := 0;
              nextchar;
              REPEAT
               IF NOT (ch IN ['0'..'9']) THEN error (20) ELSE
                  BEGIN
                    n := n + (ORD (ch) - ORD ('0')) * 100 DIV k;
                    k := k * 10;
                    nextchar
                  END
              UNTIL NOT (ch IN ['0'..'9']);
              IF ch <> ',' THEN error (10) ELSE
                 BEGIN
                   STMTBL [stmcount]^.op [1].typop := numop;
                   STMTBL [stmcount]^.op [1].nop := n
                 END
             END; {'.'}
{
}
       'B' : BEGIN  {TRANSFER BOTH}
              flg := 3;
              buf := '            ';
              buf [1] := ch;
              nextchar; buf [2] := ch;
              nextchar; buf [3] := ch;
              nextchar; buf [4] := ch;
              nextchar; buf [5] := ch;
              IF buf <> 'BOTH,       '  THEN error (23) ELSE
                 BEGIN
                   STMTBL [stmcount]^.op [1].typop := numop;
                   STMTBL [stmcount]^.op [1].nop := 1000
                 END
              END; {'B'}
         ELSE  error (23);
      END; {CASE}
{
}
    IF NOT stop THEN IF flg > 1 THEN
      BEGIN
        nextchar;
        FOR i := 2 TO 3 DO
         BEGIN
          getop (adrop,opbuff);
          IF NOT stop THEN
           BEGIN
            IF opbuff.typop = missop THEN
              BEGIN
               opbuff.typop := adrop;
               opbuff.aop.adrs := direct;
               opbuff.aop.nr := blkcount + 1;
               opbuff.aop.name := '            ';
              END;
            STMTBL [stmcount]^.op [i] := opbuff;
            IF (i = 3) AND (ch <> ' ') THEN error (9) ELSE
             IF ch = ',' THEN
                BEGIN
                 nextchar;
                 IF ch = ' ' THEN error (14);
                END;
           END;
         END {FOR}
      END;
     END;
  END;
END;{transfer}
{}
{I UNLINK.TXT}
PROCEDURE unlink;
{
}
VAR buf : stringid;
{
}
BEGIN {unlink}
 IF NOT stop THEN
  BEGIN
   buf := '            ';
   IF ch <> ' ' THEN error (9) ELSE
    BEGIN
      nextchar;
      IF ch IN [' ',','] THEN error (27) ELSE
       BEGIN
        getop (adrop,opbuff);
        STMTBL [stmcount]^.op [1] := opbuff;
        IF opbuff.typop = adrop THEN
          IF opbuff.aop.adrs = direct THEN
           IF opbuff.aop.nr > maxcha  THEN error (40);
       END;
      IF ch <> ',' THEN error (10) ELSE
       BEGIN
        nextchar;
        IF ch IN [' ',','] THEN error (27) ELSE
         BEGIN
          getop (adrop,opbuff) ;
          STMTBL [stmcount]^.op [2] := opbuff;
          IF opbuff.typop = adrop THEN
            IF opbuff.aop.adrs = direct THEN
             IF  opbuff.aop.nr > maxblk THEN error (7);
         END;
        IF ch <> ',' THEN error (10) ELSE
         BEGIN
          nextchar;
          STMTBL [stmcount]^.op [3].typop := numop;
          IF ch IN ['0'..'9'] THEN
           BEGIN
            getnum (con,n);
            STMTBL [stmcount]^.op [3].nop := n
           END
          ELSE
           BEGIN
            buf [1] := ch;
            nextchar; buf [2] := ch;
            nextchar; buf [3] := ch;
            nextchar;
            IF buf <> 'ALL         ' THEN error (38)
             ELSE STMTBL [stmcount]^.op [3].nop := MaxLongInt
           END;
          IF NOT (ch IN [' ',',']) THEN error (12) ELSE
           BEGIN
            IF ch = ' ' THEN
             BEGIN
              STMTBL [stmcount]^.op [4].typop := adrop;
              STMTBL [stmcount]^.op [4].aop.adrs := direct;
              STMTBL [stmcount]^.op [4].aop.nr:= blkcount + 1;
              STMTBL [stmcount]^.op [4].aop.name :='            '
             END
            ELSE
             BEGIN
              nextchar;
              getop (adrop,opbuff);
              STMTBL [stmcount]^.op [4] := opbuff;
              IF opbuff.typop = adrop THEN
                IF opbuff.aop.adrs = direct THEN
                 IF  opbuff.aop.nr > maxblk THEN error (7);
              IF ch <> ' ' THEN error (9)
             END
           END
         END
       END
    END
  END;
END; {unlink}
{}
BEGIN {blokstmt}
 IF NOT stop  THEN
  BEGIN
     j := 1;
             {Dali u tabeli postoji ime bloka}
     FOR i := 1 TO blkcount DO
      IF (slab = BLKTBL [i]) AND (slab <> '            ')
           THEN BEGIN j := 0; blokno := i END;
{
}
     IF NOT startflg AND (j = 0) THEN error (34) ELSE
     IF j = 1 THEN
      BEGIN
        blkcount := blkcount + 1;
        IF blkcount > maxblk THEN error (7) ELSE
        IF nlab > 0 THEN error (33) ELSE
         BEGIN   {Ubacivanje bloka u tabelu blokova}
          blokno := blkcount;
          BLKTBL [blkcount] := slab;
          STMTBL [stmcount]^.typ := block;
          STMTBL [stmcount]^.nr := blokno
         END
      END;
     CASE idcod OF
                         1 : advance;
                      2,18 : saveass;
     4,6,11,14,15,16,19,24 : mtelsrqd;
                         8 : gate;
                         9 : generate;
                        12 : link;
                        13 : logic;
                        25 : terminate;
                        26 : test;
                        27 : transfer;
                        28 : unlink;
     END {CASE}
  END;
END; {blkstmt}
{}
{I CTRLSTMT.TXT}
PROCEDURE ctrlstmt;
{
}
{I CLEAR.TXT}
PROCEDURE clear;
VAR i : LONGINT;
BEGIN {clear}
IF NOT stop THEN
  BEGIN
   IF ch <> ' ' THEN error (9) ELSE
      BEGIN
       nextchar;
       FOR i := 1 TO 5 DO  {Ispitivanje Operanada}
       IF NOT stop THEN
        BEGIN
         STMTBL [stmcount]^.op [i].typop := adrop;
         STMTBL [stmcount]^.op [i].aop.adrs := direct;
         IF (ch = ' ') OR (ch = CHR(13)) THEN
          BEGIN
           {Missing Operand}
           STMTBL [stmcount]^.op [i].aop.nr := 0;
           STMTBL [stmcount]^.op [i].aop.name := '            ';
          END
         ELSE
          IF ch <> 'X' THEN error (11) ELSE
           BEGIN
            nextchar;
            getadr (sadr,adrbuf);
            IF adrbuf.adrs = indirect THEN error (25) ELSE
            IF adrbuf.nr > maxsav THEN error (26) ELSE
            IF NOT ((ch =' ') OR (ch =',')) THEN error (12) ELSE
              BEGIN
              STMTBL [stmcount]^.op [i].aop := adrbuf;
              IF ch = ',' THEN
                  BEGIN
                  nextchar;
                  IF ch = ' ' THEN error (9)
                  END;
              END
           END
        END;
      END
  END;
END;{clear}
{}
{I START.TXT}
PROCEDURE start;
BEGIN{start}
IF NOT stop THEN
 BEGIN
   startflg := TRUE;
   IF ch <> ' ' THEN error (9) ELSE
   BEGIN
     nextchar;
     getnum (con,n);
     STMTBL [stmcount]^.op[1].typop := numop;
     STMTBL [stmcount]^.op [1].nop := n; {Terminacioni brojac}
     IF ch = ' ' THEN
      BEGIN
         STMTBL [stmcount]^.op [2].typop := numop;
         STMTBL [stmcount]^.op [2].nop := 1;{PRINT}
      END
     ELSE
      BEGIN
        nextchar;
        slab [1] := ch;
        nextchar;
        slab [2] := ch;
        nextchar;
        slab [3] := ch;
        IF (slab [1] = 'N') AND (slab [2] = 'P') AND (slab [3] = ' ')
        THEN
         BEGIN
            STMTBL [stmcount]^.op [2].typop := numop;
            STMTBL [stmcount]^.op [2].nop := 0;{NO PRINT}
         END
        ELSE error (23)
   END
  END
 END;
END;{start}
{}
{I RESIEND.TXT}
PROCEDURE resiend;
BEGIN {resiend}
IF ch <> ' ' THEN error (9) ELSE
 IF idcod = 5 THEN stop := TRUE ;
END;{resiend}
{
}
BEGIN {ctrlstmt}
      STMTBL [stmcount]^.nr := nlab;
      STMTBL [stmcount]^.typ := control;
      IF (nlab <> 0) OR (slab <> '            ') THEN error (24)
      ELSE
      CASE idcod OF
        3 : clear;
        5,17,20 : resiend;
       21 : start;
    END {CASE}
END;{ctrlstmt}
{}
BEGIN {stmt}
   stmcount := stmcount + 1;
   new (STMTBL[stmcount]);
   IF stmcount > maxstmt THEN error (35) ELSE
   IF NOT stop THEN
        BEGIN
           STMTBL [stmcount]^.oc := idcod;{Ubacivanje operaciog}
           STMTBL [stmcount]^.lab := slab;{koda i lab. u naredbu}
        {Odredjivanje vrste naredbe}
           IF idcod IN [7,10,22..23,29] THEN
            {Deklaraciona naredba}
            declstmt
             ELSE IF idcod IN [1,2,4,6,8,9,11..16,18,19,24..28]
               THEN
                {Blok naredba}
                 blokstmt
                  ELSE
                   {Kontrolna naredba}
                   ctrlstmt;
{}
          {Popunjavanje listinga tekstom}
{}
          IF NOT errflg THEN
          BEGIN
           CASE STMTBL [stmcount]^.typ OF
             block : mLst.WRITE (strexp(inttostr(STMTBL [stmcount]^.nr),3) + '  ');
             decl,control :  mLst.WRITE ('     ')
           END; {CASE}
           FOR i := 1 TO currlen DO mLst.WRITE (linebuff [i]);
           mLst.WRITELN ('')
          END;
        END;
END; {stmt}
BEGIN {line}
IF NOT stop THEN
 BEGIN
  nlab := 0;
  slab := '            ';
  idcod := 0;
  nextchar;
  WHILE ch = '*' DO
    BEGIN
      i:= 0;
      REPEAT
        i:=i+1;
        mLst.WRITE (linebuff [i])
      UNTIL i = currlen;
      mLst.WRITELN ('');
      getline;
      nextchar
    END;

  WHILE (ch = ' ') AND (next < currlen-1) DO
  BEGIN
    nextchar; {Prazna linija}
    IF next=currlen-1 THEN
     BEGIN
       mLst.Writeln ('');
       getline;
       nextchar
     END;
  END;

  IF ch IN ['0'..'9'] THEN
    BEGIN
      getnum (numlab,nlab);
      IF NOT STOP  THEN
         BEGIN
            WHILE ch = ' ' DO nextchar;{izbacivanje blanko}
            IF NOT (ch IN ['A'..'Z']) THEN error(15)
                ELSE
                   BEGIN
                   getid(idcod);
                   IF idcod <= 0 THEN error(17) ELSE  stmt
                   END
         END;
    END
        ELSE
          BEGIN
            IF ch IN ['A'..'Z'] THEN
               BEGIN
                 getid (idcod);
                 IF idcod = 0 THEN
                    BEGIN
                      slab := id;
                      IF NOT (ch = ' ') THEN error (9) ELSE
                        BEGIN
                          WHILE ch = ' ' DO nextchar;
                          IF NOT (ch IN ['A'..'Z'])
                                THEN error(15)
                                ELSE
                                 BEGIN
                                  getid(idcod);
                                  IF idcod <= 0 THEN error(17)
                                    ELSE stmt
                                 END
                        END
                    END
                        ELSE
                          IF idcod > 0 THEN stmt
                             ELSE error (17)
               END
               ELSE
                BEGIN
                   error (18)
                END
          END
 END;
END;{line}

{I INIT.TXT}
PROCEDURE init;
{}
BEGIN  {init}
{}
OPKWT [1] := 'ADVANCE     ';  {Dodeljivanje vrednosti tabeli}
OPKWT [2] := 'ASSIGN      ';        {operaciognih kodova}
OPKWT [3] := 'CLEAR       ';
OPKWT [4] := 'DEPART      ';
OPKWT [5] := 'END         ';
OPKWT [6] := 'ENTER       ';
OPKWT [7] := 'FUNCTION    ';
OPKWT [8] := 'GATE        ';
OPKWT [9] := 'GENERATE    ';
OPKWT[10] := 'INITIAL     ';
OPKWT[11] := 'LEAVE       ';
OPKWT[12] := 'LINK        ';
OPKWT[13] := 'LOGIC       ';
OPKWT[14] := 'MARK        ';
OPKWT[15] := 'QUEUE       ';
OPKWT[16] := 'RELEASE     ';
OPKWT[17] := 'RESET       ';
OPKWT[18] := 'SAVEVALUE   ';
OPKWT[19] := 'SEIZE       ';
OPKWT[20] := 'SIMULATE    ';
OPKWT[21] := 'START       ';
OPKWT[22] := 'STORAGE     ';
OPKWT[23] := 'TABLE       ';
OPKWT[24] := 'TABULATE    ';
OPKWT[25] := 'TERMINATE   ';
OPKWT[26] := 'TEST        ';
OPKWT[27] := 'TRANSFER    ';
OPKWT[28] := 'UNLINK      ';
OPKWT[29] := 'VARIABLE    ';
                     {Dodeljivanje vrednosti tabeli SNA}
SNAKWT [1] := 'C1 ';
SNAKWT [2] := 'CA ';
SNAKWT [3] := 'CC ';
SNAKWT [4] := 'CH ';
SNAKWT [5] := 'CM ';
SNAKWT [6] := 'CT ';
SNAKWT [7] := 'F  ';
SNAKWT [8] := 'FC ';
SNAKWT [9] := 'FN ';
SNAKWT[10] := 'FR ';
SNAKWT[11] := 'FT ';
SNAKWT[12] := 'M1 ';
SNAKWT[13] := 'MP ';
SNAKWT[14] := 'N  ';
SNAKWT[15] := 'P  ';
SNAKWT[16] := 'PR ';
SNAKWT[17] := 'Q  ';
SNAKWT[18] := 'QA ';
SNAKWT[19] := 'QC ';
SNAKWT[20] := 'QM ';
SNAKWT[21] := 'QT ';
SNAKWT[22] := 'QX ';
SNAKWT[23] := 'QZ ';
SNAKWT[24] := 'R  ';
SNAKWT[25] := 'RN ';
SNAKWT[26] := 'S  ';
SNAKWT[27] := 'SA ';
SNAKWT[28] := 'SC ';
SNAKWT[29] := 'SM ';
SNAKWT[30] := 'SR ';
SNAKWT[31] := 'ST ';
SNAKWT[32] := 'TB ';
SNAKWT[33] := 'TC ';
SNAKWT[34] := 'TD ';
SNAKWT[35] := 'V  ';
SNAKWT[36] := 'W  ';
SNAKWT[37] := 'X  ';
{}
stmcount := 0;
blkcount := 0;
chacount := 0;
faccount := 0;
funcount := 0;
logcount := 0;    {anuliranje brojaca entiteta}
savcount := 0;
stocount := 0;
tabcount := 0;
quecount := 0;
varcount := 0;
{}
mLst.WRITELN ('GPSS/FON - Assembler  Ver. 4.0, 2003');
mLst.WRITELN ('-------------------------------------');
{}
lineno := 0;
errorcod := 0;     {Postavljanje pocetnih vrednosti}
stop := FALSE;
errflg := FALSE;
startflg := FALSE;
{}
END;{init}

{I CLOSEFIL.TXT}
PROCEDURE GPSS_closefile;
{}
VAR Errmsg : string[64]; {Poruka o gresci}
    i : longint;
{}
BEGIN {closefile}
mLst.WRITELN ('------------------------------------');
   IF errflg THEN
     BEGIN   {Citanje datoteke greski}
       mLst.WRITELN ('Error detected');
       IF errorcod > 0 THEN
        BEGIN
          ASSIGN (ERRFILE,'GPSSASM.ERR');
          RESET (errfile);
          FOR i := 1 TO errorcod DO READLN (errfile,Errmsg);
          CLOSE (errfile);
          mLst.WRITELN (Errmsg)
        END;
     END
        ELSE mLst.WRITELN ('No errors detected');
   CLOSE (progfile);

   // Nema potrebe za zatvaranjem posto se i ne snima u ove datoteke, BJ 6/7/2003
   //CLOSE (listfile);
   // mLst.SaveToFile('._lst');
   // CLOSE (objfile);
   // mObj.SaveToFile('.obj');
   // mObj.Free;
   // CLOSE (oblfile); {Ako sklonim ; nema koda za ovu liniju !?!?!?!?}
   // mObl.SaveToFile('_obl._xt');
   // mObl.Free;
END;{closefile}

{I ASSIGNN.TXT}
Function assignname(naziv_fajla : string) : boolean;{Dodeljuje stvarna imena datotekama}

LABEL 10;

TYPE s3 = STRING [3];

VAR llbuf : STRING[80];
//    ch : CHAR ;
    {i,j,k,l,m : LONGINT;
    flg : BOOLEAN;}

PROCEDURE putext (VAR f: string25; ext :s3);
{}
VAR j : LONGINT;
{}
  BEGIN {putext}
    j := POS ('.',f);
    IF j = 0 THEN f:= CONCAT (f,'.',ext);
 END; {putext}

PROCEDURE RemBlanco (VAR f : STRING25); {Vadjenje blanko karaktera}

VAR i: LONGINT; f1 : string25;

BEGIN
 f1 := '';
 FOR i:=1 TO LENGTH (f) DO IF f[i] <> ' ' THEN  f1 :=CONCAT (f1,f[i]);
 f := COPY (f1,1,LENGTH(f1))
END;

BEGIN {assignname}
  10:llbuf :='';
     obnm:='';
     oblnm:='';
     lsnm:='';
     srnm:='';
{     WRITELN(ASM_izlaz);
     WRITELN(ASM_izlaz,'GPSS/FON Ver. 2.01, Assembler phase');
     WRITELN(ASM_izlaz,'(C) B.Radenkovic   1985,1987,1992,1995');
     WRITELN(ASM_izlaz);}
{     IF Paramcount > 0 THEN
      FOR i := 1 TO Paramcount DO llbuf := llbuf + paramstr(i)
       ELSE BEGIN
              WRITE (ASM_izlaz,'GPSSASM >');
              READLN (llbuf);
            END;
     j := POS (',',llbuf);
     k := POS ('=',llbuf);
     l := LENGTH (llbuf);
     IF j > 1 THEN obnm := COPY (llbuf,1,j-1)
      ELSE
       IF (j = 0) AND (k > 1) THEN  obnm := COPY (llbuf,1,k-1)
         ELSE IF (j = 1) OR (k = 1) THEN obnm := 'NUL';
     IF (k > j+1) AND (j <> 0) THEN lsnm := COPY (llbuf,j+1,k-j-1)
             ELSE lsnm := 'CON';
     IF (k+1 <= l) AND (l > 0) AND (k > 0)
      THEN  srnm := COPY (llbuf,k+1,l-k)
       ELSE
        BEGIN
         WRITELN(ASM_izlaz);
         WRITELN (ASM_izlaz,'*** Ilegal command');
         Halt
        END;

     remblanco (srnm);
     remblanco (lsnm);
     remblanco (obnm);
     remblanco (oblnm);
     IF lsnm = '' THEN lsnm := 'CON';
     putext (srnm,'GPS');
     oblnm := COPY(obnm,1,LENGTH(obnm));
     IF obnm <> 'NUL' THEN
        BEGIN
          m := POS ('.',oblnm);
          IF m > 0 THEN Delete (oblnm,m,Length(Oblnm)-m + 1);
          putext (obnm,'OBJ');
          putext (oblnm,'OBL')
        END;
     IF lsnm <> 'CON'  THEN putext (lsnm,'LST');

 }
  {   ASSIGN (progfile,srnm.gps);
     ASSIGN (listfile,lsnm);
     ASSIGN (objfile,obnm);
     ASSIGN (oblfile,oblnm);}
     ASSIGN (progfile,naziv_fajla+'.gps');       {Milos Izmenio !!!!!!!}

     // Umesto assign se kreiraju liste i incijalizuju pointeri, BJ 6/7/2003
     // ASSIGN (listfile,naziv_fajla+'.lst');
     mLst := TMSLst.Create;
     // ASSIGN (objfile,naziv_fajla+'.obj');
     //mObj := TMSLst.Create;//*BojanNenadJovicic jul, avgust 2003.*
     // ASSIGN (oblfile,naziv_fajla+'.obl');
     // mObl := TMSLst.Create;
     result := true;
{$I-}
     RESET (progfile);
{$I+}
     IF NOT (IOresult=0) THEN
       BEGIN
        result := false;
{        WRITELN(ASM_izlaz);
        WRITELN (ASM_izlaz,'**** File not found');
 }       {Halt}
        exit;
       END
        ELSE BEGIN
               // REWRITE (listfile);
               // REWRITE (objfile);
               // REWRITE (oblfile);
             END;
END; {assignname}

{I RESOLV.TXT}
PROCEDURE resolv;
{Dodeljuje odgovarajuce brojeve simbolicki adresiranim
entitetima i stampa obj. verziju programa}
// u novoj verziji ne stampa obj. verziju programa, zato sto procesor
// podatke iz faze asembliranja ucitava direktno preko procedure
// prepareSimulation//*BojanNenadJovicic jul, avgust 2003.*
VAR OPNRTBL :ARRAY [1..nop] OF LONGINT;
    i,j,k,n : LONGINT;

{PROCEDURE writeobj;//vise se ne koristi*BojanNenadJovicic jul, avgust 2003.*
//Stampa object cod GPSS programa u datoteke objfile i oblfile
VAR i,j,k : LONGINT;
PROCEDURE writeop (VAR outop : operand );
//Stampa operand u datoteke oblfile i listfile
BEGIN //writeop
 IF NOT errflg THEN
  BEGIN
   // mObl.WRITE ('   ');
   CASE outop.typop OF
     numop : BEGIN
               mObj.WRITE (' % ' + IntToStr(outop.nop));
               // mObl.WRITE (StrExp(inttostr(outop.nop),6))
             END;
     snaop : BEGIN
              mObj.WRITE (' & ' + IntToStr(ABS (ORD(outop.sop.snacod)))); // BJ 6/7/2003
              // mObl.WRITE (SNAKWT [ABS(ORD(outop.sop.snacod))]);
              CASE outop.sop.snaadr.adrs OF
               direct:BEGIN
                     mObj.WRITE (' D ' + IntToStr(outop.sop.snaadr.nr));
                     // mObl.WRITE (StrExp(IntToStr(outop.sop.snaadr.nr),3))
                      END;
             indirect:BEGIN
                       mObj.WRITE(' * ' + IntToStr(outop.sop.snaadr.par));
                       // mObl.WRITE('*' + StrExp(IntToStr(outop.sop.snaadr.par),2))
              END;
           ELSE BEGIN END;
        END  //CASE
       END; //snaop
     adrop : BEGIN
              CASE outop.aop.adrs OF
               direct : BEGIN
                         mObj.WRITE (' D ' + IntToStr(outop.aop.nr));
                         // mObl.WRITE (StrExp(IntToStr(outop.aop.nr),6))
                        END;
               indirect : BEGIN
                           mObj.WRITE (' * ' + IntToStr(outop.aop.par));
                           // mObl.WRITE ('   ' + '*' + StrExp(IntToStr(outop.aop.par),2))
                          END;
                    ELSE
                        BEGIN
                        END;
              END  //CASE
             END; //adrop
   END  //CASE
  END;
END; //writeop
BEGIN //writeobj
 IF NOT errflg THEN
 BEGIN
  // mObl.WRITELN ('GPSS/FON - Assembler, Ver.2.01, 1995, Obl.file');
  // mObl.WRITELN ('---------------------------------------------');
  FOR i := 1 TO stmcount DO
   BEGIN
    mObj.WRITE (IntToStr(STMTBL [i]^.nr) + '  ' + IntToStr(STMTBL [i]^.oc) + '  ');
    // IF STMTBL [i]^.nr <> 0 THEN
    // mObl.WRITE (StrExp(IntToStr(STMTBL [i]^.nr),3) + ' ' + OPKWT [STMTBL [i]^.oc])
    // ELSE
    // mObl.WRITE ('    ' + OPKWT [STMTBL [i]^.oc]);
    k := OPNRTBL [STMTBL [i]^.oc];
    FOR j := 1 TO k DO writeop (STMTBL [i]^.op [j]);
    CASE STMTBL [i]^.oc OF
     7 : BEGIN //Function
         // mObl.WRITELN ('');
         FOR j := 1 TO ABS (STMTBL [i]^.op [2].nop) DO
          BEGIN
           mObj.WRITE ('   ' +
            StrExp(FormatFloat('0.000', FUNTBL [STMTBL [i]^.nr].point [j].x), 8) + '   ' +
            StrExp(FormatFloat('0.000', FUNTBL [STMTBL [i]^.nr].point [j].y), 8));
           // mObl.WRITE (
           // StrExp(FormatFloat('0.000', FUNTBL [STMTBL [i]^.nr].point [j].x), 8) + ' , ' +
           // StrExp(FormatFloat('0.000', FUNTBL [STMTBL [i]^.nr].point [j].y), 8) + ' / ' )
          END
         END;
         //Variable
    29 : FOR j := 1 TO STMTBL [i]^.op [1].nop DO
          IF VARTBL [STMTBL [i]^.nr].op [j]^.typ  = voperator
          THEN
           BEGIN
            mObj.WRITE (' ! ');
            mObj.WRITE (VARTBL [STMTBL[i]^.nr].op[j]^.oprtr);
            // mObl.WRITE (' ' + VARTBL [STMTBL[i]^.nr].op[j]^.oprtr + ' ')
           END
          ELSE
           BEGIN
            mObj.WRITE ('  ');
            writeop (VARTBL [STMTBL[i]^.nr].op[j]^.oprnd)
           END;
            ELSE
                        BEGIN
                        END;
    END; //CASE
    mObj.WRITELN ('');
    // mObl.WRITELN ('')
   END;
 END;
END; //writeobj }//Izbacen jer se vise ne koristi obj. verzija*BojanNenadJovicic jul, avgust 2003.*

PROCEDURE nument;
{Dodeljuje odgovarajuce brojeve simbolicki definisanim
entitetima}
LABEL 10;
VAR i,j,k,l,entcount,entnr,nr,nri : LONGINT;
    name : stringid;
BEGIN {nument}
 IF NOT errflg THEN
  BEGIN
   FOR entnr := 1 TO  9 DO
   BEGIN
    CASE entnr OF
     1 : entcount := chacount;
     2 : entcount := faccount;
     3 : entcount := funcount;
     4 : entcount := logcount;
     5 : entcount := savcount;
     6 : entcount := stocount;
     7 : entcount := quecount;
     8 : entcount := tabcount;
     9 : entcount := varcount;
         ELSE
                        BEGIN
                        END;
    END; {CASE}
    l := 1;
    FOR i := 1 TO entcount DO
        BEGIN
    10 : k := 0;
         FOR j := 1 TO entcount DO
             BEGIN
               CASE entnr OF
                1 : nr := CHATBL [j].nr;
                2 : nr := FACTBL [j].nr;
                3 : nr := FUNTBL [j].nr;
                4 : nr := LOGTBL [j].nr;
                5 : nr := SAVTBL [j].nr;
                6 : nr := STOTBL [j].nr;
                7 : nr := QUETBL [j].nr;
                8 : nr := TABTBL [j].nr;
                9 : nr := VARTBL [j].nr;
               ELSE
                        BEGIN
                        END;
              END; {CASE}
              IF j = i THEN nri := nr;
              IF nr = l THEN k := 1;
             END;{FOR}
         IF k = 1 THEN {Broj postoji u tabeli}
          BEGIN
           l := l + 1; {Treba ga preskociti}
           GOTO 10  {I ponovno pretraziti tabelu}
          END
          ELSE  BEGIN  {Broj l ne postoji u tabeli }
                {Ako je broj entiteta 0 treba mu dodeliti broj}
                 IF nri = 0 THEN
                  BEGIN
                   CASE entnr OF
                    1 : CHATBL [i].nr := l;
                    2 : FACTBL [i].nr := l;
                    3 : FUNTBL [i].nr := l;
                    4 : LOGTBL [i].nr := l;
                    5 : SAVTBL [i].nr := l;
                    6 : STOTBL [i].nr := l;
                    7 : QUETBL [i].nr := l;
                    8 : TABTBL [i].nr := l;
                    9 : VARTBL [i].nr := l;
                     ELSE
                        BEGIN
                        END;
                   END; {CASE}
                   l := l + 1 {I povecati brojac za jedan }
                  END;
                END;
             END;{FOR}
     {Stampanje rezultata dodeljivanja}
     k := 0;   {Resetovanje flaga za zaglavlje}
     FOR j := 1 TO entcount DO
      BEGIN
      CASE entnr OF
      1 : BEGIN  nr:= CHATBL [j].nr; name:= CHATBL [j].name END;
      2 : BEGIN  nr:= FACTBL [j].nr; name:= FACTBL [j].name END;
      3 : BEGIN  nr:= FUNTBL [j].nr; name:= FUNTBL [j].name END;
      4 : BEGIN  nr:= LOGTBL [j].nr; name:= LOGTBL [j].name END;
      5 : BEGIN  nr:= SAVTBL [j].nr; name:= SAVTBL [j].name END;
      6 : BEGIN  nr:= STOTBL [j].nr; name:= STOTBL [j].name END;
      7 : BEGIN  nr:= QUETBL [j].nr; name:= QUETBL [j].name END;
      8 : BEGIN  nr:= TABTBL [j].nr; name:= TABTBL [j].name END;
      9 : BEGIN  nr:= VARTBL [j].nr; name:= VARTBL [j].name END;
         ELSE
                        BEGIN
                        END;
      END; {CASE}
      IF name <> '            ' THEN
       BEGIN
        IF k = 0 THEN {Stampanje zaglavlja}
           BEGIN
            k := 1; {Setovanje flaga za zaglavlje}
            mLst.WRITELN ('');
            CASE entnr OF
             1 : mLst.WRITE ('Chains ');
             2 : mLst.WRITE ('Facility ');
             3 : mLst.WRITE ('Function ');
             4 : mLst.WRITE ('Logic ');
             5 : mLst.WRITE ('Savevalue ');
             6 : mLst.WRITE ('Storage ');
             7 : mLst.WRITE ('Queue ');
             8 : mLst.WRITE ('Table ');
             9 : mLst.WRITE ('Variable ');
                ELSE
                        BEGIN
                        END;
            END; {CASE}
          mLst.WRITELN ('symbols and corresponding numbers')
           END; {Stampanje zaglavlja}
           mLst.WRITELN (strexp(inttostr(nr),3) + ':' + strexp(name,15));
       END;{Stampanje simbola}
      END;{FOR}
    END {FOR}
  END;
END; {nument}
PROCEDURE entsrc (entnr : LONGINT; name : stringid;
                                VAR nr :LONGINT);
{Pretrazuje tabelu entiteta i vraca broj za zadato ime }
VAR n,i,entcount : LONGINT;                 {nr = 0 ime nije pronadjeno}
  name1 : stringid;              {nr > 0 ime pronadjeno }
BEGIN {entsrc}
IF nr = 0 THEN
 BEGIN
 CASE entnr OF
 1 : entcount := chacount;
 2 : entcount := faccount;
 3 : entcount := funcount;
 4 : entcount := logcount;
 5 : entcount := savcount;
 6 : entcount := stocount;
 7 : entcount := quecount;
 8 : entcount := tabcount;
 9 : entcount := varcount;
10 : entcount := blkcount;
      ELSE
                        BEGIN
                        END;
END; {CASE}
nr := 0;
FOR i := 1 TO entcount DO
BEGIN
 CASE entnr OF
  1 : BEGIN name1 := CHATBL [i].name; n := CHATBL [i].nr END;
  2 : BEGIN name1 := FACTBL [i].name; n := FACTBL [i].nr END;
  3 : BEGIN name1 := FUNTBL [i].name; n := FUNTBL [i].nr END;
  4 : BEGIN name1 := LOGTBL [i].name; n := LOGTBL [i].nr END;
  5 : BEGIN name1 := SAVTBL [i].name; n := SAVTBL [i].nr END;
  6 : BEGIN name1 := STOTBL [i].name; n := STOTBL [i].nr END;
  7 : BEGIN name1 := QUETBL [i].name; n := QUETBL [i].nr END;
  8 : BEGIN name1 := TABTBL [i].name; n := TABTBL [i].nr END;
  9 : BEGIN name1 := VARTBL [i].name; n := VARTBL [i].nr END;
 10 : BEGIN name1 := BLKTBL [i];      n := i  END;
      ELSE
           BEGIN
           END;
 END; {CASE}
 IF (name = name1) AND (name <> '            ') THEN nr := n;
END;
 IF nr = 0 THEN
 BEGIN
  mLst.WRITELN (''); mLst.WRITE ('*** FATAL error * ');
  CASE entnr OF
  1 : mLst.WRITE ('Chain');
  2 : mLst.WRITE ('Facilty');
  3 : mLst.WRITE ('Function');
  4 : mLst.WRITE ('Logic');
  5 : mLst.WRITE ('Savevalue');
  6 : mLst.WRITE ('Storage');
  7 : mLst.WRITE ('Queue');
  8 : mLst.WRITE ('Table');
  9 : mLst.WRITE ('Variable');
 10 : mLst.WRITE ('Block');
   ELSE
           BEGIN
           END;
  END; {CASE}
  mLst.WRITELN (' ' + name + '  not specified');
  errflg := TRUE
 END;
 END;
END; {entsrc}

PROCEDURE snasrc (VAR S:Sna);
{Ova procedura dodeljuje odgovarajucu numericku adresu za sna }
VAR k : LONGINT;
n : snaCodes;
  BEGIN {snasrc}
    n := (S.snacod);  // Izbaceno ABS, BJ 6/7/2003
    IF (S.snaadr.adrs = direct) AND (S.snaadr.name <> '            ')
       AND NOT (n IN [  snaC1,
                        snaM1,
                        snaMP,
                        snaP,
                        snaPR,
                        snaRN]) THEN
       BEGIN
         CASE n OF
            snaR,
            snaS,
            snaSA,
            snaSC,
            snaSM,
            snaSR,
            snaST
                : k:=6; { SNA za STORAGE }
            snaQ,
            snaQA,
            snaQC,
            snaQM,
            snaQT,
            snaQX,
            snaQZ
                : k:=7; { SNA za QUEUE }
            snaF,
            snaFC,
            snaFR,
            snaFT
                : k:=2; { SNA za FACILITY }
            snaN,
            snaW
                : k:=10;{ SNA za BLOK }
            snaTB,
            snaTC,
            snaTD
                : k:=8; { SNA za TABLE }
            snaCA,
            snaCC,
            snaCH,
            snaCM,
            snaCT
                : k:=1; { SNA za CHAIN }
            snaFN
                : k:=3; { FUNCTION }
            snaX
                : k:=5; { SAVEVALUE }
            snaV
                : k:=9; { VARIABLE }
           ELSE BEGIN END;
         END; {CASE}
         entsrc (k,S.snaadr.name,S.snaadr.nr)
     END;
  END; {snasrc}


BEGIN {resolv}
 IF NOT errflg THEN
  BEGIN
   OPNRTBL [1] := 2;
   OPNRTBL [2] := 3;
   OPNRTBL [3] := 5;
   OPNRTBL [4] := 1;
   OPNRTBL [5] := 0;
   OPNRTBL [6] := 1;
   OPNRTBL [7] := 2;
   OPNRTBL [8] := 3;
   OPNRTBL [9] := 5;
   OPNRTBL [10] := 2;
   OPNRTBL [11] := 1;
   OPNRTBL [12] := 2;
   OPNRTBL [13] := 2;
   OPNRTBL [14] := 0;
   OPNRTBL [15] := 1;
   OPNRTBL [16] := 1;
   OPNRTBL [17] := 0;
   OPNRTBL [18] := 3;
   OPNRTBL [19] := 1;
   OPNRTBL [20] := 0;
   OPNRTBL [21] := 2;
   OPNRTBL [22] := 1;
   OPNRTBL [23] := 4;
   OPNRTBL [24] := 1;
   OPNRTBL [25] := 1;
   OPNRTBL [26] := 4;
   OPNRTBL [27] := 3;
   OPNRTBL [28] := 4;
   OPNRTBL [29] := 1;

{  Dodeljivanje brojnih vrednosti labelama naredbi }

   nument;
   FOR i := 1 TO stmcount DO
    BEGIN
     IF STMTBL [i]^.oc IN [7,22,23,29] THEN
      BEGIN   {Labele entiteta}
       CASE STMTBL [i]^.oc OF
        7 : k := 3; {Function}
        22 : k := 6; {Storage}
        23 : k := 8; {Table}
        29 : k := 9; {Variable}
        ELSE
           BEGIN
           END;
       END; {CASE}
       entsrc (k,STMTBL [i]^.lab,STMTBL [i]^.nr);
       { Dodeljivanje brojeva za sna u VARIABLE }
       IF k = 9 THEN
         FOR j:=1 TO STMTBL [i]^.op[1].nop DO
           IF  (VARTBL [STMTBL[i]^.nr].op[j]^.typ = voperand)
           AND (VARTBL [STMTBL[i]^.nr].op[j]^.oprnd.typop = snaop) THEN
              snasrc (VARTBL[STMTBL[i]^.nr].op[j]^.oprnd.sop);
      END;

{    Dodeljivanje odgovarajucih brojeva adresnim operandima naredbi }

     FOR j := 1 TO OPNRTBL [STMTBL [i]^.oc ] DO
      BEGIN {Dodeljivanje brojeva adresama operanada}
       IF (j = 1) AND (STMTBL [i]^.op [j].typop = adrop) AND
             (STMTBL [i]^.op [j].aop.adrs = direct) AND
             (STMTBL [i]^.op [j].aop.name <> '            ') THEN
        { Operacioni kodovi koji imaju samo jedan operand }
        BEGIN   {Adrese entiteta}
         CASE STMTBL [i]^.oc OF
           10,18 : k := 5; {Savevalue}
           4 : k := 7; {Depart}
           6 : k := 6; {Enter}
           11 : k := 6; {Leave}
           12 : k := 1; {Link}
           15 : k := 7; {Queue}
           16 : k := 2; {Relase}
           19 : k := 2; {Seize}
           24 : k := 8; {Tablulate}
           28 : k := 1; {Unlink}
           ELSE k := 10;
         END; {CASE}
         entsrc (k,STMTBL [i]^.op [1].aop.name,
                              STMTBL [i]^.op [1].aop.nr);
        END ELSE
          IF (STMTBL [i]^.op [j].typop = adrop) AND
             (STMTBL [i]^.op [j].aop.adrs = direct) AND
             (STMTBL [i]^.op [j].aop.name <> '            ') THEN
           { Operand je adresni }
           BEGIN   {Dodeljivanje adresa za adr.op}
            IF STMTBL [i]^.oc = 3 THEN k := 5 ELSE k := 10;
            IF j = 2 THEN
               CASE STMTBL [i]^.oc OF
                      {GATE}
                  8 : CASE STMTBL [i]^.op [1].nop OF
                         1,2 : k := 4; {LS  LR}
                         3,4,5,6 : k := 6; {SE SF SNE SNF }
                         7,8 : k := 2 {U NU}
                 END; {CASE}
                 13 : k := 4 {LOGIC}
                ELSE
                   BEGIN
                   END;
               END; {CASE}
            entsrc (k,STMTBL [i]^.op [j].aop.name,
                                     STMTBL [i]^.op [j].aop.nr)
           END
           { Operand je SNA }
            ELSE  {Dodeljivanje adresa za SNA}
              IF (STMTBL [i]^.op [j].typop = snaop)
                THEN snasrc (STMTBL [i]^.op[j].sop);
      END {FOR}
    END; {FOR}
   //writeobj;// jer se vise ne generise obj.*BojanNenadJovicic jul, avgust 2003.*
  END;
END; {resolv}

Function GPSSASM_START(n : string) : boolean;
BEGIN  {gpssasm}
{ assignfile(ASM_izlaz, 'gpssasm.tmp');}
{ rewrite(ASM_izlaz);}
try
 Bilo_gresaka := false;
 if assignname(n) then //Ovo je pocetak rada gpss asm, ovde treba spojiti
  begin                // procesor      *BojanNenadJovicic jul, avgust 2003.*
   init;
     WHILE NOT stop DO
         BEGIN
           getline;
           line ;
         END;
     resolv;
    GPSS_closefile;
   end;
  result := Bilo_gresaka;
 except
  result := true;
   CLOSE (progfile);
   // CLOSE (listfile);
   // CLOSE (objfile);
   // CLOSE (oblfile); {Ako sklonim ; nema koda za ovu liniju !?!?!?!?}
 end;
{ closefile(ASM_izlaz);}
 {sound (800);} {Signalizacija zavrsetka} {JA PROMENIO}
 {delay (500);} {JA PROMENIO}
{NoSound}{JA PROMENIO}
end;

END.
