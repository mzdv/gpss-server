unit UFormDijagram;//*BojanNenadJovicic jul, avgust 2003.
                   {forma na kojoj se iscrtava dijagram,
                    ima metode za ucitavanje, snimanje, brisanje
                    i iscrtavanje dijagram. Nasledjuje TForm}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ToolWin, ComCtrls, StdCtrls, Buttons, ExtCtrls;

type
  TFormDijagram = class(TForm)
    panVrh: TPanel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    BitBtn4: TBitBtn;
    sbDijagram: TScrollBox;
    Bevel1: TBevel;
    SpeedButton4: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton1: TSpeedButton;
    BitBtn5: TBitBtn;
    Bevel2: TBevel;
    odOtvori: TOpenDialog;
    sdSnimi: TSaveDialog;
    procedure BitBtn1Click(Sender: TObject);
    procedure BitBtn2Click(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure BitBtn4Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure BitBtn5Click(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
  private
    listaSimbola : TList;
    parentSimbola : TWinControl;
    procedure nacrtajDijagram;
    procedure obrisiDijagram;
    procedure prevediDijagram;
    procedure snimiDijagram(fileName : string);
    procedure ucitajDijagram(fileName : string);
    { Private declarations }
  public
    { Public declarations }
  end;

var
  FormDijagram: TFormDijagram;

implementation

uses UGrafickiSimboli, Main;

{$R *.DFM}

procedure TFormDijagram.nacrtajDijagram();
var i, countTransfer : integer;
    simbol, simbolPrev : TSimbol;
    prevTransfer : boolean;
    simbolTransfer : TSimbolTransfer;
begin
  obrisiDijagram;
  countTransfer := 0;
  for i := 0 to ( listaSimbola.Count - 1 ) do
  begin
    simbol := listaSimbola.Items[i];
    simbol.Parent := sbDijagram;
    prevTransfer := false;
    if ( i <> 0 ) then
    begin
      simbolPrev := listaSimbola.Items[i-1];
      if ( simbolPrev.getType = 'transfer' ) then
      begin
        prevTransfer := true;
        simbolTransfer := listaSimbola.Items[i-1];
        simbol.setPictureLeft;
        simbol.Left := (FormDijagram.Width - 140 ) div 2 + 140;
        simbol.Top := (i - countTransfer -1) * 90;
        simbol.setPrefiks(simbolTransfer.getTransfer);
        inc(countTransfer);
      end;
    end;
    if ( not prevTransfer ) then
    begin
      simbol.setPictureCenter;
      simbol.Left := (FormDijagram.Width - 140 ) div 2;
      simbol.Top := (i - countTransfer ) * 90;
    end;
  end;

end;//nacrtajDijagram

procedure TFormDijagram.obrisiDijagram();
var i : integer;
    simbol : TSimbol;
begin
  for i := 0 to ( listaSimbola.Count - 1 ) do
  begin
    simbol := listaSimbola.Items[i];
    simbol.Parent := parentSimbola;
  end;

  sbDijagram.Destroy;
  sbDijagram := TScrollBox.Create(FormDijagram);
  sbDijagram.Parent := FormDijagram;
  sbDijagram.Align := alClient;
  sbDijagram.Color := clWhite;
end;//obrisiDijagram

procedure TFormDijagram.prevediDijagram;
var i, j, brojTransfera: integer;
    simbol, simbolPom, simbolPrev : TSimbol;
    prevTransfer : boolean;
    nizTransfera : Array[1..100] of integer;
begin
  AForm.kod.Clear;
  AForm.kod.Lines.Add('SIMULATE');
  brojTransfera := 0;
  //ispisivanje u kod gpss programa
  for i := 0 to ( listaSimbola.Count - 1) do
  begin
    simbol := listaSimbola.Items[i];
    prevTransfer := false;
    if ( i <> 0 ) then
    begin
      simbolPrev := listaSimbola.Items[i-1];
      if ( simbolPrev.getType = 'transfer' ) then
      begin
        prevTransfer := true;
        inc(brojTransfera);
        nizTransfera[brojTransfera] := i;
      end;
    end;
    if ( not prevTransfer ) then
    begin
      if (simbol.getType = 'start' ) then
      begin
        //naredbe koje idu iz transfera
        for j := 1 to ( brojTransfera ) do
        begin
          simbolPom := listaSimbola[nizTransfera[j]];
          AForm.kod.Lines.Add(simbolPom.getSimbolText);
        end;
        brojTransfera := 0;
      end;
      AForm.kod.Lines.Add(simbol.getSimbolText);
    end;
 end;

 AForm.kod.Lines.Add('END');
end;//prevediDijagram

procedure TFormDijagram.snimiDijagram(fileName : string);
var fDijagram : textFile;
    fLinija : string;
    simbol : TSimbol;
    i : integer;
begin
  assignFile(fDijagram, fileName);
  rewrite(fDijagram);
  for i := 0 to ( listaSimbola.Count - 1 ) do
  begin
    simbol := listaSimbola[i];
    fLinija := simbol.Save;
    writeln(fDijagram, fLinija);
  end;
  closeFile(fDijagram);

end;//snimiDijagram

procedure TFormDijagram.ucitajDijagram(fileName : string);
var fDijagram : textFile;
    fLinija : string;
    simbol : TSimbol;
begin
  assignFile(fDijagram, fileName);
  reset(fDijagram);
  while ( not Eof(fDijagram) ) do
  begin
    Readln(fDijagram, fLinija);
    simbol := TSimbol.CreateFromString(fLinija, FormDijagram);
    listaSimbola.Add(simbol);
  end;
  closeFile(fDijagram);

end;//ucitajDijagram



procedure TFormDijagram.BitBtn1Click(Sender: TObject);
var simbol : TSimbolGenerate;
begin
  simbol := TSimbolGenerate.Create(FormDijagram);
  listaSimbola.Add(simbol);
  nacrtajDijagram();
end;

procedure TFormDijagram.BitBtn2Click(Sender: TObject);
var Simbol : TSimbolAdvance;
begin
  simbol := TSimbolAdvance.Create(FormDijagram);
  listaSimbola.Add(simbol);
  nacrtajDijagram;
end;

procedure TFormDijagram.BitBtn3Click(Sender: TObject);
var simbol : TSimbolTransfer;
begin
  simbol := TSimbolTransfer.Create(FormDijagram);
  listaSimbola.Add(simbol);
  nacrtajDijagram;
end;

procedure TFormDijagram.BitBtn4Click(Sender: TObject);
var simbol : TSimbolTerminate;
begin
  simbol := TSimbolTerminate.Create(FormDijagram);
  listaSimbola.Add(simbol);
  nacrtajDijagram();
end;

procedure TFormDijagram.FormCreate(Sender: TObject);
begin
  listaSimbola := TList.Create;
  listaSimbola.Count := 0;
  parentSimbola := TWinControl.Create(FormDijagram);
  parentSimbola.Visible := false;
end;

procedure TFormDijagram.SpeedButton2Click(Sender: TObject);
var i : integer;
    simbol : TSimbol;
begin
  obrisiDijagram;
  for i := 0 to ( listaSimbola.Count - 1) do
  begin
    simbol := listaSimbola.Items[i];
    simbol.Destroy;
  end;
  listaSimbola.Destroy;
  listaSimbola := TList.Create;
  listaSimbola.Count := 0;
end;

procedure TFormDijagram.SpeedButton1Click(Sender: TObject);
begin
  nacrtajDijagram;
  prevediDijagram;
  AForm.PageControl.ActivePageIndex := 0;
  Close;

end;

procedure TFormDijagram.BitBtn5Click(Sender: TObject);
var simbol : TSimbolStart;
begin
  simbol := TSimbolStart.Create(FormDijagram);
  listaSimbola.Add(simbol);
  nacrtajDijagram();

end;

procedure TFormDijagram.SpeedButton4Click(Sender: TObject);
var fileName : string;
begin
  if sdSnimi.Execute then
  begin
    fileName := sdSnimi.FileName;
    snimiDijagram(fileName);
    ShowMessage('Blok dijagram snimljen!');
  end;



end;

procedure TFormDijagram.SpeedButton3Click(Sender: TObject);
var fileName : string;
begin
  if odOtvori.Execute then
  begin
    fileName := odOtvori.FileName;
    ucitajDijagram(fileName);
    obrisiDijagram;
    nacrtajDijagram;
    ShowMessage('Blok dijagram ucitan!');
  end;

end;

end.
