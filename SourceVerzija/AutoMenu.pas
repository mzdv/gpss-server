unit AutoMenu;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Precica, menus;
const
   IS_Novi = 1; IS_Ucitaj = 2; IS_Reopen = 3; IS_Snimi = 4; IS_SnimiKao=5;
   IS_Razmak=6; IS_Font =7; IS_Oprog = 8; IS_Kraj=9; TAM_BRSTAVKI = 9;

{$DEFINE SERBIAN}

{$IFDEF SERBIAN}
  mSave =   '&Snimi';
  mSaveAs = 'Snimi pod &drugim nazivom';
  mopen =   '&Ucitaj';
  mExit =   '&Kraj';
  mAbout =  'O programu';
  mReopen=  'Ponovo ucitaj ...';
  mNew =    'Novi';
  mOpenTitle='Izaberite fajl koji zelite da ucitate';
  mSaveTitle='Izaberite naziv pod kojim ce fajl biti snimljen';
  mWarnModified='Dosada{nji podaci su bili modifikovani! Da li zelite da ih snimite?';
{$ELSE}
  mSave =   '&Save';
  mSaveAs = 'Save &as...';
  mOpen =   '&Open';
  mExit =   'E&xit';
  mAbout =  'About';
  mReopen=  'Reopen ...';
  mNew =    'New';
  mOpenTitle='Open file';
  mSaveTitle='Save file as...';
  mWarnModified='You haven''t saved the changes made to the current model! Do you want to save them now?';
{$ENDIF}

type
  TAM_UcitajEvent = procedure(naziv : string) of object;
  TAM_DefEvent = procedure (var dozvoli : boolean) of object;
  TAM_FontEvent = procedure (var font : TFont) of object;

  TAutoMenu = class(TComponent)
  private
    { Private declarations }
    FPrecica,FModifikovan,FAutoFontChange,FAktivan,
             FNazivSaEkstenzijom : boolean;
    FNazivi : array [1..TAM_BRSTAVKI] of string;
    FFajlPrecice,FNaziv,FApDir,FAutor,FProgram,FOpenTitle,FSaveTitle,
                 FExt, FFilter : string;
    FStavke : array [1..TAM_BRSTAVKI] of TMenuItem;
    FMaxFont : byte;
    FMeni : TMenuItem;
    FANP : TANP;
    FFont : TFont;
    Vlasnik : TObject;
    Fd : TFontDialog;
    FOnUcitaj, FOnSnimi : TAM_UcitajEvent;
    FOnNovi: TNotifyEvent;
    FOnKraj : TCloseQueryEvent;
    FOnOProgramu: TAM_DefEvent;
    FOnFontChange : TAM_FontEvent;

    procedure SetPrecica(b : boolean);
    procedure SetMeni (m : TMenuItem);
    procedure SetFajlPrecice (s : string);
//    procedure SetNaziv(index : integer; n : string);

    procedure OSvezi_precicu;
    procedure Ocisti;
    procedure Osvezi;
    procedure Izabrana_precica(sender : TObject; s : string);
  protected
    { Protected declarations }
    procedure itemclick(sender : TObject);
    procedure IzabranNovi;
    procedure IzabranSnimi;
    procedure IzabranUcitaj;
    procedure IzabranSnimiKao;
    procedure IzabranFont;
    function  IzabranKraj : boolean;
    procedure IzabranOProg;
    function  Proveri : boolean;
  public    { Public declarations }
    Precica : Tprecica;
    constructor Create(AOwner : TComponent); override;
    property Modifikovan : boolean read FModifikovan write FModifikovan;
    procedure Aktiviraj;
    procedure Close(sender : TObject; var allow : boolean);
    property Naziv : string read FNaziv write FNaziv;

  published
    { Published declarations }
    property Ukljuci_precicu : boolean read FPrecica write SetPrecica default false;
    property Meni : TMenuItem read FMeni write SetMeni;
    property FajlPrecice : string read FFajlPrecice write SetFajlPrecice;
{    property Naziv_Novi : string index IS_Novi  read FNazivi[IS_Novi] write Setnaziv ;
    property Naziv_Ucitaj : string index IS_Ucitaj read FNazivi[IS_Ucitaj] write Setnaziv;
    property Naziv_Reopen : string index IS_reopen read FNazivi[IS_reopen] write Setnaziv;
    property Naziv_Snimi : string index IS_Snimi read FNazivi[IS_Snimi] write Setnaziv;
    property Naziv_SnimiKao : string index IS_Snimikao read FNazivi[IS_Snimikao] write Setnaziv;
    property Naziv_Font : string index IS_Font read FNazivi[IS_Font] write Setnaziv;
    property Naziv_OProgramu: string index IS_Oprog read FNazivi[IS_Oprog] write Setnaziv;
    property Naziv_Kraj : string index IS_Kraj read FNazivi[IS_Kraj] write Setnaziv;}
    property Autor : string read FAutor write FAutor;
    property Naziv_programa : string read FPRogram write FProgram;
{    property Open_title : string read FOpenTitle write FOpenTitle;
    property Save_title : string read FSaveTitle write FSaveTitle;}
    property Default_Ekstenzija : string read FExt write FExt;
    property Filter : string read FFilter write FFilter;
    property Ako_Nema_Precice : TANP read FANP write FANP default TANP_Ignorisi;
    property Naziv_sa_ekstenzijom : boolean read FNazivSaEkstenzijom write FNazivSaEkstenzijom;
    property Auto_promena_fonta : boolean read FAUtoFontChange write FAutoFontChange;
    property Max_velicina_fonta : byte read FMaxFont write FMaxFont default 17;

    property OnUcitaj : TAM_UcitajEvent read FOnUcitaj write FOnUcitaj;
    property OnSnimi : TAM_UcitajEvent read FOnSnimi write FOnSnimi;
    property OnNovi : TNotifyEvent read FOnNovi write FOnNovi;
    property OnKraj : TCloseQueryEvent read FOnKraj write FOnKraj;
    property OnOProgramu : TAM_DefEvent read FOnOProgramu write FOnOProgramu;
    property OnFontChange : TAM_FontEvent read FOnFontChange write FOnFontChange;
  end;

procedure Register;
function BezExt(n : string) : string;

implementation

procedure Register;
begin
  RegisterComponents('Milos', [TAutoMenu]);
end;
constructor TAutoMenu.Create(AOwner : TComponent);
var i : integer;novi : TMenuItem;
begin
    inherited create(Aowner);
    vlasnik := AOwner;
    FFont := (Aowner as TForm).font;
    FApDir := getcurrentdir;
    FAktivan := false;
    FNazivi[IS_Snimi] := mSave; FNazivi[IS_Ucitaj] := mopen;
    FNazivi[IS_SnimiKao]:=mSaveAs;
    FNazivi[IS_Kraj] := mExit; FNazivi[IS_Oprog] := mAbout;
    FNazivi[IS_Reopen]:=mReopen;FNazivi[IS_Novi] := mNew;
    FNazivi[IS_Razmak]:='-';FNazivi[IS_Font]:='Font';
    FNaziv := '';
    FOpenTitle := mOpenTitle;
    FSaveTitle := mSaveTitle;
    FFilter := 'GPSS models(*.gps)|*.GPS';//'Text files (*.txt)|*.TXT|Pascal files (*.pas)|*.PAS';
     for i := 1 to TAM_BRSTAVKI do
       begin
          Novi := TMenuItem.create(self);
          Novi.caption := FNazivi[i];
          Novi.tag := i;
          Novi.name := 'Auto_Stavka'+inttostr(i);
          Novi.OnClick := itemclick;
          FStavke[i] := Novi;
       end;
     (AOWner as TForm).OnCloseQuery := close;{OnKraj;}
end;
procedure TAutoMenu.SetPrecica(b : boolean);
begin
     if FPrecica<>b then
       begin
          FPrecica := b;
          Osvezi_precicu;
          if not(b) then
             FStavke[IS_Reopen].enabled := false;
       end;
end;
procedure TAutoMenu.SetMeni (m : TMenuItem);
begin
     if m <> Fmeni then
       if FAktivan then
         begin
            Ocisti;
            FMeni := m;
            Ocisti;
            Osvezi;
         end
       else
            FMeni := m;
end;
procedure TAutoMenu.Ocisti;
begin
    if assigned(precica) then precica.Nadredjena_stavka := nil;
     if assigned(FMeni) then
        begin
          while FMeni.Count > 0 do
                FMeni.delete(0);
        end;
end;
procedure TAutoMenu.Osvezi;
var i : integer;
begin
    if assigned(FMeni) then
       begin
         while FMeni.Count > 0 do
           FMeni.delete(0);
         for i := 1 to IS_Snimikao do
            if FNazivi[i]<>'' then
               FMeni.Add(FStavke[i]);
         if (FMeni.count>0) and ((Fnazivi[IS_Oprog]<>'')or(Fnazivi[IS_kraj]<>'')or(Fnazivi[IS_Font]<>'')) then
               FMeni.Add(FStavke[IS_razmak]);
         for i := IS_Font to IS_Kraj do
            if FNazivi[i]<>'' then
               FMeni.Add(FStavke[i]);
         Osvezi_Precicu;
         if not(assigned(precica)) then
             FStavke[IS_Reopen].enabled := false;
       end;
end;
procedure TAutoMenu.SetFajlPrecice (s : string);
begin
     if s<>FFajlPrecice then
        begin
          FFajlPrecice := s;
          osvezi_precicu;
        end;
end;
procedure TAutoMenu.OSvezi_precicu;
begin
     if FPrecica then
       begin
         if not(assigned(Precica)) then
            Precica := TPrecica.create(self);
         Precica.Nadredjena_stavka := FStavke[IS_Reopen];
         Precica.Ako_nema_precice := FANP;
         Precica.OnSelect := Izabrana_precica;
         Precica.Auto_Ubaci := false;
         if FApDir[length(FApDir)]<>'\' then
            Precica.Naziv_Fajla := FApDir +'\' + FFajlPrecice
         else
            Precica.Naziv_Fajla := FApDir + FFajlPrecice;
       end
     else
       if assigned(precica) then precica.Nadredjena_stavka := nil;
end;
{procedure TAutoMenu.SetNaziv(index : integer; n : string);
begin
     if Fnazivi[index] <> n then
       begin
          if ((n = '') or (Fnazivi[index]='')) and FAktivan then
            begin
               Fnazivi[index] := n;
               ocisti;
               osvezi;
            end
          else  Fnazivi[index] := n;
          FStavke[index].caption := n;
       end;
end;}
procedure TAutoMenu.Izabrana_precica(sender : TObject; s : string);
begin
   if proveri and assigned(FOnUcitaj) then
     begin
       FOnUcitaj(s);
       if FPrecica then precica.ubaci(s);
       if FNazivSaEkstenzijom then FNaziv :=s
          else FNaziv :=bezext(s);
     end;
end;
procedure TAutoMenu.itemclick(sender : TObject);
var ko : integer;
begin
   ko := (sender as TMenuItem).tag;
     case ko of
       IS_Novi     : IzabranNovi;
       IS_Ucitaj   : if proveri then IzabranUcitaj;
{       IS_Reopen  : Reopen;}
       IS_Snimi    : IzabranSnimi;
       IS_SnimiKao : IzabranSnimikao;
{       IS_Razmak  : razmak;}
       IS_Font     : IzabranFont;
       IS_Oprog    : IzabranOprog;
       IS_Kraj     : IzabranKraj;
       end;
end;
procedure TAutoMenu.IzabranNovi;
begin
   if proveri and assigned(FOnNovi) then
     begin
        FNaziv := '';
        modifikovan := false;
        FOnNovi(self);
     end;
end;
procedure TAutoMenu.IzabranSnimi;
begin
   if naziv = '' then izabranSnimiKao
     else
       begin
         if assigned(FOnSnimi) then FOnSnimi(Fnaziv);
         modifikovan := false;
       end;
end;
procedure TAutoMenu.IzabranUcitaj;
var FOpendlg : TOpendialog;
begin
    FOpendlg := TOpenDialog.create(self);
    FOpendlg.title := FOpenTitle;
    FOpendlg.DefaultExt := Fext;
    FOpendlg.filter := ffilter;
    if FOpendlg.execute then
      begin
        Fmodifikovan := false;
        if FNazivSaEkstenzijom then FNaziv :=FOpendlg.FileName
           else FNaziv :=bezext(FOpendlg.FileName);
        if FPrecica then precica.ubaci(FNaziv);
        if assigned(FOnUcitaj) then FOnUcitaj(FNaziv);
      end;
    FOpendlg.free;
end;
procedure TAutoMenu.IzabranSnimiKao;
var FSaveDlg : TSaveDialog;
begin
   FSavedlg := TSaveDialog.create(self);
   FSavedlg.title := FSaveTitle;
   FSavedlg.DefaultExt := Fext;
   FSavedlg.filter := ffilter;
   FSavedlg.Options := FSavedlg.Options + [ofOverwritePrompt];
   if FSavedlg.execute then
         begin
           if assigned(FOnSnimi) then
             if FNazivSaEkstenzijom then FOnSnimi(FSavedlg.FileName)
                else FOnSnimi(bezext(FSavedlg.FileName));
           if FNazivSaEkstenzijom then FNaziv :=FSavedlg.FileName
              else FNaziv :=bezext(FSavedlg.FileName);
           if FPrecica then precica.ubaci(FNaziv);
           Fmodifikovan := false;
         end;
   FSavedlg.free;
end;
procedure TAutoMenu.IzabranFont;
begin
     if not assigned(fd) then fd := TFontDialog.create(self);
     if not assigned (FFont) then FFont := TFont.create;
     fd.MaxFontSize := FMaxFont;
     if fd.execute then
{       if fd.font <> FFont then}
         begin
           FFont.name:= fd.Font.name;
           FFont.style:= fd.Font.style;
           FFont.size:= fd.Font.size;
           if FAutoFontChange then (Vlasnik as TForm).font := FFont;
           if assigned(FOnFontChange) then FOnFontChange(FFont);
         end;
end;

function TAutoMenu.IzabranKraj : boolean;
var dozvoli : boolean;
begin
     dozvoli := true;
     if assigned(FOnKraj) then FOnKraj(self, dozvoli);
     if dozvoli and proveri then Application.Terminate;
     result := false;
end;
procedure TAutoMenu.IzabranOProg;
var dozvoli : boolean;
begin
     dozvoli := true;
     if assigned(FOnOProgramu) then FOnOProgramu(dozvoli);
     if dozvoli then if FProgram<>'' then
       ShowMessage(FProgram+#13+FAutor);
end;
function TAutoMenu.Proveri : boolean;
var odgovor : TModalResult;
begin
 result := true;
 if modifikovan then
    begin
      odgovor := MessageDlg(mWarnModified,mtConfirmation, [mbYes, mbNo, mbCancel], 0);
      if odgovor = mryes then
        begin
           izabransnimi;
           if modifikovan then result := false;
        end
      else
        if (odgovor = mrYes) then result := false
     end;
end;
procedure TAutoMenu.Aktiviraj;
begin
     if not FAktivan then
       begin
         FAktivan := true;
         ocisti; osvezi;
       end;
end;
procedure TAutoMenu.Close(sender : TObject; var allow : boolean);
begin
     allow := IzabranKraj;
end;
function BezExt(n : string):string;
var e : integer;
begin
   e := length(ExtractFileExt(n));
   bezext := copy(n, 1, length(n)-e);
end;
end.
