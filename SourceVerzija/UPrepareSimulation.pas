unit UPrepareSimulation;//*BojanNenadJovicic jul, avgust 2003.
                         {Unit u kome se nalazi implementacija procedure
                         prepareSimulation koja zamenjuje obj datoteku tj,
                         mObj(StringListu}
interface

        procedure prepareSimulation;

implementation

uses sysutils, UAsm, UGpss87;

procedure prepareSimulation;//*BojanNenadJovicic jul, avgust 2003.
        {procedura koja treba da zameni proceduru writeObj iz asemblera koja
        koja upisuje u stringListu(ranije u datoteku) i proceduru Input_ph iz
        procesora koja ucitava vrednosti iz string liste( datoteke) u memoriju
        da bih se pokrenuo procesor i izvrsio simulaciju
        }
var labb : longint;

PROCEDURE prepare_op (VAR Op : Operand);
BEGIN {prepare_op}
 inc(ocounter);
 case UAsm.STMTBL[counter]^.op[ocounter].typop of
   numop:  begin
             Op.Typ := Num_op;
             Op.N_op := UAsm.STMTBL[counter]^.op[ocounter].nop;
           end;
   snaop:  begin
             Op.Typ := Sna_op;
             Op.S_op.S_cod := abs(ord(UAsm.STMTBL[counter]^.op[ocounter].sop.snacod));
             case UAsm.STMTBL[counter]^.op[ocounter].sop.snaadr.adrs of
               UAsm.direct: begin
                                     Op.S_op.S_adr.Typ := Direct;
                                     Op.S_op.S_adr.Adrs := UAsm.STMTBL[counter]^.op[ocounter].sop.snaadr.nr;
                                   end;
               UAsm.indirect: begin
                                       Op.S_op.S_adr.Typ := Indirect;
                                       Op.S_op.S_adr.Adrs := UAsm.STMTBL[counter]^.op[ocounter].sop.snaadr.par;
                                     end;
             end;
           end;
   adrop:  begin
             Op.Typ := Adr_op;
             case UAsm.STMTBL[counter]^.op[ocounter].aop.adrs of
               UAsm.direct: begin
                                     Op.A_op.Typ := Direct;
                                     Op.A_op.Adrs := UAsm.STMTBL[counter]^.op[ocounter].aop.nr;
                                   end;
               UAsm.indirect: begin
                                       Op.A_op.Typ := Indirect;
                                       Op.A_op.Adrs := UAsm.STMTBL[counter]^.op[ocounter].aop.par;
                                     end;
             end;

           end;
 end;//case
end; {prepare_op}

function prepareOperand(o : UAsm.operand) : UGpss87.Operand;
begin
  case o.typop of
   numop:  begin
             prepareOperand.Typ := Num_op;
             prepareOperand.N_op := o.nop;
           end;
   snaop:  begin
             prepareOperand.Typ := Sna_op;
             prepareOperand.S_op.S_cod := abs(ord(o.sop.snacod));
             case o.sop.snaadr.adrs of
               UAsm.direct: begin
                                     prepareOperand.S_op.S_adr.Typ := Direct;
                                     prepareOperand.S_op.S_adr.Adrs := o.sop.snaadr.nr;
                                   end;
               UAsm.indirect: begin
                                       prepareOperand.S_op.S_adr.Typ := Indirect;
                                       prepareOperand.S_op.S_adr.Adrs := o.sop.snaadr.par;
                                     end;
             end;
           end;
   adrop:  begin
             prepareOperand.Typ := Adr_op;
             case o.aop.adrs of
               UAsm.direct: begin
                                     prepareOperand.A_op.Typ := Direct;
                                     prepareOperand.A_op.Adrs := o.aop.nr;
                                   end;
               UAsm.indirect: begin
                                       prepareOperand.A_op.Typ := Indirect;
                                       prepareOperand.A_op.Adrs := o.aop.par;
                                     end;
             end;

           end;
 end;//case
end;{prepeareOperand}


PROCEDURE prepare_blk;
var i : longint;
BEGIN {prepare_blk}
 IF labb > round(Max_blk * 0.9) THEN Extend_Blk_Tbl;
 IF labb < 1 THEN Error (3) ELSE
  BEGIN
    IF Blk_tbl [labb] = NIL THEN NEW (Blk_tbl[labb]);
    initBlkPtr(Blk_Tbl[labb]);
    Blk_tbl [labb]^.Op_cod := Opc;
    // Dodato ord oko Opc zbog izmene, BJ 6/7/2003
    for i:= 1 to Nr_OP[ord(Opc)] do
     prepare_op (Blk_tbl[labb]^.Op[i]);
    Blk_tbl [labb]^.N := 0;
    Blk_tbl [labb]^.W := 0
  END
END; {prepare_blk}

PROCEDURE prepare_dcl;

procedure prepare_fun;
var  i : LONGINT;begin  {prepare_fun}
  IF labb > round(Max_fun * 0.9) THEN Extend_Fun_Tbl;

  IF Fun_tbl [labb] = NIL THEN NEW (Fun_tbl [labb]);
     initFunPtr(Fun_Tbl [labb]);
     prepare_op (Op);
     Fun_tbl [labb]^.A := Op.S_op;
     prepare_op (Op);
     j := Op.N_op;
     IF ABS (j) > Max_point THEN Error (15) ELSE
           BEGIN
             IF j > 0 THEN Fun_tbl [labb]^.Typ := Con ELSE
                              Fun_tbl [labb]^.Typ := Dis;
             Fun_tbl [labb]^.N := ABS (j);
             FOR i := 1 TO ABS (j) DO
              BEGIN
                Fun_tbl[labb]^.P[i].X := FUNTBL[STMTBL[counter]^.nr].point[i].x;//mObj.READ (Fun_tbl [labb]^.P[i].X);
                // Read(Ob_f, Fun_tbl [labb]^.P[i].X);
                Fun_tbl[labb]^.P[i].Y := FUNTBL[STMTBL[counter]^.nr].point[i].y;//mObj.READ (Fun_tbl [labb]^.P[i].Y);
                // Read(Ob_f, Fun_tbl [labb]^.P[i].Y);
              END
           END

END;{prepare_fun}

PROCEDURE prepare_ini;
VAR R : REAL; i : LONGINT;BEGIN {prepare_ini} prepare_op (Op);
 Op_val (Op,R);
 IF R > round (Max_sav * 0.9)
 THEN Extend_Sav_Tbl;
     i := TRUNC(R);
     prepare_op (Op);
     Op_val (Op,R);
     Sav_tbl [i] := TRUNC(R)
END; {prepare_ini}

PROCEDURE prepare_tab;
VAR i: LONGINT;BEGIN {prepare_tab}
  IF labb > round(Max_tab * 0.9) THEN Extend_Tab_Tbl;
    IF Tab_tbl [labb] = NIL THEN NEW (Tab_tbl [labb]);
    initTabPtr(Tab_Tbl [labb]);
    Tab_tbl [labb]^.Tb := 0;
    Tab_tbl [labb]^.Tc := 0;
    Tab_tbl [labb]^.Td := 0;
    Tab_tbl [labb]^.Sa := 0;
    Tab_tbl [labb]^.Avo := 0;
    prepare_op (Op);
    Tab_tbl [labb]^.Arg := OP.S_op;
    prepare_op (Op);
    Tab_tbl [labb]^.I [1].UL := OP.N_op;
    prepare_op (Op);
    Tab_tbl [labb]^.Si := Op.N_op;
    prepare_op (Op);
    Tab_tbl [labb]^.Ni := Op.N_op;
    j := Op.N_op + 1;
    IF Op.N_op > Max_intr - 1 THEN Error (12) ELSE
          BEGIN
            FOR i:= 1 TO Tab_tbl [labb]^.Ni + 1 DO
             BEGIN
              IF i > 1 THEN
               Tab_tbl [labb]^.I [i].UL := Tab_tbl [labb]^.I[1].Ul
                           + Tab_tbl [labb]^.Si * (i-1);
              Tab_tbl [labb]^.I[i].Off := 0
             END
          END
END; {prepare_tab}

PROCEDURE prepare_var;
VAR i : LONGINT ;BEGIN {prepare_var}
 IF labb > round(Max_var  * 0.9) THEN Extend_Var_Tbl;
   IF Var_tbl [labb] = NIL THEN NEW (Var_tbl [labb]);
   initVarPtr(Var_Tbl [labb]);
   prepare_op (Op);
   Var_tbl [labb]^.N := Op.N_op;
   IF Op.N_op > Max_vop THEN Error (8) ELSE
     FOR i := 1 TO Op.N_op DO
       BEGIN
         if ( VARTBL[STMTBL[counter]^.nr].op[i]^.typ = UAsm.voperator ) then
         begin
           Var_tbl [labb]^.P_N [i].Typ := Voperator;
           Var_tbl [labb]^.P_N [i].Vopr := VARTBL[STMTBL[counter]^.nr].op[i]^.oprtr;
         end
           else
           begin
             Var_tbl [labb]^.P_N [i].Typ := Voperand;
             Var_tbl [labb]^.P_N [i].Vop := prepareOperand(VARTBL[STMTBL[counter]^.nr].op[i]^.oprnd);
           end;
       end;
 END; {prepare_var}

PROCEDURE prepare_sto;
VAR R : REAL;
BEGIN {prepare_sto}
  IF labb > round (Max_sto * 0.9)
        THEN Extend_Sto_Tbl;
   //BEGIN
     IF Sto_tbl [labb] = NIL THEN NEW (Sto_tbl [labb]);
     prepare_op (Op);
     Op_val (Op,R);
     Sto_tbl [labb]^.R := TRUNC(R);
     Sto_tbl [labb]^.S := 0;
     Sto_tbl [labb]^.Sa := 0;
     Sto_tbl [labb]^.Sc := 0;
     Sto_tbl [labb]^.Sr := 0;
     Sto_tbl [labb]^.Sm := 0;
     Sto_tbl [labb]^.St := 0;
     Sto_tbl [labb]^.Tht := 0;
     Sto_tbl [labb]^.C0 := 0
   //END
END; {prepare_sto}

BEGIN {prepare_dcl}
 CASE Opc OF         // Promenjeno iz brojeva u odgovarajuce enumerativne, BJ 6/7/2003
    ocRead_Fun : prepare_fun;
    ocRead_ini : prepare_ini;
    ocRead_sto : prepare_sto;
    ocRead_tab : prepare_tab;
    ocRead_var : prepare_var;
 END {CASE}
END; {prepare_dcl}

PROCEDURE prepare_ctr;

PROCEDURE Reset;

PROCEDURE Cec_rem (VAR Tr:Tra_ptr);
BEGIN  {Cec_rem}
  IF Tr^.Blink = NIL THEN
   BEGIN {Azurira se Head CEC}
    Cec.Head := Tr^.Flink;
    IF Cec.Head <> NIL THEN Cec.Head^.Blink  := NIL ELSE Cec.Tail := NIL;
   END
    ELSE IF Tr^.Flink = NIL THEN
      BEGIN {Azurira se tail CEC}
        Cec.Tail := Tr^.Blink;
        IF Cec.Tail <> NIL THEN Cec.Tail^.Flink := NIL ELSE Cec.Head := NIL;
      END
       ELSE
        BEGIN {Azuriraju se pokazivaci prethodne i sledece trans}
          TR^.Flink^.Blink := Tr^.Blink;
          TR^.Blink^.Flink := Tr^.Flink
        END
END; {Cec_rem}

PROCEDURE Fec_ins ( VAR  Tr : Tra_ptr );
{Ubacuje transakciju na koju pokazuje pokazivac Tr
 u FEC uredjenu po Bdt,Pr }
VAR Next,Prev : Tra_ptr; { Pointeri na predhodnu i sledecu
                           transakciju u FEC }
    Flg : BOOLEAN;
BEGIN {Fec_ins}
  IF Fec.Head = NIL THEN
   BEGIN {Ubacivanje transakcije u praznu listu}
     Fec.Head := Tr;
     Fec.Tail := Tr;
     Tr^.Flink := NIL;
     Tr^.Blink := NIL
   END  ELSE
   BEGIN
     Prev := Fec.Tail;
     Next := NIL;
    {Odredjivanje mesta za Tr u FEC}
     Flg := TRUE;
     WHILE  (Prev <> NIL) AND Flg DO
      BEGIN
       Flg := ((Prev^.Bdt > Tr^.Bdt) OR
            ((Prev^.Bdt = Tr^.Bdt) AND (Prev^.Pr < Tr^.Pr)));
       IF Flg THEN
        BEGIN
          Next := Prev;  {Prelazak na sledecu transakciju}
          Prev := Prev^.Blink
        END;
      END;
     IF Prev = NIL THEN
      BEGIN  {Ubacivanje na pocetak FEC}
        Fec.Head := Tr;
        Next^.Blink := Tr;
        Tr^.Flink := Next;
        Tr^.Blink := NIL;
      END ELSE
      IF Next = NIL THEN
       BEGIN {Ubacivanje na kraj FEC}
         Fec.Tail := Tr;
         Prev^.Flink := Tr;
         Tr^.Flink := NIL;
         Tr^.Blink := Prev
       END
       ELSE
        BEGIN
          Tr^.Flink := Next;
          Tr^.Blink := Prev;
          Prev^.Flink := Tr;
          Next^.Blink := Tr
        END
   END;
   Tr^.Ch := FEC_ch
END; {Fec_ins}

PROCEDURE Fec_rem (VAR Tr:Tra_ptr);
BEGIN  {Fec_rem}
  IF Tr^.Blink = NIL THEN
   BEGIN {Azurira se Head FEC}
    Fec.Head := Tr^.Flink;
    IF Fec.Head <> NIL THEN  Fec.Head^.Blink  := NIL ELSE Fec.Tail := NIL;
   END
   ELSE IF Tr^.Flink = NIL THEN
    BEGIN {Azurira se tail FEC}
      Fec.Tail := Tr^.Blink;
      IF Fec.Tail <> NIL THEN Fec.Tail^.Flink := NIL ELSE Fec.Head := NIL;
    END ELSE
     BEGIN {Azuriraju se pokazivaci prethodne i sledece trans}
      Tr^.Flink^.Blink := Tr^.Blink;
      Tr^.Blink^.Flink := Tr^.Flink
     END
END; {Fec_rem}

VAR i,j : LONGINT;
    Trans_tmp : Tra_ptr;
BEGIN {Reset}
 Trans_tmp := FEC.Head;
 WHILE Trans_tmp <> NIL DO  {Podesavanje vremena u FEC}
  BEGIN
    Trans_tmp^.Bdt := Trans_tmp^.Bdt - C1;
    Trans_tmp := Trans_tmp^.Flink
  END;

 WHILE Cec.head <> NIL DO {Anuliranje vremena u CEC i prebacivanje u FEC}
  BEGIN
    Trans_tmp := CEC.Head;
    Trans_tmp^.Bdt := 1;
    Cec_rem(Trans_tmp);
    Fec_ins(Trans_tmp)
  END;

 Trans_tmp := FEC.Head;
 WHILE Trans_tmp <> NIL DO
  BEGIN
    Trans := Trans_tmp;
    Trans_tmp := Trans_tmp^.Flink;
    // Enumerativni umesto 9, BJ 6/7/2003
    IF Blk_tbl[Trans^.Cb]^.Op_cod = ocError21 THEN
      BEGIN
       Fec_rem(Trans); {Generate}
       Dispose(Trans);
       Trans := NIL
      END;
  END;

 Trans := NIL;
 C1 := 0; {Anuliranje sata}
 {Brisanje statistike}
 FOR i:=1 TO Max_blk DO IF Blk_tbl[i] <> NIL THEN Blk_tbl[i]^.N:=Blk_tbl[i]^.W;
 FOR i := 1 TO Max_cha DO
  IF Cha_tbl [i] <> NIL THEN
   BEGIN
     Cha_tbl [i]^.Ca := 0;
     Cha_tbl [i]^.Cc := Cha_tbl [i]^.Ch;
     Cha_tbl [i]^.Cm := 0;
     Cha_tbl [i]^.Ct := 0;
     Cha_tbl [i]^.Tht := 0;
     Cha_tbl [i]^.C0 := 0;
   END;

 FOR i := 1 TO Max_fac DO
  IF Fac_tbl [i] <> NIL THEN
   BEGIN
     Fac_tbl [i]^.Fc := Fac_tbl [i]^.F;
     Fac_tbl [i]^.Fr := 0;
     Fac_tbl [i]^.Ft := 0;
     Fac_tbl [i]^.C0 := 0;
     Fac_tbl [i]^.Tht := 0;
   END;

 FOR i := 1 TO Max_sto DO
  IF Sto_tbl [i] <> NIL THEN
   BEGIN
     Sto_tbl [i]^.Sa := 0;
     Sto_tbl [i]^.Sc :=  Sto_tbl [i]^.S;
     Sto_tbl [i]^.Sr := 0;
     Sto_tbl [i]^.Sm := 0;
     Sto_tbl [i]^.St := 0;
     Sto_tbl [i]^.Tht := 0;
     Sto_tbl [i]^.C0 := 0
   END;

 FOR i := 1 TO Max_tab DO
  IF Tab_tbl [i] <> NIL THEN
   BEGIN
     Tab_tbl [i]^.Tb := 0;
     Tab_tbl [i]^.Tc := 0;
     Tab_tbl [i]^.Td := 0;
     Tab_tbl [i]^.Sa := 0;
     Tab_tbl [i]^.Avo:= 0;
     FOR j := 1 TO Max_intr DO Tab_tbl [i]^.I [j].Off := 0;
  END;

 FOR i := 1 TO Max_que DO
  IF Que_tbl [i] <> NIL THEN
   BEGIN
     Que_tbl [i]^.Qa := 0;
     Que_tbl [i]^.Qc := Que_tbl [i]^.Q;
     Que_tbl [i]^.Qm := 0;
     Que_tbl [i]^.Qt := 0;
     Que_tbl [i]^.Qx := 0;
     Que_tbl [i]^.Qz := 0;
     Que_tbl [i]^.Tht := 0;
     Que_tbl [i]^.Thz := 0;
     Que_tbl [i]^.C0 := 0;
     Que_tbl [i]^.Cz := 0
   END;
END; {Reset}

PROCEDURE Clear;
VAR S : ARRAY [1..5] OF word;
    i : LONGINT;

PROCEDURE Free (VAR Head : Tra_ptr); {Oslobadja transakcije}
VAR Temp : Tra_ptr;
BEGIN {Free}
   WHILE Head <> NIL DO
     BEGIN
       Temp := Head^.Flink;
       IF Head^.P <> NIL THEN DISPOSE (Head^.P);
       DISPOSE (Head);
       Head := Temp
     END
END; {Free}

BEGIN {Clear}
  FOR i:= 1 TO 5 DO  {Ulaz opreanada za CLEAR}
   BEGIN
     prepare_op (Op);
     S [i] := Op.A_op.Adrs
   END;

  Reset; {Anuliranje sata i ciscenje statistike}

  {Anuliranje SNA koje RESET ne anulira}

  Free (CEC.Head); CEC.Tail := NIL; {Oslobadjanje CEC}
  Free (FEC.Head); FEC.Tail := NIL; {Oslobadjanje FEC}

  FOR i := 1 TO Max_cha DO          {Oslobadjanje UC }
   IF Cha_tbl[i] <> NIL THEN
    BEGIN
     Free (Cha_tbl[i]^.Head);
     Cha_tbl [i]^.Tail := NIL
    END;

{Oslobadjanje prijemptovanih transakcija }

  FOR i := 1 TO Max_fac DO IF Fac_tbl[i] <> NIL THEN Free (Fac_tbl[i]^.Head);


  FOR i := 1 TO Max_blk DO IF Blk_tbl [i] <> NIL
     THEN
       BEGIN
         Blk_tbl [i]^.W := 0;
         Blk_tbl [i]^.N := 0
       END;

  FOR i := 1 TO Max_cha DO IF Cha_tbl [i] <> NIL THEN Cha_tbl [i]^.Ch := 0;

  FOR i := 1 TO Max_fac DO IF Fac_tbl [i] <> NIL
     THEN BEGIN
            Fac_tbl [i]^.Sc := 0;
            Fac_tbl [i]^.F := 0;
            Fac_tbl [i]^.Pc := 0
          END;

  FOR i := 1 TO Max_sto DO IF Sto_tbl [i] <> NIL
     THEN Sto_tbl [i]^.S := 0;

  FOR i := 1 TO Max_que DO IF Que_tbl [i] <> NIL
     THEN Que_tbl [i]^.Q := 0;

  FOR i := 1 TO Max_sav DO {Anuliranje Savevalue}
   IF NOT (i IN [ S[1],S[2],S[3],S[4],S[5] ]) THEN Sav_tbl [i] := 0;
END; {Clear}

PROCEDURE Start;
BEGIN {Start}
    prepare_op (Op);
    Tc := Op.N_op;
    prepare_op (Op);
    IF Op.N_op = 0 THEN P_flg := FALSE ELSE P_flg := TRUE
END; {Start}

BEGIN {prepare_ctr}
  CASE opc OF // Promenjeno u odgovarajuce enumarativne, BJ 6/7/2003
   ocClear : Clear;
   ocStop : Stop := TRUE; {END Naredba}
   ocReset : Reset;
   ocSim_flg : Sim_flg := TRUE; {Simulate1}
   ocStart : Start;
  ELSE BEGIN END;
  END {CASE}
END; {prepare_ctr}
{}
var read_int : integer; // Znak za pomoc pri konverziji pri ucitavanju, BJ 6/7/2003
BEGIN {prepareSimulation}
  REPEAT    // iz 0 u ocNull, BJ 6/7/2003
    inc(counter);
    ocounter := 0;
    labb := 0; Opc := ocNull;
    labb := UAsm.STMTBL[counter]^.nr;//mObj.READ (labb);
    // Read(Ob_f, labb);
    read_int := UAsm.STMTBL[counter]^.oc;//Obj.READ (read_int);
    // Read(Ob_F, read_int);
    Opc := Int2OC(read_int);
    IF Opc IN [ ocAdvance,
                ocAssign,
                ocDepart,
                ocEnter,
                ocGate,
                ocError21,
                ocLeave..ocRelease,
                ocSavevalue,
                ocSeize,
                ocTabulate..ocUnlink] THEN prepare_blk
    ELSE
    IF Opc IN [ ocRead_fun,
                ocRead_ini,
                ocRead_sto,
                ocRead_tab,
                ocRead_var] THEN prepare_dcl ELSE
    IF Opc IN [ ocClear,
                ocStop,
                ocReset,
                ocSim_flg,
                ocStart] THEN prepare_ctr ELSE Error (1);

    //mObj.READLN;
    inc(krug);
     //snimi_blokove(inttostr(krug));
    // Readln(Ob_f);
  until STOP or ( ( not Stop ) and (Tc > 0))  or ( counter = UAsm.stmcount ); // EOF(ob_f);
  IF ( counter = UAsm.stmcount{mObj.EOF})//EOF(ob_F)
      and (Opc <> ocStop) then Error (14);

  // snimi_blokove(FloatToStr(Now));
END; {prepareSimulation}










end.
