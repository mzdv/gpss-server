unit UGrafickiSimboli;//*BojanNenadJovicic jul, avgust 2003.
                      { Unit u kome su definisane klase tipa simbol
                      koje se iscrtavaju na ekranu, funkcionalnost je postignuta
                      definisanje virtualnih metoda}

interface

uses ExtCtrls, Classes, Graphics, StdCtrls, Controls;
Type
        TSimbol = class(TPanel)
                constructor Create(AOwner : TComponent);override;
                constructor CreateFromString(state : string; AOwner : TComponent);
                private
                  prefiks : string;
                  image : TImage;
                  pictureCenter : TPicture;
                  pictureLeft : TPicture;
                public
                  function getSimbolText:string;virtual;
                  function getType:string;virtual;abstract;
                  procedure setPrefiks(prefiks : string);
                  function getPrefiks:string;
                  procedure setPictureLeft;virtual;
                  procedure setPictureCenter;virtual;
                  function save: string;virtual;abstract;
                end;


        TSimbolGenerate = class(TSimbol)
                constructor Create(AOwner : TComponent);override;
                constructor CreateFromString(state : string; AOwner : TComponent);
                private
                  edText : TEdit;
                public
                  function getSimbolText:string;override;
                  function getType:string;override;
                  function save:string;override;
                end;

        TSimbolAdvance = class(TSimbol)
                constructor Create(AOwner : TComponent);override;
                constructor CreateFromString(state : string; AOwner : TComponent);
                private
                  edText : TEdit;
                public
                  function getSimbolText:string;override;
                  function getType:string;override;
                  function save:string;override;
                end;

        TSimbolTransfer = class(TSimbol)
                constructor Create(AOwner : TComponent);override;
                constructor CreateFromString(state : string; AOwner : TComponent);
                private
                  edText : TEdit;
                  edTransfer : TEdit;
                public
                  function getSimbolText:string;override;
                  function getType:string;override;
                  function getTransfer:string;
                  function save:string;override;
                end;

        TSimbolTerminate = class(TSimbol)
                constructor Create(AOwner : TComponent);override;
                constructor CreateFromString(state : string; AOwner : TComponent);
                private

                  edText : TEdit;
                public
                  function getSimbolText:string;override;
                  function getType:string;override;
                  procedure setPictureLeft;override;
                  procedure setPictureCenter;override;
                  function save:string;override;

                end;

        TSimbolStart = class(TSimbol)
                constructor Create(AOwner : TComponent);override;
                constructor CreateFromString(state : string; AOwner : TComponent);
                private
                  edText : TEdit;
                public
                  function getSimbolText:string;override;
                  function getType:string;override;
                  function save:string;override;

                end;



implementation

function StrExp (in_str : string; n : integer) : string;
var tmp_str : string;
    i : integer;
begin
    tmp_str := in_str;
    for i := length(tmp_str) to n do
        tmp_str := tmp_str + ' ';
    result := tmp_str;
end;

function getState( var state : string):string;
begin
  result := copy(state, 1, pos('~', state) - 1);
  delete(state, 1, pos('~', state));
end;


constructor TSimbol.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  pictureCenter := TPicture.Create;
  pictureLeft := TPicture.Create;
  image := TImage.Create(Self);
  image.Parent := Self;
  image.Align := alClient;
  prefiks := '';
  BevelOuter := bvNone;
  Color := clWhite;

end;

constructor TSimbol.CreateFromString(state : string; AOwner : TComponent);
var simbolType : string;
begin
  simbolType := getState(state);
  if ( simbolType = 'generate' ) then
    Self := TSimbolGenerate.CreateFromString(state, AOwner)
  else
    if (simbolType = 'advance') then
      Self := TSimbolAdvance.CreateFromString(state, AOwner)
    else
      if ( simbolType = 'transfer' ) then
        Self := TSimbolTransfer.CreateFromString(state, AOwner)
      else
        if ( simbolType = 'terminate' ) then
          Self := TSimbolTerminate.CreateFromString(state, AOwner)
        else
          if ( simbolType = 'start' ) then
            Self := TSimbolStart.CreateFromString(state, AOwner)

end;

procedure TSimbol.setPrefiks(prefiks : string);
begin
  Self.prefiks := prefiks;
end;
function TSimbol.getPrefiks:string;
begin
  result := prefiks;
end;

function TSimbol.getSimbolText:string;
begin
  result := StrExp(Self.getPrefiks, 6);
end;

procedure TSimbol.setPictureLeft;
begin
  image.Picture := pictureLeft;
end;

procedure TSimbol.setPictureCenter;
begin
  image.Picture := pictureCenter;
end;

{TSimbolGenerate}
constructor TSimbolGenerate.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  Self.Width := 141;
  Self.Height := 91;
  pictureCenter.LoadFromFile('./Slicice/Generate.bmp');
  pictureLeft.LoadFromFile('./Slicice/Generate.bmp');
  image.Picture := pictureCenter;
  edText := TEdit.Create(Self);
  edText.Parent := Self;
  edText.Top := 35;
  edText.Left := 27;
  edText.Width := 111;
end;

constructor TSimbolGenerate.CreateFromString(state : string; AOwner : TComponent);
var atribute : string;
begin
  Self.Create(AOwner);
  atribute := getState(state);
  setPrefiks(atribute);
  atribute := getState(state);
  edText.Text := atribute;
end;

function TSimbolGenerate.getSimbolText():string;
begin
  result := inherited getSimbolText;
  result := result + 'GENERATE ' + edText.Text;
end;

function TSimbolGenerate.getType:string;
begin
  result := 'generate';
end;

function TSimbolGenerate.Save:string;
begin
  result := 'generate~' + getPrefiks + '~' + edText.Text + '~';
end;
{TSimbolGenerate}


{TSimbolAdvance}
constructor TSimbolAdvance.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  Self.Width := 141;
  Self.Height := 91;
  pictureCenter.LoadFromFile('./Slicice/Advance.bmp');
  pictureLeft.LoadFromFile('./Slicice/Advance.bmp');
  image.Picture := pictureCenter;
  edText := TEdit.Create(Self);
  edText.Parent := Self;
  edText.Top := 30;
  edText.Left := 27;
  edText.Width := 90;
end;

constructor TSimbolAdvance.CreateFromString(state : string; AOwner : TComponent);
var atribute : string;
begin
  Self.Create(AOwner);
  atribute := getState(state);
  setPrefiks(atribute);
  atribute := getState(state);
  edText.Text := atribute;
end;



function TSimbolAdvance.getSimbolText():string;
begin
  result := inherited getSimbolText;
  result := result + 'ADVANCE ' + edText.Text;
end;

function TSimbolAdvance.getType:string;
begin
  result := 'advance';
end;

function TSimbolAdvance.save: string;
begin
    result := 'advance~' + getPrefiks + '~' + edText.Text + '~';
end;

{TSimbolAdvance}

{TSimbolTransfer}
constructor TSimbolTransfer.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  Self.Width := 141;
  Self.Height := 91;
  pictureCenter.LoadFromFile('./Slicice/Transfer.bmp');
  pictureLeft.LoadFromFile('./Slicice/Transfer.bmp');
  image.Picture := pictureCenter;
  edText := TEdit.Create(Self);
  edText.Parent := Self;
  edText.Top := 35;
  edText.Left := 55;
  edText.Width := 35;
  edTransfer := TEdit.Create(Self);
  edTransfer.Parent := Self;
  edTransfer.Top := 8;
  edTransfer.Left := 118;
  edTransfer.Width := 17;

end;

constructor TSimbolTransfer.CreateFromString(state : string; AOwner : TComponent);
var atribute : string;
begin
  Self.Create(AOwner);
  atribute := getState(state);
  setPrefiks(atribute);
  atribute := getState(state);
  edText.Text := atribute;
  atribute := getState(state);
  edTransfer.Text := atribute;
end;


function TSimbolTransfer.getSimbolText():string;
begin
  result := inherited getSimbolText;
  result := result + 'TRANSFER ' + edText.Text + ',' + edTransfer.Text;
end;

function TSimbolTransfer.getType:string;
begin
  result := 'transfer';
end;

function TSimbolTransfer.getTransfer:string;
begin
  result := edTransfer.Text;
end;

function TSimbolTransfer.save: string;
begin
    result := 'transfer~' + getPrefiks + '~' + edText.Text + '~' + getTransfer + '~';
end;


{TSimbolTransfer}

{TSimbolTerminate}
constructor TSimbolTerminate.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  Self.Width := 141;
  Self.Height := 91;
  pictureCenter.LoadFromFile('./Slicice/Terminate.bmp');
  pictureLeft.LoadFromFile('./Slicice/TerminateLeft.bmp');
  image.Picture := pictureCenter;
  edText := TEdit.Create(Self);
  edText.Parent := Self;
  edText.Top := 35;
  edText.Left := 107;
  edText.Width := 30;
end;

constructor TSimbolTerminate.CreateFromString(state : string; AOwner : TComponent);
var atribute : string;
begin
  Self.Create(AOwner);
  atribute := getState(state);
  setPrefiks(atribute);
  atribute := getState(state);
  edText.Text := atribute;
end;


function TSimbolTerminate.getSimbolText():string;
begin
  result := inherited getSimbolText;
  result := result + 'TERMINATE ' + edText.Text;
end;

function TSimbolTerminate.getType:string;
begin
  result := 'terminate';
end;

procedure TSimbolTerminate.setPictureCenter;
begin
  inherited setPictureCenter;
  edText.Top := 35;
  edText.Left := 107;
end;

procedure TSimbolTerminate.setPictureLeft;
begin
  inherited setPictureLeft;
  edText.Top := 35;
  edText.Left := 72;
end;

function TSimbolTerminate.save: string;
begin
    result := 'terminate~' + getPrefiks + '~' + edText.Text + '~';
end;

{TSimbolTerminate}

{TSimbolStart}
constructor TSimbolStart.Create(AOwner : TComponent);
begin
  inherited Create(AOwner);
  Self.Width := 141;
  Self.Height := 91;
  pictureCenter.LoadFromFile('./Slicice/Start.bmp');
  pictureLeft.LoadFromFile('./Slicice/Start.bmp');
  image.Picture := pictureCenter;
  edText := TEdit.Create(Self);
  edText.Parent := Self;
  edText.Top := 55;
  edText.Left := 30;
  edText.Width := 80;
end;

constructor TSimbolStart.CreateFromString(state : string; AOwner : TComponent);
var atribute : string;
begin
  Self.Create(AOwner);
  atribute := getState(state);
  setPrefiks(atribute);
  atribute := getState(state);
  edText.Text := atribute;
end;


function TSimbolStart.getSimbolText():string;
begin
  result := inherited getSimbolText;
  result := result + 'START ' + edText.Text;
end;

function TSimbolStart.getType:string;
begin
  result := 'start';
end;

function TSimbolStart.save: string;
begin
  result := 'start~' + getPrefiks + '~' + edText.Text + '~';
end;


{TSimbolStart}

end.
