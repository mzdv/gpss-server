{$D-}
unit Precica;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, menus;

type
  TANP = (TANP_Ignorisi, TANP_Pitaj, TANP_AutomatskiIzbaci);

  TPrecicaSelect = procedure(Sender: TObject; naziv : string) of object;

  TPrecica = class(TComponent)
  private    { Private declarations }
    FOnSelect : TPrecicaSelect;
    FPrethodna : TMenuItem;
    FMaxBrojstavki, FBrojStavki : integer;
    FImeFajla, FPocetakOpisa, FKrajOpisa : string;
    FNazivi : array [1..11] of string;
    FAUtoUbaci : boolean;
    FANP : TANP;
  protected    { Protected declarations }
    procedure click(sender : TObject);
    procedure SetMax_Broj_stavki(b : integer);
    procedure SetPrethodna(i : TMenuItem);
    procedure Osvezi;
    procedure Ubaci_sekciju(var f : textfile);
    procedure SetImeFajla(n : string);
    procedure SetFANP(a : TANP);
    function SveOk(n : string):boolean;
  public    { Public declarations }
    procedure Ocisti;
    constructor Create(AOwner: TComponent); override;
    procedure Ubaci(n : string);
    function Ucitaj(n : string) : boolean;
    procedure Snimi;
    destructor Destroy; override;

  published    { Published declarations }
    property Max_Broj_Stavki : integer read FMaxBrojstavki write SetMax_Broj_stavki default 5;
    property Nadredjena_stavka : TMenuitem read FPrethodna write SetPrethodna;
    property Naziv_Fajla : string read FImeFajla write setImeFajla;
    property Pocetak_Opisa : string read FPocetakOpisa write FPocetakOpisa;
    property Kraj_Opisa : string read FKrajOpisa write FKrajOpisa;
    property Auto_Ubaci : boolean read FAutoUbaci write FAutoUbaci default false;
    property Ako_nema_precice: TANP read FANP write SetFANP default TANP_ignorisi;

    property OnSelect: TPrecicaSelect read FOnSelect write FOnSelect;

    {    property read write}
  end;


procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('Milos', [TPrecica]);
end;
constructor TPrecica.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FMaxBrojstavki := 5;
  FPrethodna := nil;
  FBrojstavki := 0;FPocetakOpisa:='[Spisak precica]';FKrajOpisa:='[Kraj spiska precica]';
end;
procedure TPrecica.SetMax_Broj_stavki(b : integer);
begin
   if b in [0..10] then
     begin
       FMaxBrojstavki := b;
       ocisti; osvezi;
     end;
end;
procedure TPrecica.SetPrethodna(i : TMenuItem);
begin
     if Fprethodna <> i then
       begin
          Ocisti;
          FPrethodna := i;
          Ocisti;
          Osvezi;
       end;
end;
procedure TPrecica.ocisti;
begin     {pocisti stare stavke}
   if assigned(FPrethodna) then
      begin
        FPrethodna.destroycomponents;
        while fprethodna.Count > 0 do
           FPrethodna.delete(0);
      end;
end;
procedure TPrecica.Osvezi;
var I : integer;nova : TMenuItem;
begin
   If assigned(Fprethodna)then
     begin
        for i := 1 to FBrojstavki do
          begin
            if i > FMaxBrojStavki then break;
              nova := TMenuItem.create(FPrethodna);
              nova.caption := inttostr(i) +': '+FNazivi[i];
              nova.tag := i;
              nova.OnClick := Click;
              nova.Name := 'Stavka_Precice'+inttostr(i);
              Fprethodna.add(nova);
          end;
          if FBrojStavki > 0 then FPrethodna.enabled := true
          else FPrethodna.enabled := false;
     end;
end;
procedure TPrecica.Ubaci(n : string);
var pom, n2 : string; i : integer;
begin
     n2 := n;
     for i := 1 to FBrojStavki+1 do
       begin
          pom := FNazivi[i];
          FNazivi[i]:= n2;
          n2 := pom;
          if n2 = n then break;
       end;
     if (n2<>n) and (FBrojStavki < 10) then FBrojstavki := FBrojstavki + 1;
     ocisti;
     osvezi;
end;
function TPrecica.Ucitaj(n : string) : boolean;
var f : textfile;baf : string;
begin
     if n <> '' then FImeFajla := n;
     result := false;
     if(extractfilename(FImeFajla)<>'')and FileExists(FImeFajla)then
       begin
          FBrojStavki := 0;
          assignfile (f, FImeFajla);
          reset(f);
          baf := '';
          while ((not eof(f)) and (baf<> FPocetakOpisa)) do
             readln(f, baf);
          if not(eof(f)) then
             while baf<>FKrajOpisa do
               begin
                  readln(f, baf);
                  if baf<>FKrajOpisa then
                      if SveOK(baf) then
                        begin
                          FBrojStavki :=  FBrojStavki + 1;
                          FNazivi[FBrojStavki]:= baf;
                        end;
                  if FBrojStavki=10 then break;
               end;
          closefile(f);
          ocisti;
          osvezi;
       end;
end;
procedure TPrecica.Snimi;
var f1, f2 : Textfile;baf : string;
begin
 if extractfilename(FImeFajla)<>'' then
  if FileExists(FImeFajla) then
    begin
     assignfile (f1, FImeFajla);
     if fileexists('prectemp.tmp') then
       begin
         Showmessage('Prectemp.tmp exists. Please remove if not needed!');
         exit;
       end;
     assignfile (f2, 'prectemp.tmp');
     reset(f1); rewrite(f2);
     readln(f1, baf);
     while ((not eof(f1)) and (baf<>FPocetakOpisa)) do
         begin
           writeln(f2, baf);
           readln(f1, baf);
         end;
     Ubaci_sekciju(f2);
     while ((not eof(f1)) and (baf<>FKrajOpisa)) do
           readln(f1, baf);
     while not eof(f1) do
         begin
           readln(f1, baf);
           writeln(f2, baf);
         end;
     closefile(f1); closefile(f2);
     deletefile(FImeFajla);
     rename(f2, FImeFajla);
    end
  else
    begin
       assignfile(f1, FImeFajla);
       rewrite(f1);
       Ubaci_sekciju(f1);
       closefile(f1);
    end;
end;
procedure TPrecica.Ubaci_sekciju(var f : textfile);
var I : integer;
begin
     writeln(f, FPocetakOpisa);
     for i := 1 to FBrojStavki do
       writeln(f, FNazivi[i]);
     writeln(f, FKrajOpisa);
end;
procedure TPrecica.Click(sender : TObject);
var s: string;
begin
   s :=Fnazivi[(sender as TMenuItem).tag];
   if FAUtoUbaci then Ubaci(s);
   if assigned (FOnSelect) then FOnSelect(self, s);
end;

procedure TPrecica.setImeFajla(n : string);
begin
     if n <> FImeFajla then
       begin
         if (extractfilename(FImeFajla)<>'')
           then snimi;
         ucitaj(n);
       end;
end;
destructor TPrecica.Destroy;
begin
    try
     if FImeFajla<>'' then Snimi;
    finally
     inherited destroy;
    end;
end;
function TPrecica.SveOk(n : string):boolean;
begin
     result := true;
     if n = '' then result := false
     else
        if FANP <> TANP_Ignorisi then
          if not(fileexists(extractfilename(n)+'.*')) then
             if Fanp = TANP_Pitaj then
                result := (messagedlg('Precica : "'+n+'" ne postoji. Da li zelite da ignorisem ovu precicu?',
                                         MTConfirmation, [mbYes, mbNo], 0)=MRNo)
             else
                result := false;
end;
procedure TPrecica.SetFANP(a : TANP);
begin
   if a <> FANP then
     begin
        snimi;
        FANP := a;
        ucitaj(self.FImeFajla);
     end;
end;
end.
