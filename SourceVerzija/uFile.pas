unit uFile;
// Unit koji sadrzi procedure koje emuliraju rad sa tekstualnom datotekom, BJ 6/7/2003
interface
uses classes, sysutils;
type
    TMSLst = class (TStringList)
        private
        public
            procedure Write(tekst : string);
            procedure WriteLn(tekst : string);
            procedure Read(var slovo : char);overload;
            procedure Read(var broj : longint);overload;
            procedure Read(var rbroj : real);overload;
            function EOF : boolean;
            procedure ReadLn;
            constructor Create;
        end;
var
    mLst, {mObj,//*BojanNenadJovicic jul, avgust 2003.*} mRez : TMSLst;
          //jer se vise ne koristi obj kao medjukorak
implementation

{ mLst }

constructor TMSLst.Create;
begin
    inherited Create;
end;

procedure TMSLst.Read(var slovo: char);
begin
    slovo := self[0][1];
    self[0] := copy(self[0], 2, length(self[0]));
end;

procedure TMSLst.Read(var broj: longint);
var v, code, pb : longint;
begin
  val(self[0], v, code);
  pb := pos(inttostr(v), self[0]);
  if (pb > 1) and (self[0][pb - 1] = '-')   then
    v := v * -1;
  broj := v;
  self[0] := copy(self[0], pos(inttostr(v), self[0]) + length(inttostr(v)), length(self[0]));
end;

function TMSLst.EOF: boolean;
begin
    if self.Count < 1
        then Result := true
        else Result := false;
end;

procedure TMSLst.Read(var rbroj: real);
var tmp_string : string;
    i , j, kr : integer;
begin
 i := 1;
 while self[0][i] = ' ' do
    inc(i);
 j := pos('.',self[0]);
 tmp_string := copy(self[0], i, j - i) + '.';
 inc(j);
 i := j;
 while (i <> length(self[0])) and (self[0][i] in ['0'..'9']) do
    inc(i);
 if i <> length(self[0])
    then kr := 0
    else kr := 1;
 tmp_string := tmp_string + copy(self[0], j, i - j + kr);
 rbroj := StrToFloat(tmp_string);
 tmp_string := copy(self[0], pos(tmp_string, self[0]) + length(tmp_string), length(self[0]));
 self[0] := tmp_string;
end;

procedure TMSLst.ReadLn;
begin
    self.Delete(0);
end;

procedure TMSLst.Write(tekst: string);
begin
  if self.count > 0
    then self[self.Count - 1] := self[self.Count - 1] + tekst
    else self.Add(tekst);
end;

procedure TMSLst.WriteLn(tekst: string);
begin
  if self.count > 0
    then self[self.Count - 1] := self[self.Count - 1] + tekst
    else self.Add(tekst);
  self.Add('');
end;

end.
