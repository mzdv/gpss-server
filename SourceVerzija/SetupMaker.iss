[Setup]
Bits=32
AppName=GPSS for Windows
AppVerName=GPSS for Windows 3.02
AppCopyright=Copyright � 2000 Bozidar Radenkovic
DefaultDirName={pf}\GPSS for Windows
DefaultGroupName=GPSS for Windows
MinVersion=4,3.51
AlwaysCreateUninstallIcon=1

[Files]
Source: "w:\gpss\GPSSW.exe"; DestDir: "{app}"
Source: "w:\gpss\procitaj.txt"; DestDir: "{app}"

[Icons]
Name: "{group}\GPSS for Windows"; Filename: "{app}\GPSSW.exe"
